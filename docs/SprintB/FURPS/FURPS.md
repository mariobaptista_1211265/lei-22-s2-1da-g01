# Supplementary Specification (FURPS+)

## Functionality

_Specifies functionalities that:_

- _are common across several US/UC;_
- _are not related to US/UC, namely: Audit, Reporting and Security._

-The users can change the language of the application;  
-Validate the schedule of the vaccine via SMS;  
-Users register process, all users need a password;  
-Schedule their vaccination and obtain the vaccination certificate;  
-Record vaccination data, all data is saved;  
(to be continued in the next sprints)

## Usability

_Evaluates the user interface. It has several subcategories,
among them: error prevention; interface aesthetics and design; help and
documentation; consistency and standards._

-The worst-case time complexity analysis of the algorithms should be properly documented in the user manual of the application, that must be delivered with the application;  
(to be continued in the next sprints)

## Reliability
_Refers to the integrity, compliance and interoperability of the software. The requirements to be considered are: frequency and severity of failure, possibility of recovery, possibility of prediction, accuracy, average time between failures._

(No data available yet)

## Performance
_Evaluates the performance requirements of the software, namely: response time, start-up time, recovery time, memory consumption, CPU usage, load capacity and application availability._

-The computational complexity analysis, must be accompanied by the observation of the execution time of the algorithms for inputs of variable size, in order to observe the asymptotic behavior;  
(to be continued in the next sprints)

## Supportability
_The supportability requirements gathers several characteristics, such as:
testability, adaptability, maintainability, compatibility,
configurability, installability, scalability and more._

-Every method, beside Input/Output operations,  will have a unit test, making future maintenance easier;  
-Designed to easily support managing other future pandemic events;  
-Should be conceived having in mind that it can be further commercialized to other companies and/or organizations and/or healthcare systems besides DGS;  
-Users can change the language (Minimum: Portuguese/English), so people from everywhere can understand it;  
(to be continued in the next sprints)

## +

### Design Constraints

_Specifies or constraints the system design process. Examples may include: programming languages, software process, mandatory standards/patterns, use of development tools, class library, etc._

-The application graphical interface must be developed in JavaFX 11;  
-All the images/figures produced during the software development process should be recorded in SVG format;  
(to be continued in the next sprints)

### Implementation Constraints

_Specifies or constraints the code or construction of a system such
such as: mandatory standards/patterns, implementation languages,
database integrity, resource limits, operating system._

-Developed in Java, using the IntelliJ IDEA;  
-The unit tests should be implemented using the JUnit 5 framework. The JaCoCo plugin should be used to generate the coverage report;  
-The application should use object serialization to ensure data persistence between two runs of the application;  
(to be continued in the next sprints)

### Interface Constraints
_Specifies or constraints the features inherent to the interaction of the
system being developed with other external systems._

(No data available yet)

### Physical Constraints

_Specifies a limitation or physical requirement regarding the hardware used to house the system, as for example: material, shape, size or weight._

(No data available yet)