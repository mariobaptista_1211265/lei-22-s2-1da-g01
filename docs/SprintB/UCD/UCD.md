# Use Case Diagram (UCD)

**In the scope of this project, there is a direct relationship of _1 to 1_ between Use Cases (UC) and User Stories (US).**

However, be aware, this is a pedagogical simplification. On further projects and curricular units might also exist _1 to N **and/or** N to 1 relationships between US and UC.

**Insert below the Use Case Diagram in a SVG format**

![Use Case Diagram](UCD.svg)


**For each UC/US, it must be provided evidences of applying main activities of the software development process (requirements, analysis, design, tests and code). Gather those evidences on a separate file for each UC/US and set up a link as suggested below.**

# Use Cases / User Stories
| UC/US  | Description                                      |                   
|:-------|:-------------------------------------------------|
| US 003 | [Register a SNS user](../US03/US003.md)          |
| US 009 | [Register a Vaccination Center](../US09/US09.md) |
| US 010 | [To create a Task ](../US10/US10.md)             |
| US 004 | [ShortNameOfUS004](US004.md)                     |
| ...    | ...                                              |
| US 326 | [ShortNameOfUS326](US326.md)                     |
