# Supplementary Specification (FURPS+)

## Functionality

_Specifies functionalities that:_

- _are common across several US/UC;_
- _are not related to US/UC, namely: Audit, Reporting and Security._



-Users register process;
-Schedule their vaccination and obtain the vaccination certificate;
-Regist of vaccination data;
-Validation;
(continue....)



## Usability 

_Evaluates the user interface. It has several subcategories,
among them: error prevention; interface aesthetics and design; help and
documentation; consistency and standards._


(No data available yet)

## Reliability
_Refers to the integrity, compliance and interoperability of the software. The requirements to be considered are: frequency and severity of failure, possibility of recovery, possibility of prediction, accuracy, average time between failures._


(No data available yet)

## Performance
_Evaluates the performance requirements of the software, namely: response time, start-up time, recovery time, memory consumption, CPU usage, load capacity and application availability._


(No data available yet)

## Supportability
_The supportability requirements gathers several characteristics, such as:
testability, adaptability, maintainability, compatibility,
configurability, installability, scalability and more._ 



-Designed to easily support managing other future pandemic events;
-Should be conceived having in mind that it can be further commercialized to other companies and/or organizations and/or healthcare systems besides DGS.



## +

### Design Constraints

_Specifies or constraints the system design process. Examples may include: programming languages, software process, mandatory standards/patterns, use of development tools, class library, etc._
  

-Developed in Java, using the IntelliJ IDEA;
-The application graphical interface must be developed in JavaFX 11;
-The unit tests should be implemented using the JUnit 5 framework. The JaCoCo plugin should be used to generate the coverage report.


### Implementation Constraints

_Specifies or constraints the code or construction of a system such
such as: mandatory standards/patterns, implementation languages,
database integrity, resource limits, operating system._


(No data available yet)


### Interface Constraints
_Specifies or constraints the features inherent to the interaction of the
system being developed with other external systems._


(No data available yet)

### Physical Constraints

_Specifies a limitation or physical requirement regarding the hardware used to house the system, as for example: material, shape, size or weight._

(No data available yet)