# Use Case Diagram (UCD)

**In the scope of this project, there is a direct relationship of _1 to 1_ between Use Cases (UC) and User Stories (US).**

However, be aware, this is a pedagogical simplification. On further projects and curricular units might also exist _1 to N **and/or** N to 1 relationships between US and UC.

**Insert below the Use Case Diagram in a SVG format**

![Use Case Diagram](UCD.svg)


**For each UC/US, it must be provided evidences of applying main activities of the software development process (requirements, analysis, design, tests and code). Gather those evidences on a separate file for each UC/US and set up a link as suggested below.**

# Use Cases / User Stories
| UC/US | Description                                                                                                 |                   
|:------|:------------------------------------------------------------------------------------------------------------|
| US 03 | [Register a SNS user](../../SprintB/US03/US003.md)                                                          |
| US 09 | [Register a Vaccination Center](../../SprintB/US09/US09.md)                                                 |
| US 10 | [To create a Task ](../../SprintB/US10/US010.md)                                                            |
| US 11 | [Get List of Employees](../../SprintB/US11/US011.md)                                                        |
| US 11 | [Specify a new vaccine and its vaccine administration process](../../SprintB/US13/US13.md)                  |
| US 01 | [Schedule a Vaccine](../../SprintC/US01/US01.md)                                                            |
| US 02 | [To schedule a vaccination](../../SprintC/US02/US02.md)                                                     |
| US 04 | [Register the arrival of a SNS user](../../SprintC/US04/US04.md)                                            |
| US 05 | [Allow a nurse to consult the users in the waiting room of a vacination center](../../SprintC/US05/US05.md) |
| US 14 | [load a set of users from a CSV file](../../SprintC/US14/US14.md)                                           |
| US 16 | [Center Performance Analysis](../../SprintD/US16/US16.md)                                                   |