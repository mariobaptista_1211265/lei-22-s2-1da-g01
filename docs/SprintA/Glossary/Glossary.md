# Glossary

**Terms, Expressions and Acronyms (TEA) organized alphabetically (will be updated during all the sprints).**


| **_TEA_** (EN)  | **_TEA_** (PT) | **_Description_** (EN)                                           |                                       
|:------------------------|:-----------------|:--------------------------------------------|
| **Administrator** | **Administrador** | A person responsible for carrying out the administration of a business or organization. |
| **Algorithms** | **Algoritmos** | A process or set of rules to be followed in calculations or other problem-solving operations, especially by a computer. |
| **Coordinator** | **Coordenador** | A person whose job is to organize events or activities and to negotiate with others in order to ensure they work together effectively. |
| **Email** | **Email** | Messages distributed by electronic means from one computer user to one or more recipients via a network. |
| **Framework** | **Estrutura** | A basic structure underlying a system, concept, or text. |
| **GDH** | **DGS** | General Direction of Health. |
| **GHC** | **AGES** | Groupings of Health Centers. |
| **Health Care Center** | **Centro de Saúde** | A facility that provides (ambulatory) medical and sanitary services to a specific group in a population. |
| **Interface** | **Interface** | The place at which independent and often unrelated systems meet and act on or communicate with each other. |
| **Mass vaccination center** | **Centro de Vacinação** | Location, normally used for nonhealthcare activities, set up for high-volume and high-speed vaccinations during infectious disease emergencies. |
| **NHS** | **SNS** |  Nacional Health Service.|
| **Nurse** | **Enfermeiro** | A nurse is a caregiver for patients and helps to manage physical needs, prevent illness, and treat health conditions. |
| **Pandemic** | **Pandemia** | An outbreak of a disease that occurs over a wide geographic area (such as multiple countries or continents) and typically affects a significant proportion of the population. |
| **Process** | **Processo** | A series of actions or steps taken in order to achieve a particular end. |
| **Receptionist** | **Recepcionista** | A person who greets and deals with clients and visitors to a surgery, office, etc. |
| **RHA** | **ARS** | Regional Health Administration. |
| **Report** | **Relatório** | Give a spoken or written account of something that one has observed, heard, done, or investigated. |
| **Schedule**| **Agendar**| A plan for carrying out a process or procedure, giving lists of intended events and times. |
| **SMS** | **SMS** | Short Message Service, is a text messaging service component of most telephone, Internet, and mobile device systems. |
| **SVG** | **SVG** | Scalable Vector Graphics. |
| **UC** | **CU** | Use Cases. |
| **Unit tests** | **Testes unitários** | A way of testing a unit - the smallest piece of code that can be logically isolated in a system.|
| **User** | **Utilizador** | A person who uses or operates something.|
| **Vaccine** | **Vacina** | A substance used to stimulate the production of antibodies and provide immunity against one or several diseases. |
| **...** | **...** | ... |
| **...** | **...** | ... |











