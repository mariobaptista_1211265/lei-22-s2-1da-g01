# OO Analysis #

### _Conceptual Class Category List_ ###

---

**Roles of People or Organizations**

* NhsUser;
* Receptionist;
* Nurse;
* Administrator;
* CenterCoordinator.
    
---


**Places**

* Center;

---

**Noteworthy Events**

* VaccineSchedule;
* Vaccination;
* VacConfirmation;
* WaitingOrder;
* Notification;
* ProcessManagement;
* LeaveSMS.

---


**Physical Objects**

* Vaccine.

---


**Descriptions of Things**

*  VacType.



---


**Containers**

* Center;
* Employee.

---


**Elements of Containers**

* VaccinationCenter;
* HealthCareCenter.

---


**Organizations**

* ARS;
* AGES;
* DGS.

---

**Other External/Collaborating Systems**

*  Administration;


---


**Records of finance, work, contracts, legal matters**

* ProcessManagement;
* Certificate;

---


**Documents mentioned/used to perform some work/**

* Manual.

---



### **Rationale to identify associations between conceptual classes** ###

An association is a relationship between instances of objects that indicates a relevant connection and that is worth of remembering.


| Concept (A) 		|  Association   	|  Concept (B) |
|----------	   		|:-------------:		|------:       |
| Administrator | registers | NhsUser|
| Administrator | registers | Receptionist |
| Administrator | registers | Nurse |
| Administrator | registers | HealthCareCenter|
| Administrator | registers | VaccinationCenter|
| CenterCoordinator | is a | Employee |
| CenterCoordinator | manages | ProcessManagement |
| HealthCareCenter|part of| Center|
| NhsUser  	| requests  | Vaccination  |
| NhsUser | can request | Certificate |
| Notification | needs permission by | NhsUser|
| Notification | is a | Sms|
| Notification | is a | Email|
| Nurse	| is a     		| Employee  |
| Nurse| performs | Vaccination |
| Nurse | confirms | VacConfirmation|
| Nurse| emits | LeaveSms|
| Receptionist	| is a     		| Employee  |
| Receptionist	| registers    		| Vaccination  |
| Receptionist | creates | VaccineSchedule |
| Receptionist	| confirms   		| Vaccination  |
| Receptionist	| adds user to   		| WaitingList  |
| Vaccination | registered in | VaccineSchedule |
| Vaccination | done in | Center |
| Vaccination | confirmed at | VacConfirmation |
| VaccinationCenter| part of | Center|
| Vaccine | used in | Vaccination|
| Vaccine | described in | VacType|
| VaccineSchedule| emits | Notification|
| VaccineSchedule| organizes| Vaccination |
| VacConfirmation | confirmed by | Nurse |






## Domain Model

**Domain Model is down below on a SVG format.**

![DM_Domain_Model_Sprint_A.svg](DM_Domain_Model_Sprint_A.svg)



