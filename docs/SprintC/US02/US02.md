# US 02 - To schedule a vaccination

## 1. Requirements Engineering

As a **receptionist** at one vaccination center, I want to **schedule a vaccination**.

### 1.1. User Story Description

As a receptionist at one vaccination center, I want to schedule a vaccination.


### 1.2. Customer Specifications and Clarifications 

**From the specifications document:**

> Some users (e.g.: older ones) may want to go to a healthcare center to schedule the
vaccine appointment with the help of a receptionists at one vaccination center.


**From the client clarifications:**

> **Question:** <br>I would like to know if a receptionist has the ability to schedule an appointment in different vaccination centres or only on their own.
>
> **Answer:** <br>The receptionist has the ability to schedule the vaccine in any vaccination center. The receptionist should ask the SNS user to indicate/select the preferred vaccination center.

> **Question:** <br>When a receptionist schedules a vaccination for an SNS user, should they be presented with a list of available vaccines (brands, that meet acceptance criteria) from which to choose? Or should the application suggest only one?
>
> **Answer:** <br>The receptionist do not select the vaccine brand.
When the user is at the vaccination center to take the vaccine, the nurse selects the vaccine. In Sprint D we will introduce new USs where the nurse records the administration of a vaccine to a SNS user.

> **Question:** <br>I would like to ask if you cloud tell me if my interpretation of this US was correct.
So my interpretation was that the receptionist whould choose a vaccination center and than in that vacination center she would schedule the second dosage of the vacinne
> 
> **Answer:** <br>The goal of this US is to schedule a vaccination for a SNS user. The SNS user should go to a vaccination center and a receptionist should use the application to schedule a vaccination for the SNS user. The receptionist should ask the SNS user for data required to schedule a vaccination. The data needed to schedule a vaccination is the same required in US01. Please check the Project Description available in moodle.


> **Question:** <br>We are unsure if it's in this user stories that's asked to implement the "send a SMS message with information about the scheduled appointment" found on the Project Description available in moodle. Could you clarify?
>
> **Answer:** <br>In a previous clarification that I made on this forum, I said: "[The user should receive a] SMS Message to warn of a scheduling [and the message] should include: Date, Time and vaccination center". Teams must record the answers!
A file named SMS.txt should be used to receive/record the SMS messages. We will not use a real word service to send SMSs.

> **Question:** <br>For the US1, the acceptance criteria is: A SNS user cannot schedule the same vaccine more than once. For the US2, the acceptance criteria is: The algorithm should check if the SNS User is within the age and time since the last vaccine.
<br>[1] Are this acceptance criteria exclusive of each US or are implemented in both?
<br>[2] To make the development of each US more clear, could you clarify the differences between the two US?
>
> **Answer:** <br>1- The acceptance criteria for US1 and US2 should be merged. The acceptance criteria por US1 and US2 is: A SNS user cannot schedule the same vaccine more than once. The algorithm should check if the SNS User is within the age and time since the last vaccine."
<br>2- In US1 the actor is the SNS user, in US2 the actor is the receptionist. In US1 the SNS user is already logged in the system and information that is required and that exists in the system should be automatically obtained. In US2 the receptionist should ask the SNS user for the information needed to schedule a vaccination. Information describing the SNS user should be automatically obtained by introducing the SNS user number.

### 1.3. Acceptance Criteria

The algorithm should check if the SNS User is within the
age and time since the last vaccine.

### 1.4. Found out Dependencies

This User Story is dependent on the following User Stories:<br>
-US03 (*As a receptionist, I want to register a SNS user.*)<br>
-US09 (*As an administrator, I want to register a vaccination center to respond to a certain
pandemic.*)<br>
-US10 (*As an administrator, I want to register an Employee.*)<br>
-US12 (*As an administrator, I intend to specify a new vaccine type.*)<br>
-US13 (*As an administrator, I intend to specify a new vaccine and its administration
process.*)<br>

### 1.5 Input and Output Data

**Input Data:**

* Typed data:
    * SNS number
    * Date and time of the appointment

* Selected data:
    * Choosing Vaccination Center
    * Choosing Vaccine type

**Output Data:**

* (In)Success of the operation

### 1.6. System Sequence Diagram (SSD)

*Insert here a SSD depicting the envisioned Actor-System interactions and throughout which data is inputted and outputted to fulfill the requirement. All interactions must be numbered.*

![US02-SSD](US02_SSD.svg)


### 1.7 Other Relevant Remarks

The receptionist must be logged in to schedule a vaccination.

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 
*In this section, it is suggested to present an excerpt of the domain model that is seen as relevant to fulfill this requirement.* 

![US02-MD](US02_MD.svg)

### 2.2. Other Remarks

*n/a*


## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for...                             | Answer                    | Justification (with patterns)                                                                                                             |
|:---------------|:------------------------------------------------------------------------|:--------------------------|:------------------------------------------------------------------------------------------------------------------------------------------|
| Step 1  		     | ...interacting with the Actor                                           | ScheduleVaccineUI         | **Pure Fabrication:** there is no reason to assign this responsibility to any existing class in the Domain Model.                         |
| 		             | ...coordinating the US	                                                 | ScheduleVaccineController | **Controller**                                                                                                                            |
| Step 2  		     | 						                                                                  |                           |                                                                                                                                           |
| Step 3  		     | ...saving the typed data?                                               | VaccineSchedule           | **IE:** Knows its own data 						                                                                                                         |             |                              |
| 			  		        | 	...knows NHSUserStore?                                                 | Company                   | **IE:** Company knows the ClientStore to which it is delegating some tasks                                                                | 
| 			  		        | 	...validating the existence of the SNS number                          | NHSUserStore              | **IE:** knows all the NHS users                                                                                                           | 
| Step 4  		     | 	...providing the list of all the centers						                         | CenterStore               | **IE:** knows all the centers                                                                                                             |
| Step 5  		     | ...saving the center choice?                                            | VaccineSchedule           | **IE:** Knows its own data 						                                                                                                         |             |                              |
| Step 6  		     | 	...providing the list of all the vaccine types			                      | VacType                   | **IE:** Knows all the vaccination types                                                                                                   |              
| Step 7  		     | ...saving the vaccine type choice?                                      | VaccineSchedule           | **IE:** Knows its own data 						                                                                                                         |
| Step 8  		     | 	...saving the typed data						                                         | VaccineSchedule           | **IE:** Knows its own data                                                                                                                |
| Step 9  		     | ...confirm the user is within the age and time since the last vaccine.	 | ScheduleVaccineController | **IE:** Has all the information required to confirm the user can take the vaccine                                                         |
| Step 10  		    | 	...saving the scheduled vaccine						                                  | VaccineScheduleStore      | **IE:** Is responsible for saving all the scheduled vaccines                                                                              |
| Step 11  		    | 							                                                                 |                           |                                                                                                                                           |  
| Step 12        | ...sending the SMS message                                              | ScheduleVaccineController | **IE:** Has all the information necessary to perform this task and there is no reason to attribute this responsibility to any other class |        

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * VaccineSchedule
 * Company
 * VacType

Other software classes (i.e. Pure Fabrication) identified: 
 * ScheduleVaccinationUI  
 * ScheduleVaccinationController
 * VaccineScheduleStore
 * NHSUserStore
 * CenterStore

## 3.2. Sequence Diagram (SD)

*In this section, it is suggested to present an UML dynamic view stating the sequence of domain related software objects' interactions that allows to fulfill the requirement.* 

![US02-SD](US02_SD.svg)

## 3.3. Class Diagram (CD)

*In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods.*

![USCD](US02_CD.svg)

# 4. Tests 
*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling.* 

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**Test 1:** Check that it is not possible to create an instance of the Example class with null values. 

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

*It is also recommended to organize this content by subsections.* 

# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in accordance with the previously carried out design. Furthermore, it is recommeded to mention/describe the existence of other relevant (e.g. configuration) files and highlight relevant commits.*

*It is also recommended to organize this content by subsections.* 

# 6. Integration and Demo 

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*


# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*





