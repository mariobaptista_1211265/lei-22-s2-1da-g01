package app.tests;

import app.controller.App;
import app.domain.model.NumberOfUsersVaccinated;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static org.junit.Assert.*;

public class NumberOfUsersVaccinatedTest {

    @Test
    public void run() {
        App.getInstance();
        File f = new File("VacinationsPerDay.csv");
        if(f.exists()){
            f.delete();
        }
        new NumberOfUsersVaccinated().run();
        assertTrue(f.exists());
        int n = 0;
        try(Scanner sc = new Scanner(f)){
            while(sc.hasNextLine()){
                String line = sc.nextLine();
                if (!line.matches("(.*,.*)*") && !line.matches("(.*;.*)*")) {
                    fail();
                }
                if (!line.isEmpty() || !line.isBlank()) {
                    n++;
                }
            }
        } catch (FileNotFoundException e) {
            fail();
        }
        assertEquals(n, App.getInstance().getCompany().getCenterStore().getAll().size() + 1);

    }
}