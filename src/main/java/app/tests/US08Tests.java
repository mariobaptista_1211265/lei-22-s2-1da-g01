package app.tests;



import app.controller.App;
import app.controller.US03Controller;
import app.domain.model.UserArrival;
import app.domain.model.WaitingRoom;
import app.ui.console.stores.NHSUserStore;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class US08Tests {

    @BeforeTest
    public void setUp() {
        UserArrival ua = new UserArrival(1, 987654321);
        WaitingRoom.addUserArrival(ua);

        UserArrival ua2 = new UserArrival(1, 987654322);
        WaitingRoom.addUserArrival(ua2);

        UserArrival ua3 = new UserArrival(3, 987654323);
        WaitingRoom.addUserArrival(ua3);


        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        try {
            date = df.parse("02-12-2000");
        } catch (ParseException ignore) {
        }
        US03Controller ctrl = new US03Controller(App.getInstance());
        ctrl.createNHSUser("Inês Maria Alves Ramalho", "Rua da Alegria", "Female", 987654321, "nhsuser@lei.sem2.pt", date, 987654321, 123456789);
        ctrl.saveNHSUser();

    }

    @Test
    public void checkIfUserExistsInWaitingRoom(){

        // checks if user 987654321 exists in waiting room of center 1

        Assert.assertTrue(WaitingRoom.checkIfUserExistsInWaitingRoom(1, 987654321));

    }


    @Test
    public void checkIfUserDoesNotExistsInWaitingRoom(){

        // checks if user 987654322 does not exist in waiting room of center 2

        Assert.assertFalse(WaitingRoom.checkIfUserExistsInWaitingRoom(2, 987654322));

    }


    @Test
    public void getSnsPhoneNumber(){

        // checks if sns phone number of user 987654321 is 987654321

        Assert.assertTrue(NHSUserStore.getPhoneNumberBySNS(987654321) == 987654321);

    }

    @Test
    public void FailGettingThePhoneNumber(){

        // checks if sns phone number of user 987654321 is NOT 123456789

        Assert.assertFalse(NHSUserStore.getPhoneNumberBySNS(987654321) == 123456789);

    }

}




