package app.tests;

import app.controller.App;
import app.controller.US14Controller;
import app.domain.dto.NHSUserDto;
import app.domain.model.NHSUser;
import app.ui.console.stores.NHSUserStore;
import org.junit.Test;
import org.testng.Assert;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class US14ControllerTest {
    private US14Controller ctrl = new US14Controller(App.getInstance());
    private NHSUserStore stre = App.getInstance().getCompany().getNHSUserStore();

    @Test
    public void testIsFileValid() {
        // File exists and is csv
        Assert.assertTrue(ctrl.isFileValid("tests/fileTests/fileWithHeader.csv"));
        // File exists but isn't csv
        Assert.assertFalse(ctrl.isFileValid("tests/fileTests/notCSV.txt"));
        // File doesn't exist
        Assert.assertFalse(ctrl.isFileValid("tests/fileTests/file.csv"));
    }

    @Test
    public void testGetUsersFromFile() throws FileNotFoundException, ParseException {
        List<NHSUserDto> listFromFile;
        List<NHSUserDto> correctList = new ArrayList<NHSUserDto>();
        correctList.add(new NHSUserDto("Rogerio","4410-432","male",943215643,"rogerio@gmail.com",new Date(12/10/2002),331232131,555435231));
        correctList.add(new NHSUserDto("Roberto","4410-432","male",943215643,"roberto@gmail.com",new Date(02/11/2005),111232131,785435231));
        //Test File with Header
        listFromFile = ctrl.getUsersFromFile("tests/fileTests/fileWithHeader.csv");
        Assert.assertNotEquals(listFromFile,correctList);
        listFromFile = ctrl.getUsersFromFile("tests/fileTests/fileWithoutHeader.csv");
        Assert.assertNotEquals(listFromFile,correctList);
    }

    @Test
    public void testSaveUsersList() {
        List<NHSUserDto> listAddUsers = new ArrayList<NHSUserDto>();

        NHSUserDto userDto1 = new NHSUserDto("Rogerio","4410-432","male",943215643,"rogerio@gmail.com",new Date(12/10/2002),331232131,555435231);
        NHSUserDto userDto2 = new NHSUserDto("Roberto","4410-432","male",943215643,"roberto@gmail.com",new Date(02/11/2005),111232131,785435231);

        listAddUsers.add(userDto1);
        listAddUsers.add(userDto2);

        List<NHSUser> firstList = stre.getNHSUsers();
        firstList.add(stre.Create(userDto1));
        firstList.add(stre.Create(userDto2));

        ctrl.saveUsersList(listAddUsers);

        Assert.assertEquals(stre.getNHSUsers(),firstList);


    }
}
