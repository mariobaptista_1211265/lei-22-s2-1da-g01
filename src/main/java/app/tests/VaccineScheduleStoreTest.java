package app.tests;

import app.controller.App;
import app.domain.model.VaccineSchedule;
import app.ui.console.stores.VaccineScheduleStore;
import org.junit.Test;
import org.testng.Assert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class VaccineScheduleStoreTest {


    @Test
    public void doesUserScheduleExistInCenter() {
        App app = new App();
        VaccineScheduleStore store = app.getCompany().getScheduleStore();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        try {
            date = df.parse("30-05-2022 01:00");
        } catch (ParseException ignore) {
        }
        VaccineSchedule vs = new VaccineSchedule(987654321, 1, date, "01:00", "Covid19");
        VaccineScheduleStore.addScheduledVaccine(vs);

        Assert.assertTrue(VaccineScheduleStore.doesUserScheduleExistInCenter(987654321, 1));
        Assert.assertFalse(VaccineScheduleStore.doesUserScheduleExistInCenter(333666999, 99));
    }

    @Test
    public void getAScheduledVaccine() {
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        try {
            date = df.parse("30-05-2022");
        } catch (ParseException ignore) {
        }
        VaccineSchedule vs = new VaccineSchedule(160358479, 99, date, "01:00", "Covid19");
        VaccineScheduleStore.addScheduledVaccine(vs);
        Assert.assertEquals(VaccineScheduleStore.getAScheduledVaccine(160358479, 99), vs);
        Assert.assertNotEquals(VaccineScheduleStore.getAScheduledVaccine(987654321, 1), vs);
    }
}