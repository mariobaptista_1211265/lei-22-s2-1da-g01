package app.tests;

import app.controller.App;
import app.controller.US03Controller;
import app.controller.US04Controller;
import app.domain.dto.CenterDto;
import app.domain.model.*;
import app.ui.console.stores.CenterStore;
import app.ui.console.stores.VaccineScheduleStore;
import org.junit.Test;
import org.testng.Assert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WaitingRoomTest {

    @Test
    public void checkDuplicatedEntry(){
        App app = new App();

        Company company = app.getCompany();

        US03Controller ctrl = new US03Controller(app);
        US04Controller ctrl2 = new US04Controller(app);

        // Center
        CenterDto testVacDto = new CenterDto(99,"Centro de vacinacao em massa do Porto", "Rua do Porto", 123456789, "centrodoporto@gmail.com", "6","23","5",3,75);
        VaccinationCenter testVac = new VaccinationCenter(testVacDto);
        testVac.setVACVaccinetype("Covid19");
        CenterStore.SaveVac(testVac);

        // NHS User

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        try {
            date = df.parse("02-12-2000");
        } catch (ParseException ignore) {
        }

        ctrl.createNHSUser("User 1", "Rua da Alegria", "Male", 987654321, "nhsuser@lei.sem2.pt", date, 160358479, 654789123);
        ctrl.saveNHSUser();

        // Vaccine Schedule

        SimpleDateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date2 = null;
        try {
            date2 = df2.parse("30-05-2022 17:00:00");
        } catch (ParseException ignore) {
        }
        VaccineSchedule vs = new VaccineSchedule(160358479, 99, date2, "01:00", "Covid19");
        VaccineScheduleStore.addScheduledVaccine(vs);



        // WaitingRoom

        UserArrival arrival = new UserArrival(99,160358479);
        WaitingRoom.addUserArrival(arrival);
        Assert.assertFalse(WaitingRoom.checkDuplicatedEntry(99,160358479));
        Assert.assertTrue(WaitingRoom.checkDuplicatedEntry(99, 934962104));
    }

    @Test
    public void checkIfSameDay(){

        App app = new App();

        Company company = app.getCompany();

        US03Controller ctrl = new US03Controller(app);
        US04Controller ctrl2 = new US04Controller(app);

        // Center
        CenterDto testVacDto = new CenterDto(99,"Centro de vacinacao em massa do Porto", "Rua do Porto", 123456789, "centrodoporto@gmail.com", "6","23","5",3,75);
        VaccinationCenter testVac = new VaccinationCenter(testVacDto);
        testVac.setVACVaccinetype("Covid19");
        CenterStore.SaveVac(testVac);

        // NHS User

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        try {
            date = df.parse("02-12-2000");
        } catch (ParseException ignore) {
        }

        ctrl.createNHSUser("User 1", "Rua da Alegria", "Male", 987654321, "nhsuser@lei.sem2.pt", date, 160358479, 654789123);
        ctrl.saveNHSUser();

        // Vaccine Schedule

        SimpleDateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date2 = null;
        try {
            date2 = df2.parse("30-05-2022 17:00:00");
        } catch (ParseException ignore) {
        }
        VaccineSchedule vs = new VaccineSchedule(160358479, 99, date2, "01:00", "Covid19");
        VaccineScheduleStore.addScheduledVaccine(vs);



        // WaitingRoom

        UserArrival arrival = new UserArrival(99,160358479);
        WaitingRoom.addUserArrival(arrival);

        SimpleDateFormat df1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date test = null;
        try {
            test = df1.parse("10-05-2022 13:00:00");
        } catch (ParseException ignore) {
        }

        Date now = new Date();

        Assert.assertFalse(WaitingRoom.checkIfSameDay(test));
        Assert.assertTrue(WaitingRoom.checkIfSameDay(now));
    }


}
