package app.tests;

import app.ui.console.stores.VaccineStore;
import org.junit.Test;
import org.testng.Assert;

public class VaccineStoreTest {
    @Test
    public void checkDups() {
        VaccineStore.SaveVaccine(VaccineStore.CreateVaccine("Gensen"));
        Assert.assertFalse(VaccineStore.CheckDups("Gensen"));
        Assert.assertTrue(VaccineStore.CheckDups("Pfeizer"));
    }

    @Test
    public void checkDups2() {
        VaccineStore.SaveVaccine(VaccineStore.CreateVaccine("Gensen"));
        Assert.assertFalse(VaccineStore.CheckDups2("Gensen"));
        Assert.assertTrue(VaccineStore.CheckDups2("Pfeizer"));
    }
}
