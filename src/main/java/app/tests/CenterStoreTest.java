package app.tests;

import app.domain.dto.CenterDto;
import app.domain.model.HealthCareCenter;
import app.domain.model.VaccinationCenter;
import app.ui.console.stores.CenterStore;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * The test for the center store methods.
 *
 * @author João Fortuna <1211261@isep.ipp.pt>
 */
public class CenterStoreTest {

    /**
     * Test to Check unique vac phone.
     */
    @Test
    public void checkUniqueVacPhone(){
        CenterDto testVacDto = new CenterDto(1,"João Fortuna", "Porto", 123456789, "joao@gmail.com", "1","1","1",1,1);

        VaccinationCenter testVac = new VaccinationCenter(testVacDto);

        CenterStore.SaveVac(testVac);
        Assert.assertTrue(CenterStore.checkUniqueVacPhone(987654321));
        Assert.assertFalse(CenterStore.checkUniqueVacPhone(123456789));
    }

    /**
     * Test to Check unique health phone.
     */
    @Test
    public void checkUniqueHealthPhone(){
        CenterDto testVacDto = new CenterDto(2,"João Fortuna", "Porto", 123456789, "joao@gmail.com", "1","1","1",1,1);

        HealthCareCenter testVac = new HealthCareCenter(testVacDto,"1","1");

        CenterStore.SaveHealth(testVac);
        Assert.assertTrue(CenterStore.checkUniqueHealthPhone(987654321));
        Assert.assertFalse(CenterStore.checkUniqueHealthPhone(123456789));
    }

    /**
     * Test to Check unique vac email.
     */
    @Test
    public void checkUniqueVacEmail(){
        CenterDto testVacDto = new CenterDto(2,"João Fortuna", "Porto", 123456789, "joao@gmail.com", "1","1","1",1,1);

        VaccinationCenter testVac = new VaccinationCenter(testVacDto);

        CenterStore.SaveVac(testVac);
        Assert.assertTrue(CenterStore.checkUniqueVacEmail("oaoj@gmail.com"));
        Assert.assertFalse(CenterStore.checkUniqueVacEmail("joao@gmail.com"));
    }

    /**
     * Test to Check unique health email.
     */
    @Test
    public void checkUniqueHealthEmail(){
        CenterDto testVacDto = new CenterDto(1,"João Fortuna", "Porto", 123456789, "joao@gmail.com", "1","1","1",1,1);

        HealthCareCenter testVac = new HealthCareCenter(testVacDto,"1","1");

        CenterStore.SaveHealth(testVac);
        Assert.assertTrue(CenterStore.checkUniqueHealthEmail("oaoj@gmail.com"));
        Assert.assertFalse(CenterStore.checkUniqueHealthEmail("joao@gmail.com"));
    }
}
