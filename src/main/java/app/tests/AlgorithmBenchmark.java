package app.tests;

import app.controller.Sum;

import java.util.Random;

public class AlgorithmBenchmark {

    static Random random = new Random();
    public static void main(String args[]){

        int[] list;
        long startTime = System.currentTimeMillis();
        long endTime = System.currentTimeMillis();
        for(int i = 1; i<=20; i++){
            list = generateRandoms(2000*i,1000);
            startTime = System.currentTimeMillis();
            Sum.max(list);
            endTime = System.currentTimeMillis();
            System.out.println("Total execution time: " + (endTime-startTime) + "ms ");
        }

    }

    private static int[] generateRandoms(int size,  int max){
        int[] list = new int[size];
        for (int i = 0; i<size; i++){
            list[i] = random.nextInt(max);
        }
        return list;
    }

}
