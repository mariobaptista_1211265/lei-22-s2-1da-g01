package app.tests;

import app.ui.console.stores.VaccineAdmStore;
import org.junit.Test;
import org.testng.Assert;

public class VaccineAdmStoreTest {
    @Test
    public void checkDupsAdm() {
        VaccineAdmStore.saveProcess("Gensen","Covid19", 1, 1,1,1,1);
        Assert.assertFalse(VaccineAdmStore.CheckDupsAdm("Gensen","Covid19"));
        Assert.assertTrue(VaccineAdmStore.CheckDupsAdm("Gensen","Hepatite A"));
    }
}
