package app.tests;

import app.controller.App;
import app.domain.dto.EmployeeDto;
import app.ui.console.stores.EmployeeStore;
import org.testng.Assert;
import org.testng.annotations.Test;
import pt.isep.lei.esoft.auth.AuthFacade;

/**
 * The type Employee store test.
 */
public class EmployeeStoreTest {

    /**
     * Check if the email is valid.
     */
    @Test
    public void CheckEmail(){
        EmployeeDto dtoTest = new EmployeeDto( "Roberto","Rua de Serzedo",123456789,"123@gmail.com",111222333,"4410-011","Serzedo City","admin", "password");
        EmployeeStore.Save(EmployeeStore.Create(dtoTest), new AuthFacade());
        Assert.assertFalse(EmployeeStore.existsEmail("123@gmail.com"));
        Assert.assertTrue(EmployeeStore.existsEmail("1234@gmail.com"));
        Assert.assertFalse(EmployeeStore.existsEmail("1234gmail.com"));
        Assert.assertFalse(EmployeeStore.existsEmail("1234@gmailcom"));
        Assert.assertFalse(EmployeeStore.existsEmail("1234gmailcom"));
    }

    /**
     * Check if the phone number is valid.
     */
    @Test
    public void CheckPhone(){
        EmployeeDto dtoTest = new EmployeeDto( "Roberto","Rua de Serzedo",123456789,"123@gmail.com",111222333,"4410-011","Serzedo City","admin", "password");
        EmployeeStore.Save(EmployeeStore.Create(dtoTest), new AuthFacade());
        Assert.assertFalse(EmployeeStore.existsPhone(123456789));
        Assert.assertTrue(EmployeeStore.existsPhone(237756789));
        Assert.assertFalse(EmployeeStore.existsPhone(23775678));
    }

    /**
     * Check if the Citizen Card Number is valid.
     */
    @Test
    public void CheckCCN(){
        EmployeeDto dtoTest = new EmployeeDto( "Roberto","Rua de Serzedo",123456789,"123@gmail.com",111222333,"4410-011","Serzedo City","admin", "password");
        EmployeeStore.Save(EmployeeStore.Create(dtoTest), new AuthFacade());
        Assert.assertFalse(EmployeeStore.existsCitizenCardNumber(111222333));
    }

    /**
     * Check if the role exists.
     */
    @Test
    public void CheckRoleExists(){
        App.getInstance().getCompany().getAuthFacade().addUserRole("teste","123");
        Assert.assertTrue(EmployeeStore.roleExists("teste"));
    }
}
