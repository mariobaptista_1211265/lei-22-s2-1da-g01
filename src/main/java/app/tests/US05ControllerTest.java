package app.tests;

import app.controller.App;
import app.controller.US05Controller;
import app.domain.dto.WaitingRoomDTO;
import app.domain.model.UserArrival;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class US05ControllerTest {
    /**
     * The Ctrl.
     */
    US05Controller ctrl = new US05Controller(App.getInstance());

    /**
     * Show waiting room from center.
     */
    @Test
    public void showWaitingRoomFromCenter() {

        int idCenter = 1;

        List<UserArrival> waitingList =  App.getInstance().getCompany().getWaitingRoom().getWaitingList();
        List<WaitingRoomDTO> results = new ArrayList<>();
        for(UserArrival user : waitingList){
            if(user.getCenterID() == idCenter){
                WaitingRoomDTO w = new WaitingRoomDTO(App.getInstance().getCompany().getNHSUserStore().findByNumber(user.getUserNumber()).getName(),user.getDateOfArrival());
                results.add(w);
            }
        }
        Assert.assertEquals(ctrl.showWaitingRoomFromCenter(idCenter), results);

    }
}
