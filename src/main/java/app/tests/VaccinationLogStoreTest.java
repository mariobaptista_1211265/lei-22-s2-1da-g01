package app.tests;

import app.controller.App;
import app.domain.dto.VaccinationDTO;
import app.domain.model.VacAdministration;
import app.ui.console.stores.VaccinationLogStore;
import org.junit.Test;
import org.testng.Assert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class VaccinationLogStoreTest {

    private List<VacAdministration>  listToCompare = new ArrayList<VacAdministration>();
    private List<String> listTest = new ArrayList<String>();
    private VaccinationLogStore stre = App.getInstance().getCompany().getLogStore();
    private SimpleDateFormat dateFormatter =new SimpleDateFormat("dd-MM-yyyy");
    private Date dayToTest = dateFormatter.parse("12-11-2004");
    private VacAdministration vacAdmToAdd = new VacAdministration(new VaccinationDTO(123, "123", 2, "12", new Date(), new Date(), new Date(), new Date(), 1000345678) );

    private int centerIDToTest = (new Random()).nextInt(10000);

    public VaccinationLogStoreTest() throws ParseException {
    }

    @Test
    public void testSaveRecord(){
        listToCompare = stre.getAllVaccinationlogs();
        VaccinationLogStore.saveRecord(vacAdmToAdd);
        listToCompare.add(vacAdmToAdd);
        Assert.assertEquals(listToCompare, stre.getAllVaccinationlogs());
    }
    @Test
    public void testGetVacLogsOfDayByCenter() throws ParseException {
        listToCompare = stre.getVacLogsOfDayByCenter(new Date(),1000345678);
        System.out.println(listToCompare.size());
        VaccinationLogStore.saveRecord(vacAdmToAdd);
        listToCompare.add(vacAdmToAdd);
        Assert.assertEquals(listToCompare, stre.getVacLogsOfDayByCenter(new Date(),1000345678));
    }
    @Test
    public void testGetDatesWithRecordsByCenter(){
        listTest = stre.getDatesWithRecordsByCenter(centerIDToTest);
        vacAdmToAdd.setCenterID(centerIDToTest);
        VaccinationLogStore.saveRecord(vacAdmToAdd);
        listTest.add(dateFormatter.format(new Date()));
        Assert.assertEquals(listTest, stre.getDatesWithRecordsByCenter(centerIDToTest));
    }
    @Test
    public void testDifferenceInTime(){
        Calendar timeCounterBefore = Calendar.getInstance();
        timeCounterBefore.setTime(new Date());
        timeCounterBefore.set(Calendar.HOUR_OF_DAY, 1);
        timeCounterBefore.set(Calendar.MINUTE, 0);
        timeCounterBefore.set(Calendar.SECOND, 0);

        Calendar timeCounterAfter;
        timeCounterAfter = Calendar.getInstance();
        timeCounterAfter.setTime(new Date());
        timeCounterAfter.set(Calendar.HOUR_OF_DAY, 1);
        timeCounterAfter.set(Calendar.MINUTE, 10);
        timeCounterAfter.set(Calendar.SECOND, 10);

        Calendar timeToCompareArrival = Calendar.getInstance();
        Calendar timeToCompareLeft = Calendar.getInstance();

        timeToCompareArrival.set(Calendar.HOUR_OF_DAY, 1);
        timeToCompareArrival.set(Calendar.MINUTE, 8);
        timeToCompareArrival.set(Calendar.SECOND, 8);

        vacAdmToAdd.setArrivalDateTime(timeToCompareArrival.getTime());
        listToCompare.add(vacAdmToAdd);

        int compare = stre.differenceInTime(listToCompare,timeCounterBefore,timeCounterAfter,timeToCompareArrival,timeToCompareLeft);
        Assert.assertEquals(compare,1);
    }

}
