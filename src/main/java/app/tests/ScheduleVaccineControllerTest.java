package app.tests;

import app.controller.ScheduleVaccineController;
import org.junit.Test;
import org.testng.Assert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScheduleVaccineControllerTest {
    @Test
    public void validatedate() throws ParseException {
        String dateString = "22-07-2023 15:30";
        Date date = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(dateString);
        Assert.assertTrue(ScheduleVaccineController.validatedate(date));
        dateString="22-03-2020 15:30";
        date=new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(dateString);
        Assert.assertFalse(ScheduleVaccineController.validatedate(date));
    }

    @Test
    public void verifyhours() {
        Assert.assertTrue(ScheduleVaccineController.verifyhours("14:30"));
        Assert.assertFalse(ScheduleVaccineController.verifyhours("26:30"));
        Assert.assertFalse(ScheduleVaccineController.verifyhours("14:70"));
    }
}
