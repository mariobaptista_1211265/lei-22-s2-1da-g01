package app.tests;

import app.controller.App;
import app.controller.US11Controller;
import org.junit.Assert;
import org.junit.Test;
import pt.isep.lei.esoft.auth.mappers.dto.UserRoleDTO;

import java.util.ArrayList;
import java.util.List;

public class US011ControllerTest {
    /**
     * The Ctrl.
     */
    US11Controller ctrl = new US11Controller(App.getInstance());

    /**
     * Gets list of roles that exist.
     */
    @Test
    public void getListOfRolesThatExist() {
        List<String> s = new ArrayList<>();
        for (UserRoleDTO urdto : App.getInstance().getCompany().getAuthFacade().getUserRoles()) {
            s.add(urdto.getDescription());
        }
        Assert.assertEquals(ctrl.getListOfRolesThatExist(), s);

        App.getInstance().getCompany().getAuthFacade().addUserRole("Dummy", "JNUIT");
        s.add("JNUIT");
        Assert.assertEquals(ctrl.getListOfRolesThatExist(), s);
    }


    /**
     * Gets list of users with role.
     */
    @Test
    public void getListOfUsersWithRole() {
        List<String> real = new ArrayList<>();
        List<String> actual = new ArrayList<>();
        App.getInstance().getCompany().getAuthFacade().getUsersWithRole("ADMINISTRATOR").forEach(s -> real.add(s.getId()));
        ctrl.getListOfUsersWithRole("ADMINISTRATOR").forEach(s -> actual.add(s.getId()));
        Assert.assertEquals(real, actual);

        App.getInstance().getCompany().getAuthFacade().addUserWithRole("JUNIT", "JUNITO@isep.ipp.pt", "Qwsd!2@", "ADMINISTRATOR");
        List<String> real2 = new ArrayList<>();
        List<String> actual2 = new ArrayList<>();
        App.getInstance().getCompany().getAuthFacade().getUsersWithRole("ADMINISTRATOR").forEach(s -> real2.add(s.getId()));
        ctrl.getListOfUsersWithRole("ADMINISTRATOR").forEach(s -> actual2.add(s.getId()));
        Assert.assertEquals(real2, actual2);
    }
}
