package app.tests;

import app.domain.dto.NHSUserDto;
import app.domain.model.NHSUser;
import app.ui.console.stores.NHSUserStore;
import org.testng.Assert;
import org.testng.annotations.Test;
import pt.isep.lei.esoft.auth.AuthFacade;

import java.util.Date;

/**
 * The type Nhs user store test.
 *
 * @author Mário Baptista nº1211265
 */
public class NHSUserStoreTest {

    /**
     * Check unique email.
     */
    @Test
    public void checkUniqueEmail() {
        NHSUserDto testNhsUserDto = new NHSUserDto("Mário", "Rua da alegria 74", 966412563, "email@exemplo.com", new Date(), 997645312, 748596123);

        NHSUser testNHSUser = new NHSUser(testNhsUserDto);

        NHSUserStore.Save(testNHSUser, new AuthFacade());
        Assert.assertTrue(NHSUserStore.checkUniqueEmail("emailquenaoexiste@gmail.com"));
        Assert.assertFalse(NHSUserStore.checkUniqueEmail("email@exemplo.com"));
    }

    /**
     * Check unique phone.
     */
    @Test
    public void checkUniquePhone() {
        NHSUserDto testNhsUserDto = new NHSUserDto("Mário", "Rua da alegria 74", 966412563, "email@exemplo.com", new Date(), 997645312, 748596123);

        NHSUser testNHSUser = new NHSUser(testNhsUserDto);

        NHSUserStore.Save(testNHSUser, new AuthFacade());
        Assert.assertTrue(NHSUserStore.checkUniquePhone(966401252));
        Assert.assertFalse(NHSUserStore.checkUniquePhone(966412563));
    }

    /**
     * Check unique sns number.
     */
    @Test
    public void checkUniqueSNSNumber() {
        NHSUserDto testNhsUserDto = new NHSUserDto("Mário", "Rua da alegria 74", 966412563, "email@exemplo.com", new Date(), 997645312, 748596123);

        NHSUser testNHSUser = new NHSUser(testNhsUserDto);

        NHSUserStore.Save(testNHSUser, new AuthFacade());
        Assert.assertTrue(NHSUserStore.checkUniqueSNSNumber(123123420));
        Assert.assertFalse(NHSUserStore.checkUniqueSNSNumber(997645312));
    }

    /**
     * Check unique citizen card number.
     */
    @Test
    public void checkUniqueCitizenCardNumber() {
        NHSUserDto testNhsUserDto = new NHSUserDto("Mário", "Rua da alegria 74", 966412563, "email@exemplo.com", new Date(), 997645312, 748596123);

        NHSUser testNHSUser = new NHSUser(testNhsUserDto);

        NHSUserStore.Save(testNHSUser, new AuthFacade());
        Assert.assertTrue(NHSUserStore.checkUniqueCitizenCardNumber(420420123));
        Assert.assertFalse(NHSUserStore.checkUniqueCitizenCardNumber(748596123));
    }
}