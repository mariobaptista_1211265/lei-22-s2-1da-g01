package app.tests;

import app.controller.App;
import app.controller.US16Controller;
import app.domain.shared.Constants;
import org.junit.Test;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class US16ControllerTest {

    private US16Controller ctrl = new US16Controller(App.getInstance());
    private Properties props = App.getInstance().getProperties();
    List<String> listOfStrings = new ArrayList<String>();
    int[] ArrayTest1 = {242, 194, 241, 92, 53, 27, 74, -75, -357, -125, -29, 141, 269, 121, 50, 26, -29, -118, -254, 89, 246, -20, -119, -322};
    int[] ArrayTest2 = {171, 160, 105, 190, 107, 36, 33, 0, 47, 58, 39, -98, -278, -151, -53, -43, 38, 117, 204, 116, 70, 55, 11, 10, -15, -14, -118, -244, -4, 83, 222, 12, -8, -69, -117, -255};

    int[] ArrayToCompare1 = {242, 194, 241, 92, 53, 27, 74, -75, -357, -125, -29, 141, 269, 121, 50, 26};
    int[] ArrayToCompare2 = {171, 160, 105, 190, 107, 36, 33, 0, 47, 58, 39};

    int[] resultFromFunction = new int[15];

    @Test
    public void testValidDateForValidDate(){
        listOfStrings.add("12-11-2002");
        Assert.assertTrue(ctrl.validDate(listOfStrings,"12-11-2002"));
    }
    @Test
    public void testValidDateForIncorrectFormat(){
        listOfStrings.add("12-11-2002");
        Assert.assertFalse(ctrl.validDate(listOfStrings,"12/11/2002"));
    }
    @Test
    public void testValidDateForDateNotInList(){
        listOfStrings.add("12-11-2002");
        Assert.assertFalse(ctrl.validDate(listOfStrings,"13-11-2002"));
    }

    @Test
    public void testGetAlgorithmFromProps(){
        props.setProperty(Constants.PARAMS_CENTERPERFORMANCE_ALGORITHM, "1");
        Assert.assertEquals(ctrl.getAlgorithmFromProps(),1);
    }

    @Test
    public void testMaxContiguousSumBruteForce1(){
        props.setProperty(Constants.PARAMS_CENTERPERFORMANCE_ALGORITHM,"0");
        resultFromFunction = ctrl.getMaxContiguousSum(ArrayTest1);
        Assert.assertEquals(resultFromFunction,ArrayToCompare1);
    }
    @Test
    public void testMaxContiguousSumBruteForce2(){
        props.setProperty(Constants.PARAMS_CENTERPERFORMANCE_ALGORITHM,"0");
        resultFromFunction = ctrl.getMaxContiguousSum(ArrayTest2);
        Assert.assertEquals(resultFromFunction,ArrayToCompare2);
    }
    @Test
    public void testMaxContiguousSumBenchmark1(){
        props.setProperty(Constants.PARAMS_CENTERPERFORMANCE_ALGORITHM,"1");
        resultFromFunction = ctrl.getMaxContiguousSum(ArrayTest1);
        Assert.assertEquals(resultFromFunction,ArrayToCompare1);
    }
    @Test
    public void testMaxContiguousSumBenchmark2(){
        props.setProperty(Constants.PARAMS_CENTERPERFORMANCE_ALGORITHM,"1");
        resultFromFunction = ctrl.getMaxContiguousSum(ArrayTest2);
        Assert.assertEquals(resultFromFunction,ArrayToCompare2);
    }
}
