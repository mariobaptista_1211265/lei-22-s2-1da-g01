package app.ui.gui;

import app.controller.App;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.util.Optional;

public class MainGUI extends Application {

    private Stage mainStage;

    @Override
    public void start(Stage primaryStage) throws IOException {

        App.getInstance();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/gui/Main.fxml"));

        Parent root = loader.load();

        Scene scene = new Scene(root);

        Image icon = new Image(getClass().getResourceAsStream("/icons/rede.png"));

        this.mainStage = primaryStage;

        Platform.setImplicitExit(true);

        primaryStage.setOnCloseRequest(confirmCloseEventHandler);

        primaryStage.getIcons().add(icon);
        primaryStage.setTitle("Vaccination Management System");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    private EventHandler<WindowEvent> confirmCloseEventHandler = event -> {
        Alert closeConfirmation = new Alert(
                Alert.AlertType.CONFIRMATION,
                "Are you sure you want to exit?"
        );
        Button exitButton = (Button) closeConfirmation.getDialogPane().lookupButton(
                ButtonType.OK
        );
        exitButton.setText("Exit");
        closeConfirmation.setHeaderText("Confirm Exit");
        closeConfirmation.initModality(Modality.APPLICATION_MODAL);
        closeConfirmation.initOwner(mainStage);

        Optional<ButtonType> closeResponse = closeConfirmation.showAndWait();
        if (!ButtonType.OK.equals(closeResponse.get())) {
            event.consume();
        }
    };

    @Override
    public void stop() throws Exception
    {
        super.stop();

        Platform.exit();
        System.exit(0);
    }

    public static void main(String[] args) {
        launch();
    }

}