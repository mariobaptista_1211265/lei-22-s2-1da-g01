package app.ui.gui.controller;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.WaitingRoom;
import app.ui.console.stores.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import pt.isep.lei.esoft.auth.UserSession;

import java.io.IOException;

public class coordinatorcontroller {

    @FXML
    private Label lblMain;

    @FXML
    private AnchorPane anchorMain;

    private Company company;
    private CenterStore centerStore;

    private WaitingRoom waitingRoom;

    private NHSUserStore nhsUsers;

    private VaccineStore vaccines;

    private VaccineScheduleStore vaccineScheduleStore;

    private VaccinationLogStore logStore;

    public coordinatorcontroller() {
        company = App.getInstance().getCompany();
        centerStore = company.getCenterStore();
        waitingRoom = company.getWaitingRoom();
        nhsUsers = company.getNHSUserStore();
        vaccines = company.getVaccineStore();
        vaccineScheduleStore = company.getScheduleStore();
        logStore = company.getLogStore();
        UserSession us = company.getAuthFacade().getCurrentUserSession();
        String Nome = us.getUserName();
        System.out.println(Nome);
    }



    public void Reset_Click(ActionEvent actionEvent) {
    }

    public void ExportVaccinationStatistics(ActionEvent event) {
    }

    public void ChangeToSortSettings(ActionEvent event) throws IOException {
        Parent blah = FXMLLoader.load(getClass().getResource("/gui/Sort.fxml"));
        Scene scene = new Scene(blah);
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(scene);
        appStage.show();
        //AnchorPane pane = FXMLLoader.load(getClass().getResource("/gui/Sort.fxml"));
        //anchorMain.getChildren().setAll(pane);
    }

    public void ChangeToCenterPerformance(ActionEvent actionEvent) throws IOException {
        Parent blah = FXMLLoader.load(getClass().getResource("/gui/CenterPerformance.fxml"));
        Scene scene = new Scene(blah);
        Stage appStage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        appStage.setScene(scene);
        appStage.show();
    }
}
