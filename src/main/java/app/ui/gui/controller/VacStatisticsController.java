package app.ui.gui.controller;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.US15Obj;
import app.domain.model.VacAdministration;
import app.domain.model.WaitingRoom;
import app.ui.console.stores.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ResourceBundle;

public class VacStatisticsController implements Initializable {

    @FXML
    private AnchorPane anchorMain;

    public Button btnSortSettings2;
    @FXML
    private DatePicker DayI;

    @FXML
    private DatePicker DayF;

    @FXML
    private TextField FileName;

    @FXML
    private TableColumn Date;
    @FXML
    private TableColumn Users;
    @FXML
    private TableView StatsTable;

    private Company company;
    private VaccineAdmStore admStore;

    private VaccinationLogStore logStore;

    private LocalDate dateI;

    private LocalDate dateF;

    private String fileName;

    private int centerId;

    public VacStatisticsController() {
        company = App.getInstance().getCompany();
        admStore = company.getVaccineAdmStore();
        logStore = company.getLogStore();

        String email = company.getAuthFacade().getCurrentUserSession().getUserId().getEmail();
        centerId = company.getCoordinatorStore().getCenterIDByEmail(email);
    }

    public void ExportVaccinationStatistics(ActionEvent event) throws IOException {
        Writer writer = null;
        try {
            String filePath = FileName.getText();
            if (filePath.isEmpty()) {
                filePath = "VaccinationStatistics.csv";
            } else if (!filePath.endsWith(".csv")) {
                filePath += ".csv";
            }
            File file = new File(filePath);
            writer = new BufferedWriter(new FileWriter(file));
            writer.write("Date,NumberOfUsersFullyVaccinated\n");
            for (US15Obj person : (ObservableList<US15Obj>) StatsTable.getItems()) {

                String text = person.getDate() + "," + person.getUser() + "\n";

                writer.write(text);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {

            assert writer != null;
            writer.flush();
            writer.close();
        }


    }

    public void ChangeToSortSettings(ActionEvent event) throws IOException {
        Parent loader = FXMLLoader.load(getClass().getResource("/gui/Sort.fxml"));
        Scene scene = new Scene(loader);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    public void getDateI(ActionEvent actionEvent) {
        UpdateTable();
    }

    public void getDateF(ActionEvent actionEvent) {
        UpdateTable();
    }

    public void getFileName(ActionEvent actionEvent) {
        fileName = FileName.getText();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        DayI.setValue(LocalDate.now().minusDays(7));
        DayF.setValue(LocalDate.now());

        UpdateTable();
    }

    private void UpdateTable(){
        StatsTable.getItems().clear();
        Date.setCellValueFactory(new PropertyValueFactory<US15Obj, String>("Date"));
        Users.setCellValueFactory(new PropertyValueFactory<US15Obj, String>("User"));

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        List<VacAdministration> vacAdminis = logStore.getAllVaccinationlogs();

        for (LocalDate date = DayI.getValue(); date.isBefore(DayF.getValue().plusDays(1)); date = date.plusDays(1)) {
            int count = 0;
            for (VacAdministration vacAdmin : vacAdminis) {
                if (vacAdmin.getCenterID() == centerId) {
                    if (date.equals(vacAdmin.getAdministrationDateTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate())) {
                        String brand = vacAdmin.getVaccineName();
                        try {
                            if (vacAdmin.getVaccineDose() == Double.parseDouble(admStore.getdetails(brand)[5])) {
                                count++;
                            }
                        } catch (Exception ignore) {
                        }
                    }
                }
            }
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            ObservableList<US15Obj> list = FXCollections.observableArrayList(
                    new US15Obj(date.format(formatter), String.valueOf(count))
            );
            StatsTable.getItems().addAll(list);

        }
    }

    @FXML
    void Reset_Click(ActionEvent event) throws Exception{
        company.getAuthFacade().doLogout();
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/gui/Main.fxml"));
        anchorMain.getChildren().setAll(pane);

    }

    @FXML
    public void ChangeToCenterPerformance(ActionEvent actionEvent) throws IOException {
        Parent loader = FXMLLoader.load(getClass().getResource("/gui/CenterPerformance.fxml"));
        Scene scene = new Scene(loader);
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }
}
