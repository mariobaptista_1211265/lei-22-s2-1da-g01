package app.ui.gui.controller;

import app.controller.App;
import app.controller.AuthController;
import app.domain.dto.VaccinationDTO;
import app.domain.model.Company;
import app.domain.model.US17Obj;
import app.domain.model.WaitingRoom;
import app.ui.console.stores.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import pt.isep.lei.esoft.auth.AuthFacade;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import java.util.Scanner;

public class sortcontroller implements Initializable{

    public Button btnSortSettings2;
    private AuthController ctrl;

    private Company company;

    private AuthFacade authFacade;

    private CenterStore centerStore;

    private WaitingRoom waitingRoom;

    private VaccineStore vaccines;

    private VaccineScheduleStore vaccineScheduleStore;

    private VaccinationLogStore logStore;

    @FXML
    private AnchorPane anchorMain;

    @FXML
    private ChoiceBox<String> cboxSort;

    @FXML
    private Button btnOrder;

    @FXML
    private TableView<US17Obj> table;

    @FXML
    private Button btnSort;

    @FXML
    private Button btnSortSettings;

    @FXML
    private Button btnLogOut;

    @FXML
    private Button btnStats;

    @FXML
    private DatePicker DayChose;

    @FXML
    private CheckBox QuickSort;

    @FXML
    private CheckBox BubbleSort;

    String MatrizSort[][]=new String[2][2];
    int posy=0;

    public sortcontroller(){
        ctrl = new AuthController();
        company = App.getInstance().getCompany();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        cboxSort.getItems().addAll("Arrival Date","Leave Date");
        cboxSort.setValue("Arrival Date");
        boolean exist=false;
        //criar colunas
        TableColumn SNS = new TableColumn("SNS");
        TableColumn Name = new TableColumn("Name");
        TableColumn Vaccine = new TableColumn("Vaccine");
        TableColumn Dose = new TableColumn("Dose");
        TableColumn Lot = new TableColumn("Lot");
        TableColumn Schedule = new TableColumn("Scheduled");
        TableColumn Arrival = new TableColumn("Arrival");
        TableColumn Administration = new TableColumn("Administrated");
        TableColumn Leaving = new TableColumn("Left");
        table.getColumns().addAll(SNS, Name, Vaccine, Dose, Lot, Schedule, Arrival, Administration, Leaving);
        //Comunica com o ficheiro settings
        String path="performanceDataFromGaoFuNationalCenterDoPortoVaccinationCenter.csv";
        Scanner sc=new Scanner(System.in);
        Scanner sc2=new Scanner(System.in);
        int y=0;
        try {
            sc=new Scanner(new File(path));
            sc2=new Scanner(new File(path));
            exist=true;
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }
        if (exist) {
            sc.useDelimiter(";");
            sc2.useDelimiter(";");
            while (sc.hasNextLine()) {
                y++;
                sc.nextLine();
            }
            String[][] MatrizFicheiro = new String[10][y - 1];
            int poxY = 0, snsnum = 0;
            String[] dados = new String[8];
            String username;
            String[] datascorrect=new String[9];
            String[] horacorrect=new String[4];
            String[] detalhes = new String[7];
            sc2.nextLine();
            SNS.setCellValueFactory(new PropertyValueFactory<US17Obj, String>("SNS"));
            Name.setCellValueFactory(new PropertyValueFactory<US17Obj, String>("Name"));
            Vaccine.setCellValueFactory(new PropertyValueFactory<US17Obj, String>("Vaccine"));
            Dose.setCellValueFactory(new PropertyValueFactory<US17Obj, String>("Dose"));
            Lot.setCellValueFactory(new PropertyValueFactory<US17Obj, String>("Lot"));
            Schedule.setCellValueFactory(new PropertyValueFactory<US17Obj, String>("Schedule"));
            Arrival.setCellValueFactory(new PropertyValueFactory<US17Obj, String>("Arrival"));
            Administration.setCellValueFactory(new PropertyValueFactory<US17Obj, String>("Administration"));
            Leaving.setCellValueFactory(new PropertyValueFactory<US17Obj, String>("Leave"));
            while (sc2.hasNextLine()) {
                String linha = sc2.nextLine();
                dados = linha.split(";");
                snsnum = Integer.parseInt(dados[0]);
                if (!NHSUserStore.checkUniqueSNSNumber(snsnum)) {
                    username = NHSUserStore.getNameBySns(snsnum);
                    if (!VaccineAdmStore.CheckBrand(dados[1])) {
                        MatrizFicheiro[0][poxY] = dados[0];
                        MatrizFicheiro[1][poxY] = username;
                        for (int i = 2, j = 1; i < 9; i++, j++) {
                            if (i>=5)
                            {
                                datascorrect=dados[j].split("/");
                                horacorrect=datascorrect[2].split(" ");
                                if (datascorrect[0].length()==1) {
                                    datascorrect[0] = "0" + datascorrect[0];
                                }
                                if (datascorrect[1].length()==1) {
                                    datascorrect[1] = "0" + datascorrect[1];
                                }
                                if (horacorrect[1].length()==4){
                                    horacorrect[1]="0"+horacorrect[1];
                                }
                                dados[j]=datascorrect[0]+"/"+datascorrect[1]+"/"+horacorrect[0]+" "+horacorrect[1];
                            }
                            MatrizFicheiro[i][poxY] = dados[j];
                        }
                        detalhes = VaccineAdmStore.getdetails(MatrizFicheiro[2][poxY]);
                        MatrizFicheiro[9][poxY] = "vaccine " + detalhes[0] + " for " + detalhes[1] + ". Ages of: " + detalhes[2] + " - " + detalhes[3] + ". Dosage of " + detalhes[4] + "ml" + " (" + detalhes[5] + " doses with an interval of " + detalhes[6] + " days)";
                        ObservableList<US17Obj> list = FXCollections.observableArrayList(
                                new US17Obj(MatrizFicheiro[0][poxY], MatrizFicheiro[1][poxY], MatrizFicheiro[2][poxY], MatrizFicheiro[3][poxY], MatrizFicheiro[4][poxY], MatrizFicheiro[5][poxY], MatrizFicheiro[6][poxY], MatrizFicheiro[7][poxY], MatrizFicheiro[8][poxY])
                        );
                        table.getItems().addAll(list);
                        poxY++;
                    }
                }
            }
            posy=poxY;
            MatrizSort=MatrizFicheiro;
            System.out.println(MatrizSort[0][0]);
            try {
                sort();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    @FXML
    void changeorder_Click(){
        if (btnOrder.getText()=="↑") {
            btnOrder.setText("↓");
            order();
        }
        else {
            btnOrder.setText("↑");
            orderInv();
        }
    }

    @FXML
    void Stats_Click(ActionEvent event) throws Exception{
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/gui/VacStatistics.fxml"));
        anchorMain.getChildren().setAll(pane);
    }

    @FXML
    void Reset_Click(ActionEvent event) throws Exception{
        ctrl.doLogout();
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/gui/Main.fxml"));
        anchorMain.getChildren().setAll(pane);

    }
    @FXML
    void sort() throws Exception {
        int posx=0;
        String Arr ="Arrival Date";
        String sorttype="QuickSort";
        if(cboxSort.getValue().equals(Arr))
        {
            posx=6;
            if (sorttype.equals("QuickSort"))
                quickSort(MatrizSort, 0,posy-1, posx);
            else
                bubbleSort(MatrizSort,posy,posx);
        }
        else {
            posx=8;
            if (sorttype.equals("QuickSort"))
                quickSort(MatrizSort, 0,posy-1, posx);
            else
                bubbleSort(MatrizSort,posy,posx);
        }
    }
    //ordenar
    void order(){
        String[] Temp=new String[10];
        table.getItems().clear();
        for (int j=0; j<posy;j++)
        {
            for (int i=0; i<10; i++)
            {
                if (MatrizSort[i][j]!=null)
                    Temp[i]=MatrizSort[i][j];
            }
            if (MatrizSort[0][j]!=null) {
                ObservableList<US17Obj> list = FXCollections.observableArrayList(
                        new US17Obj(Temp[0], Temp[1], Temp[2], Temp[3], Temp[4], Temp[5], Temp[6], Temp[7], Temp[8])
                );
                table.getItems().addAll(list);
            }
        }
    }
    //ordenar Inverso
    void orderInv(){
        String[] Temp=new String[10];
        table.getItems().clear();
        for (int j=posy-1; j>=0;j--)
        {
            for (int i=0; i<10; i++)
            {
                if (MatrizSort[i][j]!=null)
                    Temp[i]=MatrizSort[i][j];
            }
            if (MatrizSort[0][j]!=null) {
                ObservableList<US17Obj> list = FXCollections.observableArrayList(
                        new US17Obj(Temp[0], Temp[1], Temp[2], Temp[3], Temp[4], Temp[5], Temp[6], Temp[7], Temp[8])
                );
                table.getItems().addAll(list);
            }
        }
    }
    //importar data do ficheiro
    public void importdata(ActionEvent actionEvent) throws Exception {
        authFacade= company.getAuthFacade();
        int id = company.getCoordinatorStore().getCenterIDByEmail(authFacade.getCurrentUserSession().getUserId().getEmail());
        int dose;
        for (int j=0;j<posy;j++) {
            if (MatrizSort[3][j].equals("Primeira"))
            {
                dose=1;
            }else {
                if (MatrizSort[3][j].equals("Segunda")) {
                    dose = 2;
                }
                else {
                    if (MatrizSort[3][j].equals("Terceira")) {
                        dose = 3;
                    } else {
                        dose = 0;
                    }
                }
            }
            VaccinationLogStore.saveRecord(VaccinationLogStore.createRecord(new VaccinationDTO(Integer.parseInt(MatrizSort[0][j]),MatrizSort[2][j],dose,MatrizSort[4][j],(new SimpleDateFormat("dd/MM/yyyy hh:mm").parse(MatrizSort[5][j])),(new SimpleDateFormat("dd/MM/yyyy hh:mm").parse(MatrizSort[6][j])),(new SimpleDateFormat("dd/MM/yyyy hh:mm").parse(MatrizSort[7][j])),(new SimpleDateFormat("dd/MM/yyyy hh:mm").parse(MatrizSort[8][j])),id)));
        }
    }
    //Bubble Sort
    private void bubbleSort(String[][] Matrizsort, int n, int pos) throws Exception
    {
        int i, j;
        String[] temp=new String[10];
        boolean swapped;
        for (i = 0; i < n - 1; i++)
        {
            swapped = false;
            for (j = 0; j < n - i - 1; j++)
            {
                if ((new SimpleDateFormat("dd/MM/yyyy hh:mm").parse(Matrizsort[pos][j])).compareTo(new SimpleDateFormat("dd/MM/yyyy hh:mm").parse(Matrizsort[pos][j + 1]))>0)
                {
                    for (int k=0;k<10;k++) {
                        temp[k] = Matrizsort[k][j];
                        Matrizsort[k][j] = Matrizsort[k][j + 1];
                        Matrizsort[k][j + 1] = temp[k];
                    }
                    swapped = true;
                }
            }
            if (swapped == false) {
                break;
            }
        }
        btnOrder.setText("↓");
        order();
    }
    //Quicksort
    static void swap(String[][] Matrizsort, int i, int j, int pos)
    {
        String[] temp=new String[10];
        for (int k=0;k<10;k++) {
            temp[k] = Matrizsort[k][i];
            Matrizsort[k][i] = Matrizsort[k][j];
            Matrizsort[k][j] = temp[k];
        }
    }
    /* This function takes last element as pivot, places
       the pivot element at its correct position in sorted
       array, and places all smaller (smaller than pivot)
       to left of pivot and all greater elements to right
       of pivot */
    static int partition(String[][] Matrizsort, int low, int high, int pos) throws Exception
    {
        // pivot
        System.out.println(pos);
        System.out.println(high);
        String pivot = Matrizsort[pos][high];
        int i = (low-1);
        for(int j = low; j <= high - 1; j++)
        {
            System.out.println(Matrizsort[pos][j]);
            System.out.println(pivot);
            if ((new SimpleDateFormat("dd/MM/yyyy hh:mm").parse(Matrizsort[pos][j])).compareTo(new SimpleDateFormat("dd/MM/yyyy hh:mm").parse(pivot))<0)
            {
                i++;
                swap(Matrizsort, i, j, pos);
            }
        }
        swap(Matrizsort, i + 1, high, pos);
        return (i + 1);
    }
    void quickSort(String[][] Matrizsort, int low, int high, int pos) throws Exception
    {
        if (low < high)
        {
            // pi is partitioning index
            int pi = partition(Matrizsort, low, high, pos);
            quickSort(Matrizsort, low, pi - 1,pos);
            quickSort(Matrizsort, pi + 1, high,pos);
        }
        btnOrder.setText("↓");
        order();
    }

    public void pressM(MouseEvent mouseEvent) {
        Alert information = new Alert(AlertType.INFORMATION);
        int i;
        information.setTitle("Vaccine Details!");
        if (btnOrder.getText().equals("↓"))
             i = table.getSelectionModel().getSelectedIndex();
        else
            i = posy-1-table.getSelectionModel().getSelectedIndex();
        String[] data = MatrizSort[9][i].split("\\. ");
        information.setContentText(data[0]+".\n"+data[1]+".\n"+data[2]);
        information.initOwner(anchorMain.getScene().getWindow());
        information.showAndWait();
    }

    @FXML
    public void ChangeToCenterPerformance(ActionEvent actionEvent) throws IOException {
        Parent loader = FXMLLoader.load(getClass().getResource("/gui/CenterPerformance.fxml"));
        Scene scene = new Scene(loader);
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }
}
