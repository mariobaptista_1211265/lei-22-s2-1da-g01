package app.ui.gui.controller;

import app.controller.App;
import app.controller.AuthController;
import app.domain.dto.VaccinationDTO;
import app.domain.model.Company;
import app.domain.model.VacAdministration;
import app.domain.model.WaitingRoom;
import app.domain.shared.Constants;
import app.ui.console.stores.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Timer;

public class nursecontroller implements Initializable {

    private AuthController ctrl;

    private int centerID = 0;
    private Company company;
    private CenterStore centerStore;

    private WaitingRoom waitingRoom;

    private NHSUserStore nhsUsers;

    private VaccineStore vaccines;

    private VaccineScheduleStore vaccineScheduleStore;

    private VaccinationLogStore logStore;

    private VaccineAdmStore vaccineAdmStore;

    @FXML
    private AnchorPane anchorMain;

    @FXML
    private Button btnChooseCenter;

    @FXML
    private Button btnGenerateReport;

    @FXML
    private Button btnReset;

    @FXML
    private Button btnLogOut;

    @FXML
    private Button btnSelectCenter;

    @FXML
    private ChoiceBox<String> cboxCenterType;

    @FXML
    private Label lblMain;

    @FXML
    private ChoiceBox<String> cboxDose;

    @FXML
    private ChoiceBox<String> cboxVaccine;

    @FXML
    private Label lblUserName;

    @FXML
    private ListView<String> listCenters;

    @FXML
    private Pane paneChooseCenter;

    @FXML
    private Pane paneReport;

    @FXML
    private TextField txtLotNumber;

    @FXML
    private TextField txtSnsNumber;

    @FXML
    private Button btnGenerate;

    @FXML
    private ImageView imageVac;

    private String[] centerTypes = {"Health Care Center", "Mass Vaccination Center"};

    private String[] vaccinesString;

    private String currentCenterName;





    public nursecontroller() {
        ctrl = new AuthController();
        company = App.getInstance().getCompany();
        centerStore = company.getCenterStore();
        waitingRoom = company.getWaitingRoom();
        nhsUsers = company.getNHSUserStore();
        vaccines = company.getVaccineStore();
        vaccineScheduleStore = company.getScheduleStore();
        logStore = company.getLogStore();
        vaccineAdmStore = company.getVaccineAdmStore();
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        cboxCenterType.getItems().addAll(centerTypes);
        cboxCenterType.setOnAction(this::centerTypeChange);
        vaccinesString = vaccines.getVaccines();
        cboxVaccine.getItems().addAll(vaccinesString);
        cboxVaccine.setOnAction(this::vaccineNameChange);
        imageVac.setImage(new Image("/images/vaccine.png"));

    }


    @FXML
    void centerTypeChange(ActionEvent event) {
        if (cboxCenterType.getValue().equals("Health Care Center")) {
            String[] health = centerStore.getHealthCenterNames();
            listCenters.getItems().clear();
            listCenters.getItems().addAll(health);


            listCenters.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {

                    currentCenterName = listCenters.getSelectionModel().getSelectedItem();
                    btnSelectCenter.setDisable(false);


                }
            });


        } else if (cboxCenterType.getValue().equals("Mass Vaccination Center")) {
            String[] vac = centerStore.getVacCenterNames();
            listCenters.getItems().clear();
            listCenters.getItems().addAll(vac);

            listCenters.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {

                    currentCenterName = listCenters.getSelectionModel().getSelectedItem();
                    btnSelectCenter.setDisable(false);


                }
            });

        }

    }

    @FXML
    void Select_Click(ActionEvent event) {
        if (cboxCenterType.getValue().equals("Health Care Center")) {
            centerID = centerStore.getHealthCenterIDbyName(currentCenterName);
        } else if (cboxCenterType.getValue().equals("Mass Vaccination Center")) {
            centerID = centerStore.getVacCenterIDbyName(currentCenterName);
        }

        btnChooseCenter.setVisible(false);
        btnReset.setVisible(true);
        btnGenerateReport.setStyle("-fx-background-color: #8A2BE2");
        btnGenerateReport.setVisible(true);
        lblMain.setText("Generate Report");
        paneChooseCenter.setVisible(false);
        paneReport.setVisible(true);

    }

    @FXML
    void LogOut_Click(ActionEvent event) throws Exception {
        ctrl.doLogout();
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/gui/Main.fxml"));
        anchorMain.getChildren().setAll(pane);
    }


    @FXML
    void sns_Verify(KeyEvent event) {

        Alert error = new Alert(Alert.AlertType.ERROR);

        if (txtSnsNumber.getText().length() == Constants.NUMBER_OF_DIGITS_NHS_NUMBER) {

            if (waitingRoom.checkIfUserExistsInWaitingRoom(centerID, Integer.parseInt(txtSnsNumber.getText()))) {
                txtSnsNumber.setDisable(true);
                lblUserName.setText(nhsUsers.getNameBySns(Integer.parseInt(txtSnsNumber.getText())));
                cboxVaccine.setDisable(false);
            }
            else{
                error.setTitle("Oops...Something went wrong!");
                error.setContentText("SNS user is not on the center!");
                error.showAndWait();
            }

        }


    }



    @FXML
    void vaccineNameChange(ActionEvent event) {
        cboxVaccine.setDisable(true);
        String[] doses = getAdministrationDoses(cboxVaccine.getValue());
        cboxDose.getItems().addAll(doses);
        cboxDose.setOnAction(this::doseNumberChange);
        cboxDose.setDisable(false);
    }


    @FXML
    void doseNumberChange(ActionEvent event) {
        cboxDose.setDisable(true);
        txtLotNumber.setDisable(false);
        btnGenerate.setDisable(false);
    }

    @FXML
    void Generate_Click(ActionEvent event) {

        int snsNumber = Integer.parseInt(txtSnsNumber.getText());
        String vaccineName = cboxVaccine.getValue();
        int doseNumber = Integer.parseInt(cboxDose.getValue());
        String lotNumber = txtLotNumber.getText();
        Date scheduledDate = vaccineScheduleStore.getScheduledDateTime(snsNumber);
        Date arrivalDateTime = waitingRoom.getDateOfArrival(centerID, snsNumber);
        Date administrationDateTime = new Date();

        // SimpleDateFormat minutes = new SimpleDateFormat("mm");

       // int minutesToAdd = Integer.parseInt(minutes.format(administrationDateTime)) + (Constants.DEFAULT_RECOVERY_PERIOD_IN_MILLISECONDS / 60000 + 2);

        Date leavingDateTime = getLeavingDateTime(administrationDateTime);

        VaccinationDTO vaccinationDTO = new VaccinationDTO(snsNumber,vaccineName, doseNumber, lotNumber, scheduledDate, arrivalDateTime, administrationDateTime, leavingDateTime, centerID);

        VacAdministration vacAdministration = logStore.createRecord(vaccinationDTO);

        logStore.saveRecord(vacAdministration);



        sendLeaveMessage(snsNumber);


        txtSnsNumber.clear();
        txtLotNumber.clear();
        txtSnsNumber.setDisable(false);
        cboxVaccine.setDisable(true);
        cboxVaccine.setValue(null);
        cboxDose.setDisable(true);
        cboxDose.setValue(null);
        txtLotNumber.setDisable(true);
        btnSelectCenter.setDisable(true);
        btnGenerate.setDisable(true);
        lblUserName.setText("");




        Alert information = new Alert(Alert.AlertType.CONFIRMATION);

        information.setTitle("Record created!");
        information.setContentText("Record created successfully, and " + nhsUsers.getNameBySns(snsNumber) + " will receive a message when the recovery period is finished.");
        information.initOwner(anchorMain.getScene().getWindow());
        information.showAndWait();


    }

    public void sendLeaveMessage(int snsNumber){

        Timer t = new java.util.Timer();

        t.schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {


                        File smsFile = new File("sms.txt");

                        String smsString = "Your recovery period has ended. You can now leave the Center. Thank You!";

                        try {
                            if (smsFile.createNewFile()) {
                                int a = 0;
                            } else {
                                int a = 1;
                            }
                            FileWriter myWriter = new FileWriter("sms.txt", true);
                            myWriter.write("SMS message to: " + nhsUsers.getPhoneNumberBySNS(snsNumber) + "\n");
                            myWriter.write(smsString);
                            myWriter.write("\n--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
                            myWriter.close();

                            System.out.println("\nSuccessfully sent the message to " + nhsUsers.getPhoneNumberBySNS(snsNumber) + ".");
                        } catch (IOException e) {
                            System.out.println("An error occurred.");
                            e.printStackTrace();
                        }

                        t.cancel();
                    }
                },
                Constants.RECOVERY_PERIOD_FOR_MESSAGE_TEST
        );
    }


    public Date getLeavingDateTime(Date administrationDateTime){
        Date leavingDateTime = new Date(administrationDateTime.getTime() + Constants.DEFAULT_RECOVERY_PERIOD_IN_MILISECONDS);
        return leavingDateTime;
    }

    public String[] getAdministrationDoses(String vaccinename){
        int number = (int)vaccineAdmStore.getAdministrationDoses(vaccinename);

        String[] doses = new String[number];

        for(int i = 0; i < number; i++){
            String dose = Integer.toString(i+1);
            doses[i] = dose;
        }


        return doses;
    }


    @FXML
    void Reset_Click(ActionEvent event) {

        txtSnsNumber.clear();
        txtLotNumber.clear();
        txtSnsNumber.setDisable(false);
        cboxVaccine.setDisable(true);
        cboxVaccine.setValue(null);
        cboxDose.setDisable(true);
        cboxDose.setValue(null);
        txtLotNumber.setDisable(true);
        btnSelectCenter.setDisable(true);
        btnGenerate.setDisable(true);
        lblUserName.setText("");
        cboxDose.getItems().clear();

    }

}
