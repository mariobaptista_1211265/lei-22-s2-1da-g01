package app.ui.gui.controller;

import app.controller.App;
import app.controller.AuthController;
import app.domain.model.Company;
import app.domain.shared.Constants;
import app.ui.console.stores.EmployeeStore;
import app.ui.console.utils.Utils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import pt.isep.lei.esoft.auth.mappers.dto.UserRoleDTO;
import java.net.URL;
import java.util.ResourceBundle;


import java.io.IOException;
import java.util.*;

public class maincontroller implements Initializable {

    private AuthController ctrl;

    private Company company;

    private EmployeeStore employeeStore;


    @FXML
    private AnchorPane anchorMain;
    @FXML
    private Button btnKnowTeam;

    @FXML
    private Button btnLogIn;

    @FXML
    private Button btnLogInMenu;

    @FXML
    private TextField txtEmail;

    @FXML
    private TextField txtPassword;

    @FXML
    private Label lblMain;

    @FXML
    private Pane paneDevTeam;

    @FXML
    private Pane paneLogIn;
    @FXML
    private ImageView imageVac;


    public maincontroller(){
        ctrl = new AuthController();
        company = App.getInstance().getCompany();
        employeeStore = company.getEmployeeStore();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        display_image();
    }

    @FXML
    void Log_In(ActionEvent event) throws IOException {


        boolean success = false;

        Alert error = new Alert(Alert.AlertType.ERROR);



            String id = txtEmail.getText();
            String pwd = txtPassword.getText();

            success = ctrl.doLogin(id, pwd);

            if (!success)
            {
                txtEmail.setText("");
                txtPassword.setText("");
                error.setResizable(false);
                error.setTitle("Oops...Something went wrong!");
                error.setContentText("Invalid Email or Password.");
                error.showAndWait();

            }else{

                List<UserRoleDTO> roles = this.ctrl.getUserRoles();
                if ( (roles == null) || (roles.isEmpty()) )
                {
                    txtEmail.setText("");
                    txtPassword.setText("");
                    error.setResizable(false);
                    error.setTitle("Oops...Something went wrong!");
                    error.setContentText("User does not have any role assigned.");
                    error.showAndWait();
                }
                else
                {
                    UserRoleDTO role = selectsRole(roles);
                    if (!Objects.isNull(role)) {
                        if (role.getId().equals(Constants.ROLE_NURSE)) {

                            AnchorPane pane = FXMLLoader.load(getClass().getResource("/gui/Nurse.fxml"));
                            anchorMain.getChildren().setAll(pane);


                        }else{
                            if (role.getId().equals(Constants.ROLE_COORDINATOR)) {

                                AnchorPane pane = FXMLLoader.load(getClass().getResource("/gui/Sort.fxml"));
                                anchorMain.getChildren().setAll(pane);


                            }else{
                                if (role.getId().equals(Constants.ROLE_COORDINATOR)) {

                                    AnchorPane pane = FXMLLoader.load(getClass().getResource("/gui/VacStatistics.fxml"));
                                    anchorMain.getChildren().setAll(pane);
                                }else{
                                    txtEmail.setText("");
                                    txtPassword.setText("");
                                    error.setResizable(false);
                                    error.setTitle("Oops...Something went wrong!");
                                    error.setContentText("No UI for this Role. Please contact any of our Developers.");
                                    error.initOwner(anchorMain.getScene().getWindow());
                                    error.showAndWait();
                                }
                            }
                        }


                    }
                    else
                    {
                        txtEmail.setText("");
                        txtPassword.setText("");
                        error.setResizable(false);
                        error.setTitle("Oops...Something went wrong!");
                        error.setContentText("User did not select a role.");
                        error.showAndWait();
                    }
                }


            }


    }

    public void display_image(){
        imageVac.setImage(new Image("/images/health.png"));
    }
    private UserRoleDTO selectsRole(List<UserRoleDTO> roles)
    {
        if (roles.size() == 1)
            return roles.get(0);
        else
            return (UserRoleDTO) Utils.showAndSelectOne(roles, "Select the role you want to adopt in this session:");
    }

    public void handleClicks(ActionEvent actionEvent) {

        if(actionEvent.getSource() == btnLogInMenu){

            btnKnowTeam.setStyle("-fx-background-color:  #9932CC;");
            btnLogInMenu.setStyle("-fx-background-color:  #8A2BE2;");
            paneDevTeam.setVisible(false);
            paneLogIn.setVisible(true);
            lblMain.setText("WELCOME TO OUR APPLICATION");

        }
        else{
            if(actionEvent.getSource() == btnKnowTeam){

                btnLogInMenu.setStyle("-fx-background-color: #9932CC;");
                btnKnowTeam.setStyle("-fx-background-color:  #8A2BE2;");
                paneLogIn.setVisible(false);
                paneDevTeam.setVisible(true);
                lblMain.setText("OUR DEVELOPMENT TEAM");

            }
        }
    }
}
