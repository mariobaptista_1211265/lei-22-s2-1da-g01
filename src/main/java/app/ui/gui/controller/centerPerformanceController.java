package app.ui.gui.controller;

import app.controller.App;
import app.controller.AuthController;
import app.controller.US16Controller;
import app.domain.model.US16ListObject;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;

public class centerPerformanceController implements Initializable{

    public DatePicker inpDay;
    public TextField inpTimeInterval;
    public TextField outSumOfContiguous;
    public TableView tblOriginalInfo;
    public TableView tblContiguousInfo;
    private AuthController authCtrl;
    private US16Controller ctrl;
    private List<String> datesWithRecords;
    private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");


    @FXML
    private AnchorPane anchorMain;

    //@FXML
    //private TextField inpTimeInterval;


    public centerPerformanceController(){
        ctrl = new US16Controller(App.getInstance());
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        datesWithRecords = ctrl.getDatesWithInfo();
    }

    @FXML
    void Stats_Click(ActionEvent event) throws Exception{
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/gui/VacStatistics.fxml"));
        anchorMain.getChildren().setAll(pane);
    }

    @FXML
    void Reset_Click(ActionEvent event) throws Exception{
        ctrl.doLogoutGUI();
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/gui/Main.fxml"));
        anchorMain.getChildren().setAll(pane);

    }

    public void ChangeToSortSettings(ActionEvent actionEvent) throws IOException {
        Parent blah = FXMLLoader.load(getClass().getResource("/gui/Sort.fxml"));
        Scene scene = new Scene(blah);
        Stage appStage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        appStage.setScene(scene);
        appStage.show();
    }

    public void updateInfo(ActionEvent actionEvent) throws ParseException {

        String inpDate = "";
        try {
            inpDate = inpDay.getValue().format(dateFormatter);
        } catch (Exception e){
            // alert tempo nao no formato correto
            return;
        }
        double timeInterval;
        try {
            timeInterval = Double.parseDouble(inpTimeInterval.getText());
        } catch (Exception e) {
            // alert tempo assim nao muito valido
            return;
        }

        if (720 % timeInterval != 0) {
            // alert timeInterval nao valido
            return;
        }

        if (!validDate(datesWithRecords, inpDate)) {
            // alert data nao tem registos
            return;
        }

        int[] differences = ctrl.getListOfDifferences(timeInterval, new SimpleDateFormat("dd-MM-yyyy").parse(inpDate));
        System.out.println(Arrays.toString(differences));
        int[] greatestContiguousSum = ctrl.getMaxContiguousSum(differences);
        int sum = 0;
        for (int addToSum : greatestContiguousSum){
            sum += addToSum;
        }

        int startOfContiguous = -1;
        if (sum != 0) {
            startOfContiguous = ctrl.getStartOfSublist(greatestContiguousSum, differences);
        }

        Calendar calToOutput = Calendar.getInstance();
        calToOutput.set(Calendar.HOUR_OF_DAY, 8);
        calToOutput.set(Calendar.MINUTE, 0);
        calToOutput.set(Calendar.SECOND, 0);

        double calculateSeconds = timeInterval%1;
        int minutesToAdd = (int) (timeInterval-calculateSeconds);
        calculateSeconds *= 60;
        int secondsToAdd = (int) calculateSeconds;

        TableColumn dateStart = new TableColumn("Time start");
        TableColumn dateEnd = new TableColumn("Time end");
        TableColumn difference = new TableColumn("Difference");

        TableColumn dateStart2 = new TableColumn("Time start");
        TableColumn dateEnd2 = new TableColumn("Time end");
        TableColumn difference2 = new TableColumn("Difference");

        tblOriginalInfo.getColumns().addAll(dateStart,dateEnd,difference);
        tblContiguousInfo.getColumns().addAll(dateStart2,dateEnd2,difference2);

        dateStart.setCellValueFactory(new PropertyValueFactory<US16ListObject, String>("TimeStart"));
        dateEnd.setCellValueFactory(new PropertyValueFactory<US16ListObject, String>("TimeEnd"));
        difference.setCellValueFactory(new PropertyValueFactory<US16ListObject, String>("Difference"));
        dateStart2.setCellValueFactory(new PropertyValueFactory<US16ListObject, String>("TimeStart"));
        dateEnd2.setCellValueFactory(new PropertyValueFactory<US16ListObject, String>("TimeEnd"));
        difference2.setCellValueFactory(new PropertyValueFactory<US16ListObject, String>("Difference"));

        String timeStart = "";
        String timeEnd = "";
        for (int positionOnList = 0; positionOnList < differences.length; positionOnList ++){

            timeStart = calToOutput.get(Calendar.HOUR_OF_DAY) + ":" + calToOutput.get(Calendar.MINUTE) + ":" + calToOutput.get(Calendar.SECOND);
            calToOutput.add(Calendar.MINUTE, minutesToAdd);
            calToOutput.add(Calendar.SECOND, secondsToAdd);
            timeEnd = calToOutput.get(Calendar.HOUR_OF_DAY) + ":" + calToOutput.get(Calendar.MINUTE) + ":" + calToOutput.get(Calendar.SECOND);

            tblOriginalInfo.getItems().add(new US16ListObject(timeStart,timeEnd,String.valueOf(differences[positionOnList])));
            if (positionOnList >= startOfContiguous && positionOnList < startOfContiguous + greatestContiguousSum.length){
                tblContiguousInfo.getItems().add(new US16ListObject(timeStart,timeEnd,String.valueOf(differences[positionOnList])));
            }
        }


        outSumOfContiguous.setText(String.valueOf(sum));

    }

    private boolean validDate(List<String> possibleDays, String inpDate){
        try{
            new SimpleDateFormat("dd-MM-yyyy").parse(inpDate);
        } catch (Exception e){
            System.out.println("Input date not in correct form. Please write again");
            return false;
        }
        if (!possibleDays.contains(inpDate)){
            System.out.println("This date doesn't contain records. Please select another one");
            return false;
        }
        return true;

    }
}
