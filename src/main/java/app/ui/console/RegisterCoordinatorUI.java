package app.ui.console;

import app.controller.App;
import app.controller.RegisterCoordinatorController;
import app.ui.console.utils.Utils;

import java.util.Scanner;

public class RegisterCoordinatorUI implements Runnable{
    private RegisterCoordinatorController ctrl;

    public RegisterCoordinatorUI(){
        ctrl = new RegisterCoordinatorController(App.getInstance());
    }

    @Override
    public void run() {
        Scanner sc = new Scanner(System.in);

        String[] listOfVacCenterNames = ctrl.getVacCenterNames();

        int counter = 1;
        for (String name : listOfVacCenterNames) {
            System.out.println(counter + " - " + name);
            counter++;
        }
        int vacCenterSelection;
        vacCenterSelection = Utils.readIntegerFromConsole("Select Vacination Center - ");
        while(vacCenterSelection <= 0 || vacCenterSelection > listOfVacCenterNames.length){
            System.out.println("Center doesn't exist please select a new center");
            vacCenterSelection = Utils.readIntegerFromConsole("Select Vacination Center - ");
        }

        String email;
        String name;

        email=Utils.readLineFromConsole("Coordinator Email - ");
        name=Utils.readLineFromConsole("Coordinator Name - ");

        ctrl.registerCoordinator(email,listOfVacCenterNames[vacCenterSelection-1],name);

    }
}
