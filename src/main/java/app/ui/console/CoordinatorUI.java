package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class CoordinatorUI implements Runnable{
    public CoordinatorUI()
    {
    }

    public void run()
    {
        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("Vaccination Reports", new CreateVaccineUI()));
        options.add(new MenuItem("Center Performance analysis", new CenterPerformanceUI()));
        options.add(new MenuItem("Change algorithm", new ConfigAlgorithmUI()));

        int option = 0;
        do
        {
            option = Utils.showAndSelectIndex(options, "\n\nCoordinator Menu:");

            if ( (option >= 0) && (option < options.size()))
            {
                options.get(option).run();
            }
        }
        while (option != -1 );
    }
}

