package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */

public class AdminUI implements Runnable{
    public AdminUI()
    {
    }

    public void run()
    {
        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("Create Vaccination Center", new CreateVacCenterUI()));
        options.add(new MenuItem("Get List of Employees with a Role", new GetEmployeesWithRoleUI()));
        options.add(new MenuItem("Register a new Employee", new RegisterEmployeeUI()));
        options.add(new MenuItem("Create vaccines", new CreateVaccineUI()));
        options.add(new MenuItem("Register Users from file", new ReadUserFromFileUI()));

        int option = 0;
        do
        {
            option = Utils.showAndSelectIndex(options, "\n\nAdmin Menu:");

            if ( (option >= 0) && (option < options.size()))
            {
                options.get(option).run();
            }
        }
        while (option != -1 );
    }
}
