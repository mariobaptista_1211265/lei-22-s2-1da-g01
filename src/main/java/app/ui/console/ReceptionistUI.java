package app.ui.console;

import app.controller.ChooseCenterController;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;


/**
 * The type Receptionist ui.
 *
 * @author Mário Baptista nº1211265
 */
public class ReceptionistUI implements Runnable{

    public void run()
    {
        int id;
        int centerType = 0;

        do{
            System.out.println("Choose the type of center you working on... \n");
            System.out.println("1 - Mass Vaccination Centers");
            System.out.println("2 - Health Care Centers \n");
            centerType = Utils.readIntegerFromConsole("Center Type- ");
        }while(centerType!=1 && centerType !=2);



        System.out.println("What Center are you working on? \n");

        if(centerType==1){
            ChooseCenterController.showVacCenters();
        }else{
            ChooseCenterController.showHealthCenters();
        }

        int op = Utils.readIntegerFromConsole("Option - ");

        if(centerType == 1){
            id = ChooseCenterController.getVacCenterID(op);
        }else{
            id = ChooseCenterController.getHealthCenterID(op);
        }





        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("Create new NHS User ", new CreateNHSUserUI(id)));
        options.add(new MenuItem("Schedule a vaccination ", new ScheduleVaccinationUI(id,"Receptionist")));
        options.add(new MenuItem("Register a user arrival ", new RegisterUserArrivalUI(id)));

        int option = 0;
        do
        {
            try{
                option = Utils.showAndSelectIndex(options, "\n\nReceptionist Menu:");

                if ( (option >= 0) && (option < options.size()))
                {
                    options.get(option).run();

                }
            } catch (Exception e) {
                System.out.println(e);
                System.out.println("\nInvalid option. Try again.");
                option = -7;
            }
        }
        while (option != -1 );
    }
}
