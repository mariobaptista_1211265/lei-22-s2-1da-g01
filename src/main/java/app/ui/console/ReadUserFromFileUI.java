package app.ui.console;

import app.controller.App;
import app.controller.US14Controller;
import app.ui.console.utils.Utils;

import java.util.Scanner;

/**
 * The type Read user from file ui.
 */
public class ReadUserFromFileUI implements Runnable{
    private final US14Controller ctrl;

    /**
     * Instantiates a new Read user from file ui.
     */
    public ReadUserFromFileUI()
    {
        ctrl = new US14Controller(App.getInstance());
    }

    public void run() {

        Scanner UserInp = new Scanner(System.in);
        /*
        Question: "When the admin wants to upload a CSV file to be read, should the file be stored at a specific location on the computer (e.g. the desktop) or should the admin be able to choose the file he wants to upload in a file explorer?"

        Answer: The Administrator should write the file path. In Sprint C we do not ask students to develop a graphical user interface.
         */
        String FilePath = Utils.readLineFromConsole("File Path - ");

        while (!ctrl.isFileValid(FilePath)) {
            System.out.println("Error: Incorrect file format / file does not exist\nPlease write new File Path");
            FilePath = Utils.readLineFromConsole("File Path - ");
        }

        try {
            ctrl.registerUsersFromFile(FilePath);
        } catch (Exception e) {

        }


        //ctrl.SaveUsersList(UserList);

    }

}

