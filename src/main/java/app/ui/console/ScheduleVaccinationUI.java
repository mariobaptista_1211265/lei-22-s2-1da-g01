package app.ui.console;

import app.controller.App;
import app.controller.ScheduleVaccineController;
import app.ui.console.utils.Utils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.Scanner;


/**
 *
 * @author Mário Baptista <1211265@isep.ipp.pt>
 * @author Pedro Sá <1211269@isep.ipp.pt>
 */


/**
 * The type Schedule vaccination ui.
 */
public class ScheduleVaccinationUI implements Runnable {

    private int centerID = 0;
    private ScheduleVaccineController ctrl;

    private Scanner scan = new Scanner(System.in);

    /**
     * Instantiates a new Schedule vaccination ui.
     */
    public ScheduleVaccinationUI(){

    }

    /**
     * Instantiates a new Schedule vaccination ui.
     *
     * @param id the id
     */
    String Function;

    /**
     * Instantiates a new Schedule vaccination ui.
     *
     * @param id       the id
     * @param function the function
     */
    public ScheduleVaccinationUI(int id, String function){
        this.centerID = id;
        Function=function;
    }
    public void run() {
        int SNSNumber, cont=0;

        ctrl=new ScheduleVaccineController(App.getInstance());

        SimpleDateFormat hours = new SimpleDateFormat("HH");
        SimpleDateFormat minutes = new SimpleDateFormat("mm");

        //check if the user exists
        if (!Objects.equals(Function, "NHSUSER")) {
            do {
                if (cont == 1) {
                    System.out.println("That SNS number isnt registered, try again");
                }
                SNSNumber = Utils.readIntegerFromConsole("Type the SNS Number of the user: ");
                cont = 1;
            } while (ctrl.checkUniqueSNSNumber(SNSNumber));
        }
        else {
            SNSNumber=ctrl.getSNS();
        }

        int centerId = ctrl.chooseCenterForVaccination();

        String vactype = ctrl.chooseVacType(centerId);
        if (ctrl.checkDuplicateSchedules(SNSNumber, vactype)) {
            String time;
            Date date;
            do {

                System.out.println("Enter the date and time of the vaccination (dd-mm-yyyy hh:mm): ");
                String dateString = scan.nextLine();


                try {
                    date = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(dateString);
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }


                do {
                    String hour = hours.format(date);
                    String minute = minutes.format(date);


                    time = hour + ":" + minute;

                } while (!ctrl.verifyhours(time));


            } while (!ctrl.validatedate(date));



            ctrl.scheduleVaccine(SNSNumber, centerId, date, time, vactype, Function);


            ctrl.showschedules();
        }

    }
}
