package app.ui.console;
import app.controller.App;
import app.controller.US03Controller;
import app.domain.model.NHSUser;
import app.domain.shared.Constants;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 * The type Create nhs user ui.
 *
 * @author Mário Baptista nº1211265
 */
public class CreateNHSUserUI implements Runnable{

    int centerID = 0;
    private US03Controller ctrl;
    public CreateNHSUserUI(){
        ctrl=new US03Controller(App.getInstance());
    }

    /**
     * Instantiates a new Create nhs user ui.
     */
    public CreateNHSUserUI(int id)
    {
        this.centerID = id;
        ctrl = new US03Controller(App.getInstance());
    }


    public void run() {
        Scanner sc = new Scanner(System.in);


        String name = null;
        String address = null;
        String sex = null;
        int phone = 0;
        String email = null;
        Date birthDate = null;
        int SNSNumber = 0;
        int citizenCardNumber = 0;


        name = Utils.readLineFromConsole("Insert the name: ");

        address = Utils.readLineFromConsole("Insert the address: ");

        try {
            List<String> options = new ArrayList<String>();
            options.add("Male");
            options.add("Female");
            options.add("Other");
            sex = Utils.showAndSelectOne(options, "Select the sex: ").toString();
        } catch (Exception e) {
            sex = "Rather not say";
        }

        do {
            try {
                phone = Utils.readIntegerFromConsole("Insert the phone number: ");
                if (!ctrl.checkUniqueNHSUserPhone(phone)) {
                    System.out.println("The phone number is already in use, please try again");
                    phone = 0;
                } if (("" + phone).length() != Constants.NUMBER_OF_DIGITS_PHONE_NUMBER) {
                    System.out.println("The phone number must have 9 digits");
                    phone = 0;
                }
            } catch (Exception e) {
                System.out.println("There was an error with the phone number, please try again");
                phone = 0;
            }
        } while (phone == 0);

        do {
            try {
                email = Utils.readLineFromConsole("Insert the email: ");
                if (!ctrl.checkUniqueNHSUserEmail(email)) {
                    System.out.println("The email is already in use, please try again");
                    email = null;
                } if (!ctrl.validEmail(email)) {
                    System.out.println("The email is not valid, please try again");
                    email = null;
                }
            } catch (Exception e) {
                System.out.println("There was an error with the email, please try again");
                email = null;
            }
        } while (email == null);

        do {
            try {
                birthDate = Utils.readDateFromConsole("Insert the birth date (dd-mm-yyyy):");
                //Date date = new SimpleDateFormat("dd-MM-yyyy").parse(birthDate.toString());
                if (new Date().before(birthDate)) {
                    System.out.println("The birth date is not valid");
                    birthDate = null;
                }
            } catch (Exception e) {
                System.out.println("There was an error with the birth date, please try again");
                birthDate = null;
            }
        } while (birthDate == null);

        do {
            try {
                SNSNumber = Utils.readIntegerFromConsole("Insert the SNS number: ");
                if (!ctrl.checkUniqueNHSUserSNSNumber(SNSNumber)) {
                    System.out.println("The SNS number is already in use, please try again");
                    SNSNumber = 0;
                }
            } catch (Exception e) {
                System.out.println("There was an error with the SNS number, please try again");
                SNSNumber = 0;
            }
        } while (SNSNumber == 0);

        do {
            try {
                citizenCardNumber = Utils.readIntegerFromConsole("Insert the citizen card number: ");
                if (!ctrl.checkUniqueNHSUserCitizenCardNumber(citizenCardNumber)) {
                    System.out.println("The citizen card number is already in use, please try again");
                    citizenCardNumber = 0;
                }
            } catch (Exception e) {
                System.out.println("There was an error with the citizen card number, please try again");
                citizenCardNumber = 0;
            }
        } while (citizenCardNumber == 0);


        NHSUser userToSave = ctrl.createNHSUser(name, address, sex, phone, email, birthDate, SNSNumber, citizenCardNumber);

        ConfirmData(userToSave);

        ctrl.saveNHSUser();
    }

    private void ConfirmData(NHSUser userToSave){
        List<String> list = new ArrayList<String>();
        list.add("Name");
        list.add("Address");
        list.add("Sex");
        list.add("Phone number");
        list.add("email");
        list.add("Birth date");
        list.add("SNS Number");
        list.add("Citizen card number");

        boolean boo;
        do {
            System.out.println("Please confirm the user's informations: \n" + userToSave.toString());
            boo = Utils.confirm("Is everything correct? (s/n)");
            if (!boo) {
                int i = Utils.showAndSelectIndex(list, "Please select the field you want to change");
                boolean b = false;
                switch (i) {
                    case 0:
                        String name = Utils.readLineFromConsole("Insert the name: ");
                        userToSave.setName(name);
                        break;
                    case 1:
                        String address = Utils.readLineFromConsole("Insert the address: ");
                        userToSave.setAddress(address);
                        break;
                    case 2:
                        do {
                            try {
                                String sex = Utils.readLineFromConsole("Insert the sex: ");
                                userToSave.setSex(sex);
                                b = true;
                            } catch (Exception e) {
                                System.out.println("There was an error with the sex, please try again");
                            }
                        } while (!b);
                        break;
                    case 3:
                        do {
                            try {
                                int phone = Utils.readIntegerFromConsole("Insert the phone number: ");
                                userToSave.setPhone(phone);
                                b = true;
                                if (!ctrl.checkUniqueNHSUserPhone(phone)) {
                                    System.out.println("The phone number is already in use, please try again");
                                    b = false;
                                } if (("" + phone).length() != Constants.NUMBER_OF_DIGITS_PHONE_NUMBER) {
                                    System.out.println("The phone number must have 9 digits");
                                    b = false;
                                }
                            } catch (Exception e) {
                                System.out.println("There was an error with the phone number, please try again");
                            }
                        } while (!b);
                        break;
                    case 4:
                        do {
                            try {
                                String email = Utils.readLineFromConsole("Insert the email: ");
                                userToSave.setEmail(email);
                                b = true;
                                if (!ctrl.checkUniqueNHSUserEmail(email)) {
                                    System.out.println("The email is already in use, please try again");
                                    b = false;
                                } if (!ctrl.validEmail(email)) {
                                    System.out.println("The email is not valid, please try again");
                                    b = false;
                                }

                            } catch (Exception e) {
                                System.out.println("There was an error with the email, please try again");
                            }
                        } while (!b);
                        break;
                    case 5:
                        do {
                            try {
                                Date birthDate = Utils.readDateFromConsole("Insert the birth date (dd-mm-yyyy): ");
                                userToSave.setBirthDate(birthDate);
                                b = true;
                                if (new Date().before(birthDate)) {
                                    System.out.println("The birth date is not valid");
                                    b = false;
                                }
                            } catch (Exception e) {
                                System.out.println("There was an error with the birth date, please try again");
                            }
                        } while (!b);
                        break;
                    case 6:
                        do {
                            try {
                                int SNSNumber = Utils.readIntegerFromConsole("Insert the SNS number: ");
                                userToSave.setSNSNumber(SNSNumber);
                                b = true;
                                if (!ctrl.checkUniqueNHSUserSNSNumber(SNSNumber)) {
                                    System.out.println("The SNS number is already in use, please try again");
                                    b = false;
                                }
                            } catch (Exception e) {
                                System.out.println("There was an error with the SNS number, please try again");
                            }
                        } while (!b);

                        break;
                    case 7:
                        do {
                            try {
                                int citizenCardNumber = Utils.readIntegerFromConsole("Insert the citizen card number: ");
                                userToSave.setCitizenCardNumber(citizenCardNumber);
                                b = true;
                                if (!ctrl.checkUniqueNHSUserCitizenCardNumber(citizenCardNumber)) {
                                    System.out.println("The citizen card number is already in use, please try again");
                                    b = false;
                                }
                            } catch (Exception e) {
                                System.out.println("There was an error with the citizen card number, please try again");
                            }
                        } while (!b);

                        break;
                }
            }
        } while (!boo);
    }
}
