package app.ui.console;

import app.controller.App;
import app.controller.US10Controller;
import app.domain.model.Employee;
import app.domain.shared.Constants;
import app.ui.console.utils.Utils;
import pt.isep.lei.esoft.auth.mappers.dto.UserRoleDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

/**
 * The type Register employee ui.
 */
public class RegisterEmployeeUI implements Runnable{
    private final US10Controller ctrl;

    /**
     * Instantiates a new Register employee ui.
     */
    public RegisterEmployeeUI()
    {
        ctrl = new US10Controller(App.getInstance());
    }

    public void run() {

        Scanner Input = new Scanner(System.in);

        String name = "";
        String address = "";
        int phone = 0;
        String email = "";
        int citizenCardNumber = 0;
        String postCode = "";
        String city = "";

        List<UserRoleDTO> RoleList = ctrl.getListOfRoles();
        //System.out.println(RoleList);

        System.out.println("List of Roles: \n");
        for (UserRoleDTO role : RoleList) {
            System.out.println(role.getId() + " - " + role.getDescription());
        }
        System.out.print("Role Selection: ");
        String inpRole = "";
        inpRole = Input.next();

        while (!ctrl.roleExists(inpRole)){
            System.out.print("Role doesnt exist. Please select another one: ");
            inpRole = Input.next();
        }

        if (inpRole.toLowerCase(Locale.ROOT).equals(Constants.ROLE_COORDINATOR.toLowerCase(Locale.ROOT))){
            (new RegisterCoordinatorUI()).run();
            return;
        } else if (inpRole.toLowerCase(Locale.ROOT).equals(Constants.ROLE_NHS_USER.toLowerCase(Locale.ROOT))){
            (new CreateNHSUserUI()).run();
            return;
        }

        boolean flag = false;

        while(name.trim().isEmpty()) {
            name = Utils.readLineFromConsole("Name - ");
        }
        while(address.trim().isEmpty()) {
            address = Utils.readLineFromConsole("Address - ");
        }
        phone = Utils.readIntegerFromConsole("Phone - ");
        while (!ctrl.checkPhone(phone)){
            phone = Utils.readIntegerFromConsole("Invalid Phone number (format: 999999999) \nInsert new phone number - ");
        }
        email = Utils.readLineFromConsole("Email - ");
        while (!ctrl.checkEmail(email)){
            email = Utils.readLineFromConsole("Invalid Email\nInsert new Email - ");
        }
        citizenCardNumber = Utils.readIntegerFromConsole("Citizen Card Number - ");
        while (!ctrl.checkCitizenCardNumber(citizenCardNumber)){
            citizenCardNumber = Utils.readIntegerFromConsole("Invalid Citizen Card Number (format: 99999999) \nInsert new Citizen Card Number - ");
        }
        while(postCode.trim().isEmpty()) {
            postCode = Utils.readLineFromConsole("PostCode - ");
        }
        while(city.trim().isEmpty()) {
            city = Utils.readLineFromConsole("City - ");
        }
        Employee RegEmployee = ctrl.createEmployee(name, address, phone, email, citizenCardNumber, postCode, city, inpRole);

        System.out.println(RegEmployee.toString());
        List<String> list = new ArrayList<String>();
        list.add("Name");
        list.add("Address");
        list.add("Phone number");
        list.add("email");
        list.add("Citizen card number");
        list.add("postCode");
        list.add("City");
        list.add("Role");
        flag = Utils.confirm("Is everything correct? (s/n)");

        while (!flag) {
            int i = Utils.showAndSelectIndex(list, "Please select the field you want to change");
            switch (i) {
                case 0: {
                    name = Utils.readLineFromConsole("Name - ");
                    while(name.trim().isEmpty()) {
                        name = Utils.readLineFromConsole("Name - ");
                    }
                    RegEmployee.setName(name);
                }
                break;
                case 1: {
                    address = Utils.readLineFromConsole("Address - ");
                    while(address.trim().isEmpty()) {
                        address = Utils.readLineFromConsole("Address - ");
                    }
                    RegEmployee.setAddress(address);
                }
                break;
                case 2: {
                    phone = Utils.readIntegerFromConsole("Phone - ");
                    while (!ctrl.checkPhone(phone)) {
                        phone = Utils.readIntegerFromConsole("Invalid Phone number (format: 999999999)\nInsert new phone number - ");
                    }
                    RegEmployee.setPhone(phone);
                }
                break;
                case 3: {
                    email = Utils.readLineFromConsole("Email - ");
                    while (!ctrl.checkEmail(email)) {
                        email = Utils.readLineFromConsole("Invalid Email\nInsert new Email - ");
                    }
                    RegEmployee.setEmail(email);
                }
                break;
                case 4: {
                    citizenCardNumber = Utils.readIntegerFromConsole("Citizen Card Number - ");
                    while (!ctrl.checkCitizenCardNumber(citizenCardNumber)) {
                        citizenCardNumber = Utils.readIntegerFromConsole("Invalid Citizen Card Number (format: 99999999) \nInsert new Citizen Card Number - ");
                    }
                    RegEmployee.setCitizenCardNumber(citizenCardNumber);
                }
                break;
                case 5: {
                    postCode = Utils.readLineFromConsole("PostCode - ");
                    while(postCode.trim().isEmpty()) {
                        postCode = Utils.readLineFromConsole("PostCode - ");
                    }
                    RegEmployee.setPostCode(postCode);
                }
                break;
                case 6: {
                    city = Utils.readLineFromConsole("City - ");
                    while(city.trim().isEmpty()) {
                        city = Utils.readLineFromConsole("City - ");
                    }
                    RegEmployee.setCity(city);
                }
                break;
                case 7: {
                    for (UserRoleDTO role : RoleList) {
                        System.out.println(role.getId() + " - " + role.getDescription());
                    }
                    System.out.print("Role Selection: ");
                    inpRole = Input.next();

                    while (!ctrl.roleExists(inpRole)) {
                        System.out.print("Role doesnt exist. Please select another one: ");
                        inpRole = Input.next();
                    }
                    RegEmployee.setRole(inpRole);
                }
                break;
            }
            System.out.println(RegEmployee.toString());
            flag = Utils.confirm("Is everything correct? (s/n)");
        }
        ctrl.saveEmployee(RegEmployee);
    }
}
