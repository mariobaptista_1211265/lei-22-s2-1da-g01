package app.ui.console;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class DevTeamUI implements Runnable{

    public DevTeamUI()
    {

    }
    public void run()
    {
        System.out.println("\n");
        System.out.printf("Development Team:\n");
        System.out.printf("\t João Fortuna - 1211261@isep.ipp.pt \n");
        System.out.printf("\t Mário Baptista - 1211265@isep.ipp.pt \n");
        System.out.printf("\t Pedro Sá - 1211269@isep.ipp.pt \n");
        System.out.printf("\t Tiago Couto - 1211272@isep.ipp.pt \n");
        System.out.printf("\t Manuel Pimentel - 1210670@isep.ipp.pt \n");
        System.out.println("\n");
    }
}
