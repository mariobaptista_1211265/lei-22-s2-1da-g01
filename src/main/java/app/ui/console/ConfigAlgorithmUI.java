package app.ui.console;

import app.controller.App;
import app.controller.US16Controller;
import app.ui.console.utils.Utils;

import java.io.IOException;

public class ConfigAlgorithmUI implements Runnable{

    private US16Controller ctrl;
    public ConfigAlgorithmUI(){
        {
            ctrl = new US16Controller(App.getInstance());
        }
    }

    public void run(){

        while (true) {
            int currentAlgorithm = ctrl.getAlgorithmFromProps();
            if (currentAlgorithm == -1) {
                return;
            }
            System.out.print("Current Algorithm: ");
            switch (currentAlgorithm) {
                case 0: {
                    System.out.println("brute-force");
                }
                break;
                case 1: {
                    System.out.println("benchmark");
                }
                break;
            }
            System.out.println("Change to: ");
            System.out.println("0 - return");
            System.out.println("1 - brute-force");
            System.out.println("2 - benchmark");
            int userSelection = -1;
            do {
                userSelection = Utils.readIntegerFromConsole("Selection: ");
                if (userSelection == 0) {
                    return;
                }
            } while (userSelection < 0 || userSelection > 2);
            try {
                ctrl.changeAlgorithmProps(userSelection - 1);
            } catch (IOException e) {
                System.out.println("Error in saving changes to config file.");
                return;
            }
        }

    }

}
