package app.ui.console;

import app.controller.App;
import app.controller.US04Controller;
import app.domain.shared.Constants;
import app.ui.console.utils.Utils;

/**
 * The Register a user arrival ui.
 */
public class RegisterUserArrivalUI implements Runnable{

    private int centerID;
    private US04Controller ctrl;

    /**
     * Instantiates a new Register user arrival ui.
     *
     * @param id the center id
     * @author João Fortuna <1211261@isep.ipp.pt>
     */
    public RegisterUserArrivalUI(int id){
        this.centerID = id;
        ctrl=new US04Controller(App.getInstance());
    }

    @Override
    public void run(){

        int number = 0;

        do{
            number = Utils.readIntegerFromConsole("Please provide the user's NHS Number - ");
        }while(!(("" + number).length() == Constants.NUMBER_OF_DIGITS_NHS_NUMBER));



       if(ctrl.doesUserScheduleExistInCenter(number,centerID)){

            if(ctrl.checkIfInTimeToArrive(number,centerID)){

                if(ctrl.checkDuplicatedEntry(number,centerID)){

                    if(ConfirmData(number,centerID)){

                        ctrl.createUserArrival(number,centerID);
                        System.out.println("User added to the waiting room.");

                    }else System.out.println("User was not added.");

                }else System.out.println("This user already has an appointment.");


            }else System.out.println("This user did not come in time.");


       }else System.out.println("This user doesn't have a registered appointment.");

    }


    /**
     * Confirm data boolean.
     *
     * @param number   the nhs number
     * @param centerID the center id
     * @return the boolean
     */
    public boolean ConfirmData(int number, int centerID){

        String answer = null;

        ctrl.showAScheduledVaccine(number, centerID);

        do {

            answer = Utils.readLineFromConsole("Do you want to add this user to the waiting room? (s/n) -");

        }while(answer.equalsIgnoreCase("s") && answer.equalsIgnoreCase("n"));


        if(answer != "s") return true;
        else return false;


    }

}
