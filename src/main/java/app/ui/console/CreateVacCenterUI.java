package app.ui.console;

import app.controller.App;
import app.controller.US09Controller;
import app.domain.model.HealthCareCenter;
import app.domain.model.VaccinationCenter;
import app.domain.shared.Constants;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Create a vaccination/healthcare center ui.
 *
 * @author João Fortuna <1211261@isep.ipp.pt>
 */
public class CreateVacCenterUI implements Runnable{

    private US09Controller ctrl;

    public CreateVacCenterUI(){
        ctrl = new US09Controller(App.getInstance());
    }

    @Override
    public void run() {

        int id = 0;
        String name = null;
        String address = null;
        String emailAddress = null;
        String openingHours = null;
        String closingHours = null;
        String slotDuration = null;
        int phoneNumber = 0;
        int slots = 0;
        int maximumVacSlot = 0;



        int op = 0;


        do{
            System.out.println("What type of center do you want to create? \n 1- MassVaccinationCenter \n 2- HealthCareCenter \n ");
            op = Utils.readIntegerFromConsole("Option - ");
        }while(op!=1 && op !=2);




        if(op == 1){
            System.out.println("Creating a Vaccination Center...");
            System.out.println("--------------------------------");

            do{
                id = Utils.readIntegerFromConsole("ID - ");
            }while(ctrl.checkUniqueCenterID(id) == false);

            name = Utils.readLineFromConsole("Name - ");

            address = Utils.readLineFromConsole("Address - ");

            do {
                phoneNumber = Utils.readIntegerFromConsole("Phone Number - ");
            }while(validatePhone(phoneNumber) == false || ctrl.checkUniqueVacPhone(phoneNumber) == false);

            do{
                emailAddress = Utils.readLineFromConsole("Email Address - ");
            }while(validateEmail(emailAddress) == false || ctrl.checkUniqueVacEmail(emailAddress) == false);


            openingHours = Utils.readLineFromConsole("Opening Hours - ");

            closingHours = Utils.readLineFromConsole("Closing Hours - ");

            slotDuration = Utils.readLineFromConsole("Slot Duration - ");

            slots = Utils.readIntegerFromConsole("Slots - ");

            maximumVacSlot = Utils.readIntegerFromConsole("Maximum Vaccine Per Slot - ");

            VaccinationCenter vcenter = ctrl.createVacCenter(id,name,address,phoneNumber,emailAddress,openingHours,closingHours,slotDuration,slots,maximumVacSlot);

            ctrl.showVacTypes();
            int x = Utils.readIntegerFromConsole("Insert the type to add: ");
            vcenter.setVACVaccinetype(ctrl.getVAVVacType(x));

            ConfirmVacData(vcenter);

            ctrl.saveVacCenter(vcenter);

        }else{
            System.out.println("Creating a Health Care Center...");
            System.out.println("--------------------------------");


            do{
                id = Utils.readIntegerFromConsole("ID - ");
            }while(ctrl.checkUniqueCenterID(id) == false);

            name = Utils.readLineFromConsole("Name - ");

            address = Utils.readLineFromConsole("Address - ");

            do {
                phoneNumber = Utils.readIntegerFromConsole("Phone Number - ");
            }while(validatePhone(phoneNumber) == false || ctrl.checkUniqueHealthPhone(phoneNumber) == false);

            do{
                emailAddress = Utils.readLineFromConsole("Email Address - ");
            }while(validateEmail(emailAddress) == false || ctrl.checkUniqueHealthEmail(emailAddress) == false);

            openingHours = Utils.readLineFromConsole("Opening Hours - ");

            closingHours = Utils.readLineFromConsole("Closing Hours - ");

            slotDuration = Utils.readLineFromConsole("Slot Duration - ");

            slots = Utils.readIntegerFromConsole("Slots - ");

            maximumVacSlot = Utils.readIntegerFromConsole("Maximum Vaccine Per Slot - ");

            String ARS = Utils.readLineFromConsole("ARS - ");

            String AGES = Utils.readLineFromConsole("AGES - ");

            HealthCareCenter hcenter = ctrl.createHealthCenter(id,name,address,phoneNumber,emailAddress,openingHours,closingHours,slotDuration,slots,maximumVacSlot,ARS,AGES);

            boolean done;

            do{
                ctrl.showVacTypes();
                int y = Utils.readIntegerFromConsole("Insert the type to add: ");
                hcenter.addHealthType(ctrl.getVAVVacType(y));

                done = Utils.confirm("Do you want to add another type? (s/n)");

            }while(done==true);

            ConfirmHealthData(hcenter);

            ctrl.saveHealthCenter(hcenter);


        }

    }

    /**
     * Where the admin will confirm the data of the vaccination center being created.
     */
    private void ConfirmVacData(VaccinationCenter vcenter1){
        List<String> list = new ArrayList<String>();
        list.add("Name");
        list.add("Address");
        list.add("Phone Number");
        list.add("Email");
        list.add("Opening Hours");
        list.add("Closing Hours");
        list.add("Slots");
        list.add("Slot Duration");
        list.add("Maximum Vaccines per slot");
        list.add("Vaccine Types");

        boolean boo;
        do {
            System.out.println("Please confirm the center's informations: \n" + vcenter1.toString());
            boo = Utils.confirm("Is everything correct? (s/n)");
            if (!boo) {
                int i = Utils.showAndSelectIndex(list, "Please select the field you want to change- ");
                switch (i) {
                    case 0:

                        String name = Utils.readLineFromConsole("Insert the name: ");
                        vcenter1.setName(name);

                        break;

                    case 1:

                        String address = Utils.readLineFromConsole("Insert the address: ");
                        vcenter1.setAddress(address);

                        break;

                    case 2:
                        int phone = 0;
                        do {
                            phone = Utils.readIntegerFromConsole("Phone Number - ");
                        }while(validatePhone(phone) == false || ctrl.checkUniqueVacPhone(phone) == false);
                        vcenter1.setPhoneNumber(phone);

                        break;

                    case 3:
                        String emailAddress = null;

                        do{
                            emailAddress = Utils.readLineFromConsole("Email Address - ");
                        }while(validateEmail(emailAddress) == false || ctrl.checkUniqueVacEmail(emailAddress) == false);
                            vcenter1.setEmailAddress(emailAddress);

                        break;

                    case 4:
                        String openingHours = Utils.readLineFromConsole("Insert the opening hours: ");
                        vcenter1.setOpeningHours(openingHours);
                        break;

                    case 5:
                        String closingHours = Utils.readLineFromConsole("Insert the closing hours: ");
                        vcenter1.setClosingHours(closingHours);
                        break;

                    case 6:
                        int slots = Utils.readIntegerFromConsole("Insert the number of slots: ");
                        vcenter1.setSlots(slots);
                        break;

                    case 7:
                        String slotDuration = Utils.readLineFromConsole("Insert the slots duration: ");
                        vcenter1.setSlotDuration(slotDuration);
                        break;

                    case 8:
                        int maximumVacPerSlots = Utils.readIntegerFromConsole("Insert the maximum of vaccines per slot: ");
                        vcenter1.setMaximumVacSlot(maximumVacPerSlots);
                        break;

                    case 9:
                        ctrl.showVacTypes();
                        int op = Utils.readIntegerFromConsole("Insert the type to add: ");
                        vcenter1.setVACVaccinetype(ctrl.getVAVVacType(op));

                }
            }
        } while (!boo);
    }

    /**
     * Where the admin will confirm the data of the healthcare center being created.
     */
    private void ConfirmHealthData(HealthCareCenter hcenter1){
        List<String> list = new ArrayList<String>();

        list.add("Name");
        list.add("Address");
        list.add("Phone Number");
        list.add("Email");
        list.add("Opening Hours");
        list.add("Closing Hours");
        list.add("Slots");
        list.add("Slot Duration");
        list.add("Maximum Vaccines per slot");
        list.add("ARS");
        list.add("AGES");
        list.add("Vaccine Types");

        boolean boo;
        do {
            System.out.println("Please confirm the center's informations: \n" + hcenter1.toString());
            hcenter1.getHealthTypes();

            boo = Utils.confirm("Is everything correct? (s/n)");
            if (!boo) {
                int i = Utils.showAndSelectIndex(list, "Please select the field you want to change- ");
                switch (i) {
                    case 0:

                        String name = Utils.readLineFromConsole("Insert the name: ");
                        hcenter1.setName(name);

                        break;

                    case 1:

                        String address = Utils.readLineFromConsole("Insert the address: ");
                        hcenter1.setAddress(address);

                        break;

                    case 2:
                        int phone = 0;
                        do {
                            phone = Utils.readIntegerFromConsole("Phone Number - ");
                        }while(validatePhone(phone) == false || ctrl.checkUniqueHealthPhone(phone) == false);
                        hcenter1.setPhoneNumber(phone);

                        break;

                    case 3:
                        String emailAddress = null;

                        do{
                            emailAddress = Utils.readLineFromConsole("Email Address - ");
                        }while(validateEmail(emailAddress) == false || ctrl.checkUniqueHealthEmail(emailAddress) == false);
                        hcenter1.setEmailAddress(emailAddress);

                        break;

                    case 4:
                        String openingHours = Utils.readLineFromConsole("Insert the opening hours: ");
                        hcenter1.setOpeningHours(openingHours);
                        break;

                    case 5:
                        String closingHours = Utils.readLineFromConsole("Insert the closing hours: ");
                        hcenter1.setClosingHours(closingHours);
                        break;

                    case 6:
                        int slots = Utils.readIntegerFromConsole("Insert the number of slots: ");
                        hcenter1.setSlots(slots);
                        break;

                    case 7:
                        String slotDuration = Utils.readLineFromConsole("Insert the slots duration: ");
                        hcenter1.setSlotDuration(slotDuration);
                        break;

                    case 8:
                        int maximumVacPerSlots = Utils.readIntegerFromConsole("Insert the maximum of vaccines per slot: ");
                        hcenter1.setMaximumVacSlot(maximumVacPerSlots);
                        break;

                    case 9:
                        String ARS = Utils.readLineFromConsole("Insert the ARS: ");
                        hcenter1.setARS(ARS);
                        break;

                    case 10:
                        String AGES = Utils.readLineFromConsole("Insert the AGES: ");
                        hcenter1.setAGES(AGES);
                        break;

                    case 11:
                        int x = Utils.readIntegerFromConsole("Insert the number of the type to change: ");
                        ctrl.showVacTypes();
                        int op = Utils.readIntegerFromConsole("Insert the type to add: ");
                        hcenter1.setHealthVacType(x,ctrl.getVAVVacType(op));


                }
            }
        } while (!boo);
    }

    /**
     * Local phone number validation ( if the number has 9 digits)
     */
    private boolean validatePhone(int number){
        if(("" + number).length() == Constants.NUMBER_OF_DIGITS_PHONE_NUMBER){
            return true;
        }
        System.out.println("Phone number doesn't have " + Constants.NUMBER_OF_DIGITS_PHONE_NUMBER + " digits, please try again.");
        return false;

    }

    /**
     * Local email validation ( if the email contains "@" and ".")
     */
    private boolean validateEmail(String email){

        if(email.contains(".") && email.contains(("@"))){
            return true;
        }
        System.out.println("Invalid email, please try again.");
        return false;
    }
}
