package app.ui.console;

import app.controller.ChooseCenterController;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class NurseUI implements Runnable {
    @Override
    public void run()
    {


        int centerID;
        int centerType = 0;

        do{
            System.out.println("Choose the type of center you working on... \n");
            System.out.println("1 - Mass Vaccination Centers");
            System.out.println("2 - Health Care Centers \n");
            centerType = Utils.readIntegerFromConsole("Center Type- ");
        }while(centerType!=1 && centerType !=2);



        System.out.println("What Center are you working on? \n");

        if(centerType==1){
            ChooseCenterController.showVacCenters();
        }else{
            ChooseCenterController.showHealthCenters();
        }

        int Option  = Utils.readIntegerFromConsole("Option - ");

        if(centerType == 1){
            centerID = ChooseCenterController.getVacCenterID(Option);
        }else{

            centerID = ChooseCenterController.getHealthCenterID(Option);
        }

        boolean view = false;
        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("View the users in certain Waiting Room", new GetWaitingRoomUsersUI()));
        options.add(new MenuItem("Record a Vaccine", new RecordVaccineUI(centerID)));
        int option = 0;
        do
        {
            option = Utils.showAndSelectIndex(options, "\n\nNurse Menu");

            if ( (option >= 0) && (option < options.size()))
            {
                options.get(option).run();
            }
        }
        while (option != -1 );
    }
}