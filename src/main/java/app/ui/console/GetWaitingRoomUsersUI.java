package app.ui.console;


import app.controller.App;
import app.controller.US05Controller;
import app.domain.dto.WaitingRoomDTO;
import app.ui.console.utils.Utils;

import java.util.List;
import java.util.Scanner;

/**
 * The type Get waiting room users ui.
 *
 * @author Manuel Pimentel <1210670@isep.ipp.pt>
 */
public class GetWaitingRoomUsersUI implements Runnable {

    /**
     * The Ctrl.
     */
    US05Controller ctrl = new US05Controller(App.getInstance());


    @Override
    public void run() {
        Scanner scan = new Scanner(System.in);

        int centerType = 0, center = 0, idCenter;
        do{
            System.out.println("Choose the Center of the Waiting Room \n");
            System.out.println("1 - Mass Vaccination Centers");
            System.out.println("2 - Health Care Centers \n");
            centerType = Utils.readIntegerFromConsole("Center Type- ");
        }while(centerType!=1 && centerType !=2);

        if(centerType==1){
            ctrl.showVacCenters();
            center = Utils.readIntegerFromConsole("Center- ");
            idCenter = ctrl.getVacID(center);
        }else{
            ctrl.showHealthCenters();
            center = Utils.readIntegerFromConsole("Center- ");
            idCenter = ctrl.getHealthID(center);
        }

        List<WaitingRoomDTO> ListWaiting = ctrl.showWaitingRoomFromCenter(idCenter);

        if(ListWaiting.size() >= 1){
            if(centerType==1) System.out.println("\n List of Users in the Waiting Room in the Center " + ctrl.getVacCenterName(idCenter) + "\n");
            else System.out.println("\n List of Users in the Waiting Room in the Center " + ctrl.getHealthCenterName(idCenter) + "\n");

            int i = 1;

            for(WaitingRoomDTO user : ListWaiting){
                System.out.println(i + " - " + user.getName() + " | " + user.getDateOfArrival().toString());
                i++;
            }
        }
        else System.out.println("There are no users in this Waiting Room!");
    }
}
