package app.ui.console.stores;

import app.domain.model.NHSUser;
import app.domain.model.Serializable;
import app.domain.model.Vaccine;
import app.domain.shared.Constants;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class VaccineStore {

    private static List<Vaccine> Vaccines = new ArrayList<>();

    public VaccineStore(){

    }
    public static Vaccine CreateVaccine(String brand)
    {
        System.out.println("Creating Vaccine... ");
        return new Vaccine(brand);
    }
    public static void SaveVaccine(Vaccine brand){
        Vaccines.add(brand);
        System.out.println("Vaccine created: ");
        for (Vaccine Vaccine0 : Vaccines) { // ver se guarda
        }
        Serializable.ExportDataToBinaryFile(Collections.singletonList(Vaccines), Constants.VACCINES_FILE_NAME);
    }

    public static void ImportData(){
        String FileName = Constants.VACCINES_FILE_NAME;
        File file = new File(FileName);

        try
        {
            file.createNewFile();
            List<List<Object>> listaAuxiliar = new ArrayList<>();
            FileInputStream fis = new FileInputStream(file);
            if (fis.available() > 0) {
                ObjectInputStream ois = new ObjectInputStream(fis);

                listaAuxiliar = (List<List<Object>>) ois.readObject();
                ois.close();
                fis.close();

                List<Object> a = listaAuxiliar.get(0);

                Vaccines = (List<Vaccine>)(Object) a;
            }
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
        catch (ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
        }
    }

    public static boolean CheckDups(String brand){
        for (Vaccine Vaccine1 : Vaccines) {
            if (Vaccine1.getBrand().equals(brand)) {
                System.out.println("This brand already exists, please try again.");
                return false;
            }
        }
        return true;
    }
    public static boolean CheckDups2(String brand){
        boolean exist=false;
        for (Vaccine Vaccine2 : Vaccines) {
            if (Vaccine2.getBrand().equals(brand)) {
                exist=true;
                return false;
            }
        }
        if (!exist)
        {
            System.out.println("This brand doesn't exist, please try again.");
            return true;
        }
        else {
            return false;
        }
    }


    public static void showVaccines(){
        System.out.println("\nVaccines: ");

        int i = 1;

        for (Vaccine Vaccine3 : Vaccines) {
            System.out.println(i + " - " +Vaccine3.getBrand());
            i++;
        }
    }

    public String[] getVaccines(){
        String[] vaccines = new String[Vaccines.size()];
        int i = 0;
        for (Vaccine Vaccine4 : Vaccines) {
            vaccines[i] = Vaccine4.getBrand();
            i++;
        }
        return vaccines;
    }



    public static String getVaccineName(int option){
        return Vaccines.get(option-1).getBrand();
    }


    public static int vaccinescount(){
        return Vaccines.toArray().length;
    }
}
