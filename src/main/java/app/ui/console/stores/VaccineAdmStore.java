package app.ui.console.stores;

import app.domain.model.AdministrationProcess;
import app.domain.model.NHSUser;
import app.domain.model.Serializable;
import app.domain.shared.Constants;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class VaccineAdmStore {

    private static List<AdministrationProcess> Adm = new ArrayList<>();

    public VaccineAdmStore(){

    }
    public static AdministrationProcess createProcess(String brand,String vactype ,int ageI, int ageF, double dosage, double numberOfDoses, int timeInterval){
        System.out.println("Creating administration process... ");
        return new AdministrationProcess(brand,vactype ,ageI, ageF, dosage, numberOfDoses, timeInterval);
    }
    public static void saveProcess(String brand,String vactype ,int ageI, int ageF, double dosage, double numberOfDoses, int timeInterval)
    {
        Adm.add(new AdministrationProcess(brand,vactype,ageI,ageF,dosage,numberOfDoses,timeInterval));
        System.out.println("Process created: ");
        for (AdministrationProcess Adm1 : Adm) { // ver se guarda
        }
        Serializable.ExportDataToBinaryFile(Collections.singletonList(Adm), Constants.VACCINE_ADM_FILE_NAME);
    }

    public static void ImportData(){
        String FileName = Constants.VACCINE_ADM_FILE_NAME;
        File file = new File(FileName);

        try
        {
            file.createNewFile();
            List<List<Object>> listaAuxiliar = new ArrayList<>();
            FileInputStream fis = new FileInputStream(file);
            if (fis.available() > 0) {
                ObjectInputStream ois = new ObjectInputStream(fis);

                listaAuxiliar = (List<List<Object>>) ois.readObject();
                ois.close();
                fis.close();

                List<Object> a = listaAuxiliar.get(0);

                Adm = (List<AdministrationProcess>)(Object) a;
            }
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
        catch (ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
        }
    }

    public static boolean CheckDupsAdm(String brand, String vactype){
        for (AdministrationProcess Adm2 : Adm) {
            if (Adm2.getBrand().equals(brand) && Adm2.getvactype().equals(vactype)) {
                System.out.println("This combination of brand and vaccine type already has a process, please try again.");
                return false;
            }
        }
        return true;
    }
    public static boolean CheckBrand(String brand){
        for (AdministrationProcess Adm2 : Adm) {
            if (Adm2.getBrand().equals(brand)) {
                return false;
            }
        }
        return true;
    }
    public static String[] getdetails(String brand){
        String detalhes[]=new String[7];
        for (AdministrationProcess Adm2 : Adm) {
            if (Adm2.getBrand().equals(brand)) {
                detalhes[0]=Adm2.getBrand();
                detalhes[1]=Adm2.getvactype();
                detalhes[2]= String.valueOf(Adm2.getAgeI());
                detalhes[3]= String.valueOf(Adm2.getAgeF());
                detalhes[4]= String.valueOf(Adm2.getDosage());
                detalhes[5]= String.valueOf(Adm2.getNumberOfDoses());
                detalhes[6]= String.valueOf(Adm2.getTimeInterval());
                return (detalhes);
            }
        }
        return null;
    }


    public static double getAdministrationDoses(String brand){
        for (AdministrationProcess Adm2 : Adm) {
            if (Adm2.getBrand().equals(brand)) {
                return Adm2.getNumberOfDoses();
            }
        }
        return 0;
    }
    public static List<AdministrationProcess> getProcesses() {
        return Adm;
    }
}
