package app.ui.console.stores;

import app.domain.model.NHSUser;
import app.domain.model.Serializable;
import app.domain.model.VaccineSchedule;
import app.domain.shared.Constants;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Mário Baptista <1211265@isep.ipp.pt>
 * @author Pedro Sá <1211269@isep.ipp.pt>
 * @author João Fortuna <1211261@isep.ipp.pt>
 */

/**
 * The type Vaccine schedule store.
 */
public class VaccineScheduleStore {
    private static List<VaccineSchedule> scheduledVaccines = new ArrayList<>();

    /**
     * Gets scheduled vaccines.
     *
     * @return the scheduled vaccines
     */
    public static List<VaccineSchedule> getScheduledVaccines() {
        return scheduledVaccines;
    }

    /**
     * Add scheduled vaccine.
     *
     * @param vaccineSchedule the vaccine schedule
     */
    public static void addScheduledVaccine(VaccineSchedule vaccineSchedule) {
        scheduledVaccines.add(vaccineSchedule);
        System.out.println("Vaccine scheduled successfully");
        Serializable.ExportDataToBinaryFile(Collections.singletonList(scheduledVaccines), Constants.VACCINE_SCHEDULE_FILE_NAME);
    }

    public static void ImportData(){
        String FileName = Constants.VACCINE_SCHEDULE_FILE_NAME;
        File file = new File(FileName);

        try
        {
            file.createNewFile();
            List<List<Object>> listaAuxiliar = new ArrayList<>();
            FileInputStream fis = new FileInputStream(file);
            if (fis.available() > 0) {
                ObjectInputStream ois = new ObjectInputStream(fis);

                listaAuxiliar = (List<List<Object>>) ois.readObject();
                ois.close();
                fis.close();

                List<Object> a = listaAuxiliar.get(0);

                scheduledVaccines = (List<VaccineSchedule>)(Object) a;
            }
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
        catch (ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
        }
    }

    /**
     * Does user schedule exist in center boolean.
     *
     * @param number the number
     * @param id     the id
     * @return the boolean
     */
    public static boolean doesUserScheduleExistInCenter(int number,int id){
        for (VaccineSchedule schedule : scheduledVaccines) {
            if (schedule.getSNSNumber() == number && schedule.getCenter() == id) {
                return true;
            }
        }
        return false;
    }

    /**
     * Showschedules.
     */
    public static void showschedules(){
        for (VaccineSchedule schedule : scheduledVaccines) {
            System.out.println(schedule);
        }
    }

    /**
     * Get a scheduled vaccine vaccine schedule.
     *
     * @param number   the number
     * @param centerID the center id
     * @return the vaccine schedule
     */
    public static VaccineSchedule getAScheduledVaccine(int number, int centerID){

        for (VaccineSchedule schedule : scheduledVaccines) {
            if (number == schedule.getSNSNumber() && centerID == schedule.getCenter()) {
                return schedule;
            }
        }

        return null;
    }

    /**
     * Check duplicate schedules boolean.
     *
     * @param SnsNumber the sns number
     * @param Vactype   the vactype
     * @return the boolean
     */
    public static boolean checkDuplicateSchedules(int SnsNumber, String Vactype) {
        Date hoje = new Date();
        for (VaccineSchedule sch : scheduledVaccines) {
            if (sch.getSNSNumber() == SnsNumber && sch.getVacType().equals(Vactype) && sch.getDate().after(hoje)) {
                System.out.println("This User already has scheduled for that Vaccine Type.");
                return false;
            }
        }
        return true;
    }

    public static Date getScheduledDateTime(int snsNumber){
        for (VaccineSchedule schedule : scheduledVaccines) {
            if (schedule.getSNSNumber() == snsNumber) {
                return schedule.getDate();
            }
        }
        return null;
    }
}