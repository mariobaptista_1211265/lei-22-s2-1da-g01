package app.ui.console.stores;


import app.domain.dto.VaccinationDTO;
import app.domain.model.Serializable;
import app.domain.model.VacAdministration;
import app.domain.shared.Constants;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.SimpleDateFormat;
import java.util.*;

public class VaccinationLogStore {

    private static List<VacAdministration> VacConfirmation = new ArrayList<>();

    private static SimpleDateFormat dateFormater = new SimpleDateFormat("dd-MM-yyyy");

    public VaccinationLogStore(){

    }


    public static VacAdministration createRecord(VaccinationDTO vacConfirmation) {
        System.out.println("\nCreating a Vaccine Record... ");
        return new VacAdministration(vacConfirmation);

    }

    public static void saveRecord(VacAdministration vacConfirmation) {
        VacConfirmation.add(vacConfirmation);
        System.out.println(vacConfirmation.toString());
        System.out.println("\nRecord saved! ");
        Serializable.ExportDataToBinaryFile(Collections.singletonList(VacConfirmation), Constants.VACCINATION_LOG_FILE_NAME);
    }

    public static void ImportData(){
        String FileName = Constants.VACCINATION_LOG_FILE_NAME;
        File file = new File(FileName);

        try
        {
            file.createNewFile();
            List<List<Object>> listaAuxiliar = new ArrayList<>();
            FileInputStream fis = new FileInputStream(file);
            if (fis.available() > 0) {
                ObjectInputStream ois = new ObjectInputStream(fis);

                listaAuxiliar = (List<List<Object>>) ois.readObject();
                ois.close();
                fis.close();

                List<Object> a = listaAuxiliar.get(0);

                VacConfirmation = (List<VacAdministration>)(Object) a;
            }
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
        catch (ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
        }
    }

    public List<VacAdministration> getAllVaccinationlogs() {
        return VacConfirmation;
    }

    public List<VacAdministration> getVacLogsOfDay(Date dayOfLogs){
        List<VacAdministration> vacInfoOfDay = new ArrayList<>();

        for (VacAdministration vacInfo : VacConfirmation){

            String vacInfoDate = dateFormater.format(vacInfo.getAdministrationDateTime());

            if (vacInfoDate.equals(dateFormater.format(dayOfLogs))){
                vacInfoOfDay.add(vacInfo);
            }

        }
        return vacInfoOfDay;
    }
    public List<VacAdministration> getVacLogsOfDayByCenter(Date dayOfLogs,int centerID){
        List<VacAdministration> vacInfoOfDay = new ArrayList<>();

        for (VacAdministration vacInfo : VacConfirmation){

            String vacInfoDate = dateFormater.format(vacInfo.getAdministrationDateTime());
            if (vacInfoDate.equals(dateFormater.format(dayOfLogs))){
                if (vacInfo.getCenterID() == centerID){
                    vacInfoOfDay.add(vacInfo);
                }
            }

        }
        return vacInfoOfDay;
    }

    public List<String> getDatesWithRecordsByCenter(int centerID){
        List<String> listOfDatesToReturn = new ArrayList<String>();
        for (VacAdministration infoOfVaccination : VacConfirmation){
            String vacInfoDate = dateFormater.format(infoOfVaccination.getArrivalDateTime());
            if (infoOfVaccination.getCenterID() == centerID) {
                if (!(listOfDatesToReturn.contains(vacInfoDate))) {
                    listOfDatesToReturn.add(vacInfoDate);
                }
            }
        }
        return listOfDatesToReturn;
    }

    public int[] getDifferencesInDay(double timeInterval, Date analysisDay, int centerID) {

        // Start Variables
        int[] listOfDiferencesByTimeInterval = new int[(int) (720/timeInterval)];
        double calculateSeconds = timeInterval%1;
        int minutesToAdd = (int) (timeInterval-calculateSeconds);
        calculateSeconds *= 60;
        int secondsToAdd = (int) calculateSeconds;
        int counter = 0;
        List<VacAdministration> listOfVaccinationLogs = getVacLogsOfDayByCenter(analysisDay,centerID);

        // Initialize Calendars
        // timeCounterBefore is used to compare if the time is after it and the timeCounterAfter if the time happened before
        // so for it to be correct    timeCounterBefore <= time < timeCounterAfter
        Calendar timeCounterBefore = Calendar.getInstance();
        timeCounterBefore.setTime(analysisDay);
        timeCounterBefore.set(Calendar.HOUR_OF_DAY, 8);
        timeCounterBefore.set(Calendar.MINUTE, 0);
        timeCounterBefore.set(Calendar.SECOND, 0);

        Calendar timeCounterAfter;
        timeCounterAfter = Calendar.getInstance();
        timeCounterAfter.setTime(analysisDay);
        timeCounterAfter.set(Calendar.HOUR_OF_DAY, 8);
        timeCounterAfter.set(Calendar.MINUTE, 0);
        timeCounterAfter.set(Calendar.SECOND, 0);
        timeCounterAfter.add(Calendar.MINUTE,minutesToAdd);
        timeCounterAfter.add(Calendar.SECOND,secondsToAdd);

        Calendar timeToCompareArrival = Calendar.getInstance();
        Calendar timeToCompareLeft = Calendar.getInstance();

        while(timeCounterBefore.get(Calendar.HOUR_OF_DAY) < 20){
            int diference = this.differenceInTime(listOfVaccinationLogs,timeCounterBefore,timeCounterAfter,timeToCompareArrival,timeToCompareLeft);
            timeCounterAfter.add(Calendar.MINUTE,minutesToAdd);
            timeCounterAfter.add(Calendar.SECOND,secondsToAdd);
            timeCounterBefore.add(Calendar.MINUTE,minutesToAdd);
            timeCounterBefore.add(Calendar.SECOND,secondsToAdd);

            listOfDiferencesByTimeInterval[counter] = diference;
            counter ++;
        }
        return listOfDiferencesByTimeInterval;
    }
    public int differenceInTime(List<VacAdministration> listOfVaccinationLogs, Calendar timeCounterBefore,Calendar timeCounterAfter, Calendar timeToCompareArrival, Calendar timeToCompareLeave){

        // the difference between the people who arrived and those who left
        int peopleDifference = 0;

        for (VacAdministration vacInfo : listOfVaccinationLogs){
            timeToCompareArrival.setTime(vacInfo.getArrivalDateTime());
            timeToCompareLeave.setTime(vacInfo.getLeavingDateTime());
            //timeToCompareArrival.set(Calendar.HOUR_OF_DAY, 13);
            //timeToCompareLeave.set(Calendar.HOUR_OF_DAY, 13);
            //System.out.println(timeCounterBefore.getTime());
            //System.out.println(timeCounterAfter.getTime());

            //System.out.println("TimeArrival \n" + timeToCompareArrival.getTime());
            //System.out.println("TimeLeave \n" + timeToCompareLeave.getTime());

            if (timeToCompareArrival.after(timeCounterBefore) && timeToCompareArrival.before(timeCounterAfter)){
                peopleDifference ++;
                // prints to test / debug
                //System.out.println("++Pessoas Aqui " + timeToCompareArrival.after(timeCounterBefore) + " " + timeToCompareArrival.before(timeCounterAfter));
            }
            if(timeToCompareLeave.after(timeCounterBefore) && timeToCompareLeave.before(timeCounterAfter)){
                // prints to test / debug
                //System.out.println("--Pessoas Aqui");
                peopleDifference --;
            }
            //System.out.println("Difference: " + (peopleDifference) + "\n\n");
        }
        return  (peopleDifference);
    }
}
