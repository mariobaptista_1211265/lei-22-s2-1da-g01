package app.ui.console.stores;

import app.controller.App;
import app.domain.dto.EmployeeDto;
import app.domain.model.Company;
import app.domain.model.Employee;
import app.domain.model.NHSUser;
import app.domain.model.Serializable;
import app.domain.shared.Constants;
import pt.isep.lei.esoft.auth.AuthFacade;
import pt.isep.lei.esoft.auth.mappers.dto.UserRoleDTO;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * The type Employee store.
 */
public class EmployeeStore {

    private static List<Employee> ListEmployees = new ArrayList<>();


    /**
     * Get list of roles list.
     *
     * @return the list
     */
    public List<UserRoleDTO> getListOfRoles(){
        return App.getInstance().getCompany().getAuthFacade().getUserRoles();
    }


    /**
     * Instantiates a new Employee store.
     */
    public EmployeeStore() {

    }

    /**
     * Create employee.
     *
     * @param user the user
     * @return the employee
     */
    public static Employee Create(EmployeeDto user) {
        return new Employee(user);
    }

    /**
     * Save.
     *
     * @param user the user
     */
    public static void Save(Employee user, AuthFacade facade) {
        ListEmployees.add(user);
        System.out.println("\nCreated employee: " + user.getName() + "\nUser Email : " + user.getEmail());
        facade.addUserWithRole(user.getName(), user.getEmail(), user.getPassword(), user.getRole());
        Serializable.ExportDataToBinaryFile(Collections.singletonList(ListEmployees), Constants.EMPLOYEE_FILE_NAME);
    }

    public static void ImportData(AuthFacade facade){
        String FileName = Constants.EMPLOYEE_FILE_NAME;
        File file = new File(FileName);

        try
        {
            file.createNewFile();
            List<List<Object>> listaAuxiliar = new ArrayList<>();
            FileInputStream fis = new FileInputStream(file);
            if (fis.available() > 0) {
                ObjectInputStream ois = new ObjectInputStream(fis);

                listaAuxiliar = (List<List<Object>>) ois.readObject();
                ois.close();
                fis.close();

                List<Object> a = listaAuxiliar.get(0);

                ListEmployees = (List<Employee>)(Object) a;

                for (Employee employee : ListEmployees){
                    facade.addUserWithRole(employee.getName(), employee.getEmail(),employee.getPassword(), employee.getRole());
                }
            }
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
        catch (ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
        }
    }



    /**
     * Exists email boolean.
     *
     * @param email the email
     * @return the boolean
     */
    public static boolean existsEmail(String email) {
        if (!(email.contains("@") && email.contains("."))){
            return false;
        }
        for (Employee checkEmp : ListEmployees) {
            if (checkEmp.getEmail().equals(email)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Exists phone boolean.
     *
     * @param phone the phone
     * @return the boolean
     */
    public static boolean existsPhone(int phone) {

        if (phone>999999999 || phone < 100000000){
            return false;
        }

        for (Employee checkEmp : ListEmployees) {
            if (checkEmp.getPhone() == phone) {
                return false;
            }
        }
        return true;
    }

    /**
     * Exists citizen card number boolean.
     *
     * @param citizenCardNumber the citizen card number
     * @return the boolean
     */
    public static boolean existsCitizenCardNumber(int citizenCardNumber) {

        if (citizenCardNumber < 10000000 || citizenCardNumber > 99999999){
            return false;
        }

        for (Employee checkEmp : ListEmployees) {
            if (checkEmp.getCitizenCardNumber() == citizenCardNumber) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get list of roles that exist list.
     *
     * @return the list
     */
    public static List<String> getListOfRolesThatExist(){

        List<String> RolesList = new ArrayList<>();

        for (Employee outEmp : ListEmployees) {
            String role = outEmp.getRole();
            boolean val = false;
            for(String outRoles : RolesList){
                if(outRoles.equals(role)) val = true;
            }
            if(!val){
                RolesList.add(role);
            }
        }

        return RolesList;
    }

    /**
     * Get list of users with role list.
     *
     * @param Role the role
     * @return the list
     */
    public static List<Employee> getListOfUsersWithRole(String Role){
        List<Employee> usersWithRoles = new ArrayList<>();

        for (Employee outEmp : ListEmployees){
            if(outEmp.getRole().equals(Role)){
                usersWithRoles.add(outEmp);
            }
        }

        return usersWithRoles;
    }

    /**
     * Role exists boolean.
     *
     * @param id the id
     * @return the boolean
     */
    public static boolean roleExists(String id){
        return App.getInstance().getCompany().getAuthFacade().existsRole(id);
    }

    public static String getEmployeeRoleWithEmail(String email){
        for(Employee emp : ListEmployees){
            if(emp.getEmail().equals(email)){
                return emp.getRole();
            }
        }
        return null;
    }

}
