package app.ui.console.stores;

import app.domain.model.NHSUser;
import app.domain.dto.NHSUserDto;
import app.domain.model.Serializable;
import app.domain.shared.Constants;
import org.apache.commons.lang3.RandomStringUtils;
import pt.isep.lei.esoft.auth.AuthFacade;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.*;

/**
 * The type Nhs user store.
 *
 * @author Mário Baptista nº1211265
 */
public class NHSUserStore {


    private static List<NHSUser> NHSUsers = new ArrayList<>();

    /**
     * Instantiates a new Nhs user store.
     */
    public NHSUserStore() {

    }

    /**
     * Create nhs user.
     *
     * @param user the user
     * @return the nhs user
     */
    public static NHSUser Create(NHSUserDto user) {
        return new NHSUser(user);
    }

    /**
     * Save.
     *
     * @param nhsUser the nhs user
     */
    public static void Save(NHSUser nhsUser, AuthFacade facade) {
        NHSUsers.add(nhsUser);
        System.out.println("\nCreated user: " + nhsUser.getName());
//        System.out.println("SNS users in the system: ");
//        for (NHSUser nhsUser1 : NHSUsers) {
//            System.out.println(nhsUser1.toString());
//        }
        String password = generatePassword();
        facade.addUserWithRole(nhsUser.getName(), nhsUser.getEmail(), password, Constants.ROLE_NHS_USER);
        System.out.println(nhsUser.getEmail() + "  " + password);

        Serializable.ExportDataToBinaryFile(Collections.singletonList(NHSUsers), Constants.NHSUSERS_FILE_NAME);
    }

    /**
     * Check unique email boolean.
     *
     * @param email the email
     * @return the boolean
     */
    public static boolean checkUniqueEmail(String email) {
        for (NHSUser nhsUser : NHSUsers) {
            if (nhsUser.getEmail().equals(email)) {
                return false;
            }
        }
        return true;
    }

    private static String generatePassword(){
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@$&-_.?";
        String pwd = RandomStringUtils.random( 15, characters );
        return pwd;
    }

    public static void ImportData(AuthFacade facade){
        String FileName = Constants.NHSUSERS_FILE_NAME;
        File file = new File(FileName);

        try
        {
            file.createNewFile();
            List<List<Object>> listaAuxiliar = new ArrayList<>();
            FileInputStream fis = new FileInputStream(file);
            if (fis.available() > 0) {
                ObjectInputStream ois = new ObjectInputStream(fis);
                listaAuxiliar = (List<List<Object>>) ois.readObject();
                ois.close();
                fis.close();

                List<Object> auxiliar = listaAuxiliar.get(0);

                NHSUsers = (List<NHSUser>)(Object) auxiliar;


                for (NHSUser user : NHSUsers){
                    System.out.println(user.toString());
                    facade.addUserWithRole(user.getName(), user.getEmail(), user.getPassword(), Constants.ROLE_NHS_USER);
                }
            }
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
        catch (ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
        }
        //NHSUsers = (List<NHSUser>) Serializable.ImportDataFromBinaryFile(FileName, Collections.singletonList(NHSUsers));
    }

    /**
     * Check unique phone boolean.
     *
     * @param phone the phone
     * @return the boolean
     */
    public static boolean checkUniquePhone(int phone) {
        for (NHSUser nhsUser : NHSUsers) {
            if (nhsUser.getPhone() == phone) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check unique sns number boolean.
     *
     * @param snsNumber the sns number
     * @return the boolean
     */
    public static boolean checkUniqueSNSNumber(int snsNumber) {
        for (NHSUser nhsUser : NHSUsers) {
            if (nhsUser.getSNSNumber() == snsNumber) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check unique citizen card number boolean.
     *
     * @param citizenCardNumber the citizen card number
     * @return the boolean
     */
    public static boolean checkUniqueCitizenCardNumber(int citizenCardNumber) {
        for (NHSUser nhsUser : NHSUsers) {
            if (nhsUser.getCitizenCardNumber() == citizenCardNumber) {
                return false;
            }
        }
        return true;
    }

    /**
     * Gets nhs users.
     *
     * @return the nhs users
     */
    public static List<NHSUser> getNHSUsers() {
        return NHSUsers;
    }

    public static int getSns(String email) {
        for (NHSUser nhsUser : NHSUsers) {
            if (nhsUser.getEmail() == email) {
                return (nhsUser.getSNSNumber());
            }
        }
        return 0;
    }

    public NHSUser findByNumber(int userNumber) {
        for (NHSUser u : NHSUsers){
            if (u.getSNSNumber()==userNumber){
                return u;
            }
        }
        return null;
    }

    public static int getPhoneNumberBySNS(int snsNumber){
        for (NHSUser nhsUser : NHSUsers) {
            if (nhsUser.getSNSNumber() == snsNumber) {
                return (nhsUser.getPhone());
            }
        }
        return 0;
    }

    public static String getNameBySns(int snsNumber){
        for (NHSUser nhsUser : NHSUsers) {
            if (nhsUser.getSNSNumber() == snsNumber) {
                return (nhsUser.getName());
            }
        }
        return null;
    }
}
