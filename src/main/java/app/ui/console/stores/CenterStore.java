package app.ui.console.stores;

import app.domain.dto.CenterDto;

import app.domain.model.*;
import app.domain.shared.Constants;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The Store for all Centers.
 *
 * @author João Fortuna <1211261@isep.ipp.pt>
 */
public class CenterStore {
    /**
     * ArrayList with all Vaccination Centers owned by the company
     */
    private static List<VaccinationCenter> VacCenters = new ArrayList<>();

    /**
     * ArrayList with all Health Care Center owned by the company
     */
    private static List<HealthCareCenter> HealthCenters = new ArrayList<>();

    /**
     * Instantiates a new Center Store.
     */
    public CenterStore(){

    }

    /**
     * Creates a new vaccination center.
     *
     * @param vacCenter the center DTO
     * @return the new vaccination center
     */
    public static VaccinationCenter CreateVac(CenterDto vacCenter) {
        System.out.println("Creating Vaccination Center... ");
        return  new VaccinationCenter(vacCenter);
    }

    /**
     * Saves the vaccination center in the ArrayList.
     *
     * @param vacCenter the vaccination center
     */
    public static void SaveVac(VaccinationCenter vacCenter){

        VacCenters.add(vacCenter);
    //    System.out.println("Center created: ");
       for (VaccinationCenter vacCenter0 : VacCenters) { // ver se guarda
          //  System.out.println(vacCenter0.toString());
        }
        Serializable.ExportDataToBinaryFile(Collections.singletonList(VacCenters), Constants.VACCINATION_CENTER_FILE_NAME);
    }

    public static void ImportDataVacCenter(){
        String FileName = Constants.VACCINATION_CENTER_FILE_NAME;
        File file = new File(FileName);

        try
        {
            file.createNewFile();
            List<List<Object>> listaAuxiliar = new ArrayList<>();
            FileInputStream fis = new FileInputStream(file);
            if (fis.available() > 0) {
                ObjectInputStream ois = new ObjectInputStream(fis);

                listaAuxiliar = (List<List<Object>>) ois.readObject();
                ois.close();
                fis.close();

                List<Object> auxiliar = listaAuxiliar.get(0);

                VacCenters = (List<VaccinationCenter>)(Object) auxiliar;
            }
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
        catch (ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
        }
    }

    /**
     * Creates a new health care center.
     *
     * @param vacCenter the center DTO
     * @param ARS       the ars
     * @param AGES      the ages
     * @return the new health care center
     */
    public static HealthCareCenter CreateHealth(CenterDto vacCenter, String ARS, String AGES) {
        System.out.println("Creating Vaccination Center... ");
        return new HealthCareCenter(vacCenter,ARS,AGES);
    }

    /**
     * Saves the health care center in the ArrayList.
     *
     * @param healthCenter the health center
     */
    public static void SaveHealth(HealthCareCenter healthCenter){

        HealthCenters.add(healthCenter);
       // System.out.println("Center created: ");
        for (HealthCareCenter vacCenter1 : HealthCenters) { // ver se guarda
          //  System.out.println(vacCenter1.toString());
        }

        Serializable.ExportDataToBinaryFile(Collections.singletonList(HealthCenters), Constants.HEALTH_CARE_CENTER_FILE_NAME);
    }

    public static void ImportDataHealthCenters(){
        String FileName = Constants.HEALTH_CARE_CENTER_FILE_NAME;
        File file = new File(FileName);

        try
        {
            file.createNewFile();
            List<List<Object>> listaAuxiliar = new ArrayList<>();
            FileInputStream fis = new FileInputStream(file);
            if (fis.available() > 0) {
            ObjectInputStream ois = new ObjectInputStream(fis);

            listaAuxiliar = (List<List<Object>>) ois.readObject();
            ois.close();
            fis.close();

            List<Object> auxiliar = listaAuxiliar.get(0);

            HealthCenters = (List<HealthCareCenter>)(Object) auxiliar;
            }
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
        catch (ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
        }
    }

    /**
     * Checks if the phone number of the new vaccination center being created already exists in other center.
     *
     * @param phone the phone number
     * @return false if the number already exists.
     */
    public static boolean checkUniqueVacPhone(int phone) {
        for (VaccinationCenter vaccenter3 : VacCenters) {
            if (vaccenter3.getPhoneNumber() == phone) {
                System.out.println("Phone number already exists, please try again.");
                return false;
            }
        }
        return true;
    }

    /**
     * Check unique center id boolean.
     *
     * @param id the id
     * @return the boolean
     */
    public static boolean checkUniqueCenterID(int id) {
        for (VaccinationCenter vaccenter3 : VacCenters) {
            if (vaccenter3.getId() == id) {
                System.out.println("ID already exists, please try again.");
                return false;
            }
        }
        for (HealthCareCenter vaccenter5 : HealthCenters) {
            if (vaccenter5.getId() == id) {
                System.out.println("ID already exists, please try again.");
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if the email of the new vaccination center being created already exists in other center.
     *
     * @param email the email
     * @return false if the email already exists.
     */
    public static boolean checkUniqueVacEmail(String email) {
        for (VaccinationCenter vaccenter2 : VacCenters) {
            if (vaccenter2.getEmailAddress().equalsIgnoreCase(email)) {
                System.out.println("Email already exists, please try again.");
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if the phone number of the new health care center being created already exists in other center.
     *
     * @param phone the phone number
     * @return false if the number already exists.
     */
    public static boolean checkUniqueHealthPhone(int phone) {
        for (HealthCareCenter vaccenter4 : HealthCenters) {
            if (vaccenter4.getPhoneNumber() == phone) {
                System.out.println("Phone number already exists, please try again.");
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if the email of the new health care center being created already exists in other center.
     *
     * @param email the email
     * @return false if the email already exists.
     */
    public static boolean checkUniqueHealthEmail(String email) {
        for (HealthCareCenter vaccenter5 : HealthCenters) {
            if (vaccenter5.getEmailAddress().equalsIgnoreCase(email)) {
                System.out.println("Email already exists, please try again.");
                return false;
            }
        }
        return true;
    }

    /**
     * Show all centers.
     */
    public static void showAllCenters(){
        int i = 1;

        System.out.println("Existent Health Care Centers: \n");

        for (HealthCareCenter healthCenter : HealthCenters) {
            System.out.println(i + healthCenter.getName());
            i++;
        }

        System.out.println("Existent Mass Vacination Centers: \n");

        i = 1;

        for (VaccinationCenter vacCenter : VacCenters) {
            System.out.println(i + vacCenter.getName());
            i++;
        }

    }

    /**
     * Show health centers.
     */
    public static void showHealthCenters(){
        int i = 1;

        System.out.println("Existent Health Care Centers: \n");

        for (HealthCareCenter healthCenter : HealthCenters) {
            System.out.println(i + " - " + healthCenter.getName());
            i++;
        }

    }

    /**
     * Show mass vaccination centers.
     */
    public static void showMassVaccinationCenters(){
        int i = 1;
        System.out.println("Existent Mass Vacination Centers: \n");


        for (VaccinationCenter vacCenter : VacCenters) {
            System.out.println(i + " - " +  vacCenter.getName());
            i++;
        }

    }

    /**
     * Get health id int.
     *
     * @param option the option
     * @return the int
     */
    public static int getHealthID(int option){
        return HealthCenters.get(option-1).getId();
    }

    /**
     * Get vac id int.
     *
     * @param option the option
     * @return the int
     */
    public static int getVacID(int option){ return VacCenters.get(option-1).getId();}

    /**
     * Get vac center vaccination center.
     *
     * @param id the id
     * @return the vaccination center
     */
    public static VaccinationCenter getVacCenter(int id){
        for (VaccinationCenter vacCenter : VacCenters) {
            if (vacCenter.getId() == id) {
                return vacCenter;
            }
        }
        return null;
    }

    /**
     * Get health center health care center.
     *
     * @param id the id
     * @return the health care center
     */
    public static HealthCareCenter getHealthCenter(int id){
        for (HealthCareCenter healthCenter : HealthCenters) {
            if (healthCenter.getId() == id) {
                return healthCenter;
            }
        }
        return null;
    }

    public static String[] getHealthCenterNames(){
        String[] names = new String[HealthCenters.size()];
        int i = 0;
        for (HealthCareCenter healthCenter : HealthCenters) {
            names[i] = healthCenter.getName();
            i++;
        }
        return names;
    }

    public static String[] getVacCenterNames(){
        String[] names = new String[VacCenters.size()];
        int i = 0;
        for (VaccinationCenter vacCenter : VacCenters) {
            names[i] = vacCenter.getName();
            i++;
        }
        return names;
    }


    public static int getHealthCenterIDbyName(String name){
        for (HealthCareCenter healthCenter : HealthCenters) {
            if (healthCenter.getName().equalsIgnoreCase(name)) {
                return healthCenter.getId();
            }
        }
        return -1;
    }

    public static int getVacCenterIDbyName(String name){
        for (VaccinationCenter vacCenter : VacCenters) {
            if (vacCenter.getName().equalsIgnoreCase(name)) {
                return vacCenter.getId();
            }
        }
        return -1;
    }

    public List<Center> getAll(){
        List<Center> result = new ArrayList<>(CenterStore.VacCenters);
        result.addAll(CenterStore.HealthCenters);
        return result;
    }

}
