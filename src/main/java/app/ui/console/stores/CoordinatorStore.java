package app.ui.console.stores;

import app.controller.App;
import app.domain.dto.CoordinatorDto;
import app.domain.model.Coordinator;
import app.domain.model.Serializable;
import app.domain.shared.Constants;
import org.apache.commons.lang3.RandomStringUtils;
import pt.isep.lei.esoft.auth.AuthFacade;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CoordinatorStore {
    private static List<Coordinator> ListCoordinators = new ArrayList<>();

    public CoordinatorStore() {

    }

    public static int getCenterIDByEmail(String email){
        for (Coordinator user : ListCoordinators ){
            if (user.getEmail().equals(email)){
                return user.getCenterID();
            }
        }
        return 0;
    }

    public static void ImportData(AuthFacade facade){
        String FileName = Constants.COORDINATORS_FILE_NAME;
        File file = new File(FileName);

        try
        {
            file.createNewFile();
            List<List<Object>> listaAuxiliar = new ArrayList<>();
            FileInputStream fis = new FileInputStream(file);
            if (fis.available() > 0) {
                ObjectInputStream ois = new ObjectInputStream(fis);

                listaAuxiliar = (List<List<Object>>) ois.readObject();
                ois.close();
                fis.close();

                List<Object> auxiliar = listaAuxiliar.get(0);

                ListCoordinators = (List<Coordinator>)(Object) auxiliar;

                for (Coordinator coordinatorToLoad : ListCoordinators){
                    facade.addUserWithRole(coordinatorToLoad.getName(),coordinatorToLoad.getEmail(),coordinatorToLoad.getPassword(), Constants.ROLE_COORDINATOR);
                }

            }
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
        catch (ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
        }
    }

    public void saveWithCenterName(String email, String vacCenterName, String name){
        int centerID = CenterStore.getVacCenterIDbyName(vacCenterName);
        String pwd = createPassword();
        ListCoordinators.add(new Coordinator(createCoordinatorDTO(email,centerID, name, pwd)));
        AuthFacade facade = App.getInstance().getCompany().getAuthFacade();
        facade.addUserWithRole(name,email,pwd,Constants.ROLE_COORDINATOR);
        Serializable.ExportDataToBinaryFile(Collections.singletonList(ListCoordinators), Constants.COORDINATORS_FILE_NAME);
    }

    public void saveWithId(String email, int vacCenterId, String name){
        int centerID = vacCenterId;
        String pwd = createPassword();
        ListCoordinators.add(new Coordinator(createCoordinatorDTO(email,centerID, name, pwd)));
        AuthFacade facade = App.getInstance().getCompany().getAuthFacade();
        facade.addUserWithRole(name,email,pwd,Constants.ROLE_COORDINATOR);
        Serializable.ExportDataToBinaryFile(Collections.singletonList(ListCoordinators), Constants.COORDINATORS_FILE_NAME);
    }

    public static CoordinatorDto createCoordinatorDTO(String email, int centerID, String name, String password){
        return new CoordinatorDto(centerID,email, name, password);
    }

    private String createPassword(){
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?";
        String pwd = RandomStringUtils.random( 15, characters );
        System.out.println("User Password : " + pwd);
        return pwd;
    }

}
