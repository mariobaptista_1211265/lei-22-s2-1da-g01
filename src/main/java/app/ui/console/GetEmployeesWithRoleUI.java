package app.ui.console;


import app.controller.App;
import app.controller.US11Controller;
import pt.isep.lei.esoft.auth.mappers.dto.UserDTO;
import pt.isep.lei.esoft.auth.mappers.dto.UserRoleDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * The type Get employees with role ui.
 *
 * @author Manuel Pimentel <1210670@isep.ipp.pt>
 */
public class GetEmployeesWithRoleUI implements Runnable {

    /**
     * The Ctrl.
     */
    US11Controller ctrl = new US11Controller(App.getInstance());


    @Override
    public void run() {
        Scanner scan = new Scanner(System.in);

        List<String> RoleList = ctrl.getListOfRolesThatExist();

        if (RoleList.size() >= 1) {
            System.out.println("List of Roles That Have Employees: \n");
            int numberList = 1;
            for (String role : RoleList) {
                System.out.println(numberList + " - " + role);
                numberList++;
            }
            int inpRole = scan.nextInt();
            while (inpRole < 1 || inpRole > RoleList.size()) {
                System.out.println("Role doesnt exist. Please select another one: ");
                inpRole = scan.nextInt();
            }

            String getRole = "";
            numberList = 1;
            for (String role : RoleList) {
                if (numberList == inpRole) {
                    getRole = role;
                }
                numberList++;
            }
            List<UserDTO> usersWithRole = ctrl.getListOfUsersWithRole(getRole);
            System.out.println("\nUsers with " + getRole + " Role:\n");
            int index = 1;
            for (UserDTO outEmp : usersWithRole) {

                List<String> list = new ArrayList<>();

                for (UserRoleDTO r : outEmp.getRoles()) {
                    list.add(r.getDescription());
                }


                System.out.printf("%d - %s | %s -> %s%n", index, outEmp.getId(), outEmp.getName(), list);
                index++;
            }
        } else {
            System.out.println("There is no Employees Registered!");
        }
    }
}
