package app.ui.console;

import app.controller.App;
import app.controller.US16Controller;
import app.domain.shared.Constants;
import app.ui.console.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CenterPerformanceUI implements Runnable{
    private final US16Controller ctrl;
    private int CenterUI;

    /**
     * Instantiates a new Read user from file ui.
     */
    public CenterPerformanceUI()
    {
        ctrl = new US16Controller(App.getInstance());
    }

    /* Test Coordinator Info
        otario@email.pt
        =+/eN(F@k2xs.VK
     */

    public void run() {
        // Check if coordinator is associated with a center
        // break application if it isn't
        if (!ctrl.checkCenterExists()) {
            System.out.println("Center coordinator isn't associated to any center");
            return;
        }

        // get all dates with records
        List<String> possibleDays = ctrl.getDatesWithInfo();
        if (!(possibleDays.isEmpty())) {
            Scanner scan = new Scanner(System.in);
            double timeInterval = 0;
            do {
                timeInterval = Utils.readDoubleFromConsole("Time interval in min - ");
            } while (720 % timeInterval != 0);

            String selectionFromPrompt = "";
            System.out.println("Dates with records: ");
            for (String possibleDate : possibleDays) {
                System.out.println(possibleDate);
            }

            selectionFromPrompt = Utils.readLineFromConsole("Write Date (dd-mm-yyyy)").trim();
            while (!ctrl.validDate(possibleDays, selectionFromPrompt)) {
                selectionFromPrompt = Utils.readLineFromConsole("Write Date (dd-mm-yy)").trim();
            }
            String selectedDate = selectionFromPrompt;

            int algorithmSelection = ctrl.getAlgorithmFromProps();
            outPutInfo(selectedDate,timeInterval,algorithmSelection);

            boolean flag = Utils.confirm("Is everything correct? (s/n)");
            List<String> list = new ArrayList<String>();
            list.add("Date");
            list.add("Time Interval");
            list.add("Algorithm");
            while (!flag){
                int changeFieldSelection = Utils.showAndSelectIndex(list, "Please select the field you want to change");
                switch (changeFieldSelection){
                    case 0:{
                        System.out.println("Dates with records");
                        for (String possibleDate : possibleDays) {
                            System.out.println(possibleDate);
                        }
                        selectionFromPrompt = Utils.readLineFromConsole("Write Date (dd-mm-yyyy)").trim();
                        while (!ctrl.validDate(possibleDays, selectionFromPrompt)) {
                            selectionFromPrompt = Utils.readLineFromConsole("Write Date (dd-mm-yy)").trim();
                        }
                        selectedDate = selectionFromPrompt;
                    } break;
                    case 1:{
                        do {
                            timeInterval = Utils.readDoubleFromConsole("Time interval in min - ");
                        } while (720 % timeInterval != 0);
                    } break;
                    case 2:{
                        do {
                            System.out.println("Select: ");
                            System.out.println("1: brute-force");
                            System.out.println("2: benchmark");
                            algorithmSelection = (Utils.readIntegerFromConsole("Selection: ") - 1);
                        } while (algorithmSelection < 0 || algorithmSelection > 1);

                    } break;
                }
                outPutInfo(selectedDate,timeInterval,algorithmSelection);
                flag = Utils.confirm("Is everything correct? (s/n)");
            }


            try {
                ctrl.setProperty(Constants.PARAMS_CENTERPERFORMANCE_ALGORITHM,algorithmSelection);

                int[] differences = ctrl.getListOfDifferences(timeInterval,new SimpleDateFormat("dd-MM-yyyy").parse(selectedDate));
                int[] greatestContiguousSum = ctrl.getMaxContiguousSum(differences);
                int sum = 0;
                for (int addToSum : greatestContiguousSum){
                    sum += addToSum;
                }

                System.out.println("Original list: ");
                System.out.println(Arrays.toString(differences));
                if (greatestContiguousSum.length == 0){
                    System.out.println("There is no sublist with sum");
                    return;
                }
                System.out.println("Sublist with max contiguous sum:");
                System.out.println(Arrays.toString(greatestContiguousSum));
                System.out.print("Sum of List: ");
                int sumSubList = 0;
                for (int addToSum : greatestContiguousSum){
                    sumSubList += addToSum;
                }
                System.out.println(sumSubList);
                System.out.print("Time Interval: ");

                int firstOcurrenceOfSublist = ctrl.getStartOfSublist(greatestContiguousSum,differences);
                Calendar timeToOutput = Calendar.getInstance();
                timeToOutput.set(Calendar.HOUR_OF_DAY, 8);
                timeToOutput.set(Calendar.MINUTE, 0);
                timeToOutput.set(Calendar.SECOND, 0);

                double calculateSeconds = timeInterval%1;
                int minutesToAdd = (int) (timeInterval-calculateSeconds);
                calculateSeconds *= 60;
                int secondsToAdd = (int) calculateSeconds;

                // Start Of Sublist
                timeToOutput.add(Calendar.MINUTE, minutesToAdd * firstOcurrenceOfSublist);
                timeToOutput.add(Calendar.SECOND, secondsToAdd * firstOcurrenceOfSublist);
                System.out.print(timeToOutput.get(Calendar.HOUR_OF_DAY) + ":" + timeToOutput.get(Calendar.MINUTE) + ":" + timeToOutput.get(Calendar.SECOND) + " to " );
                timeToOutput.add(Calendar.MINUTE, minutesToAdd * greatestContiguousSum.length);
                timeToOutput.add(Calendar.SECOND, secondsToAdd * greatestContiguousSum.length);
                System.out.println(timeToOutput.get(Calendar.HOUR_OF_DAY) + ":" + timeToOutput.get(Calendar.MINUTE) + ":" + timeToOutput.get(Calendar.SECOND));

            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        } else {
            System.out.println("Center associated with the coordinator has no records.");
        }
    }



    private void outPutInfo(String date, double timeInterval, int algorithm){
        System.out.println("Date - " + date);
        System.out.printf("Time interval - %.2f\n",timeInterval);
        System.out.print("Algorithm - ");
        if (algorithm == 1){
            System.out.print("benchmark");
        } else{
            System.out.print("bruteforce");
        }
    }

}
