package app.ui.console;

import app.controller.App;
import app.controller.CreateVacAdmController;
import app.controller.CreateVaccineController;
import app.domain.model.AdministrationProcess;
import app.domain.model.VacType;
import app.domain.model.Vaccine;
import app.ui.console.stores.VaccineAdmStore;
import app.ui.console.stores.VaccineStore;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Pedro Sá nº1211269
 *
 * The type Create vaccine ui.
 */
public class CreateVaccineUI implements Runnable {

    private CreateVaccineController ctrl;
    private CreateVacAdmController ctrl2;

    public CreateVaccineUI(){
        ctrl=new CreateVaccineController(App.getInstance());
        ctrl2=new CreateVacAdmController(App.getInstance());
    }


    @Override
    public void run() {

        String brand = null;
        String vactype = null;
        int ageI = 0;
        int ageF = 0;
        double dosage = 0;
        double numberOfDoses=0;
        int timeInterval = 0;

        int op = 0;

        do{
            System.out.printf("What operation you want? \n1 - Create a new vaccine brand. \n2 - Create/Change the Vaccination Process.");
            op = Utils.readIntegerFromConsole("Option - ");
        }while (op!=1 && op!=2);
        if (op==2)
        {
            if (VaccineStore.vaccinescount()>=1) {
                System.out.println("Creating/editing Vaccine Administration Process...");
                System.out.println("--------------------------------------------------");
                do {
                    do {
                        brand = Utils.readLineFromConsole("Brand - ");
                    } while (ctrl.CheckDups2(brand) == true);
                    VacType.showVacTypes();
                    do {
                        vactype = Utils.readLineFromConsole("Vaccine Type - ");
                    } while (VacType.Checkvac(vactype) == true);
                } while (VaccineAdmStore.CheckDupsAdm(brand, vactype) == false);
                int i = 0;
                do {
                    if (i != 0)
                        System.out.println("The first age shall me lower.");
                    i++;
                    ageI = Utils.readIntegerFromConsole("Age, initial number - ");
                    ageF = Utils.readIntegerFromConsole("Age, final number - ");
                } while (ageF < ageI || ageF > 120 || ageI > 120 || ageI < 0 || ageF < 0);
                dosage = Utils.readDoubleFromConsole("Dosage - ");
                numberOfDoses = Utils.readDoubleFromConsole("Number of doses - ");
                timeInterval = Utils.readIntegerFromConsole("Time Interval (in days) - ");
                AdministrationProcess adm = ctrl2.createProcess(brand, vactype, ageI, ageF, dosage, numberOfDoses, timeInterval);
                ConfirmData(adm);
                VaccineAdmStore.saveProcess(brand, vactype, ageI, ageF, dosage, numberOfDoses, timeInterval);
            }
            else {
                System.out.println("Its necessary at least one vaccine to create a process.");
            }
        }
        if(op==1)
        {
            System.out.println("Creating a new Vaccine brand...");
            System.out.println("-------------------------------");
            do {
                brand = Utils.readLineFromConsole("Brand - ");
            }while (ctrl.CheckDups(brand)==false);
            Vaccine vac = ctrl.createVaccine(brand);
            boolean confirm = Utils.confirm("Is everything correct? (s/n)");
            if (confirm)
            {
                ctrl.saveVaccine(vac);
            }
        }
    }
    private void ConfirmData(AdministrationProcess Vc1){
        List<String> list = new ArrayList<>();
        list.add("Brand");
        list.add("Vaccine Type");
        list.add("Initial Age");
        list.add("Final Age");
        list.add("Dosage");
        list.add("Number of doses");
        list.add("Time Interval");

        boolean confirm;
        do {
            System.out.println("Please confirm the Vaccine's informations: \n" + Vc1.toString());
            confirm = Utils.confirm("Is everything correct? (s/n)");
            if (!confirm){
                int i = Utils.showAndSelectIndex(list,"Please select the field you want to change- ");
                switch (i){
                    case 0:
                        String brand;
                        do {
                            brand = Utils.readLineFromConsole("Insert the brand: ");
                        }while (ctrl.CheckDups(brand)==true);
                        Vc1.setBrand(brand);
                        break;
                    case 1:
                        String vactype = Utils.readLineFromConsole("Insert the vaccine type: ");
                        Vc1.setvactype(vactype);
                    case 2:
                        int ageI=0, ageF=0;
                        do {
                            ageI = Utils.readIntegerFromConsole("Insert the initial age: ");
                            Vc1.setAgeI(ageI);
                            ageF = Utils.readIntegerFromConsole("Insert the final age");
                            Vc1.setAgeF(ageF);
                        }while (ageF>=ageI && ageF<=120 && ageI<=120);
                        break;
                    case 3:
                        double dosage = Utils.readDoubleFromConsole("Insert the dosage: ");
                        Vc1.setDosage(dosage);
                        break;
                    case 4:
                        double numberOfDoses = Utils.readDoubleFromConsole("Insert the number of doses: ");
                        Vc1.setNumberOfDoses(numberOfDoses);
                        break;
                    case 5:
                        int timeInterval = Utils.readIntegerFromConsole("Insert the time interval: ");
                        Vc1.setTimeInterval(timeInterval);
                        break;
                }
            }
        }while (!confirm);
    }
}
