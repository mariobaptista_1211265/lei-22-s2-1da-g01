package app.controller;


import app.domain.model.Company;
import app.domain.model.UserArrival;
import app.domain.model.VaccineSchedule;
import app.domain.model.WaitingRoom;
import app.ui.console.stores.VaccineScheduleStore;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * The US04 controller.
 *
 * @author João Fortuna <1211261@isep.ipp.pt>
 */
public class US04Controller {

    private Company company;
    private VaccineScheduleStore scheduleStore;
    private WaitingRoom waitingRoom;


    public US04Controller(App app){
        company=app.getCompany();
        scheduleStore=company.getScheduleStore();
        waitingRoom=company.getWaitingRoom();
    }


    /**
     * Does user schedule exist in center boolean.
     *
     * @param number the number
     * @param id     the id
     * @return the boolean
     */
    public boolean doesUserScheduleExistInCenter(int number,int id){ return scheduleStore.doesUserScheduleExistInCenter(number,id); }


    /**
     * Show a scheduled vaccine.
     *
     * @param number   the nhs number
     * @param centerID the center id
     */
    public void showAScheduledVaccine(int number, int centerID){

        VaccineSchedule schedule = scheduleStore.getAScheduledVaccine(number, centerID);

        System.out.println(schedule.toString());

    }

    /**
     * Check if in time to arrive boolean.
     *
     * @param number   the nhs number
     * @param centerID the center id
     * @return the boolean
     */
    public boolean checkIfInTimeToArrive(int number, int centerID){

        SimpleDateFormat day = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat hour = new SimpleDateFormat("HH");
        SimpleDateFormat minutes = new SimpleDateFormat("mm");

        Date now = new Date();

        VaccineSchedule schedule = scheduleStore.getAScheduledVaccine(number, centerID);

            if (day.format(now).equals(day.format(schedule.getDate()))   &&      Integer.parseInt(hour.format(now)) >=  Integer.parseInt(hour.format(schedule.getDate()))-1    &&   Integer.parseInt(hour.format(now)) <=  Integer.parseInt(hour.format(schedule.getDate()))+2   && Integer.parseInt(minutes.format(now)) >=  Integer.parseInt(minutes.format(schedule.getDate()))   ) {
                return true;
            }


        return false;

    }

    /**
     * Create user arrival.
     *
     * @param number the nhs number
     * @param id     the center id
     */
    public void createUserArrival(int number, int id){
        UserArrival arrival = new UserArrival(id,number);
        waitingRoom.addUserArrival(arrival);
    }


    /**
     * Check duplicated entry boolean.
     *
     * @param number the nhs number
     * @param id     the center id
     * @return the boolean
     */
    public boolean checkDuplicatedEntry(int number, int id){ return waitingRoom.checkDuplicatedEntry(id,number);}

}
