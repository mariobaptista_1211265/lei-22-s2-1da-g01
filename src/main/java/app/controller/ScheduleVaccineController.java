package app.controller;

import app.domain.model.*;
import app.ui.console.stores.CenterStore;
import app.ui.console.stores.NHSUserStore;
import app.ui.console.stores.VaccineAdmStore;
import app.ui.console.stores.VaccineScheduleStore;
import app.ui.console.utils.Utils;
import pt.isep.lei.esoft.auth.AuthFacade;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Mário Baptista <1211265@isep.ipp.pt>
 * @author Pedro Sá <1211269@isep.ipp.pt>
 */

/**
 * The type Us 01/02 controller.
 */
public class ScheduleVaccineController {


    /**
     * Schedule vaccine.
     *
     * @param SNSNumber the sns number
     * @param centerId  the center id
     * @param date      the date
     * @param time      the time
     * @param vacType   the vac type
     */
    private Company company;
    private AuthFacade authFacade;

    /**
     * Instantiates a new Schedule vaccine controller.
     *
     * @param app the app
     */
    public ScheduleVaccineController(App app){
        company=app.getCompany();
        authFacade= company.getAuthFacade();
    }


    /**
     * Schedule vaccine.
     *
     * @param SNSNumber the sns number
     * @param centerId  the center id
     * @param date      the date
     * @param time      the time
     * @param vacType   the vac type
     * @param function  the function
     */
    public void scheduleVaccine(int SNSNumber, int centerId, Date date, String time, String vacType,String function){
        VaccineSchedule vac = new VaccineSchedule(SNSNumber, centerId, date, time, vacType);

        boolean userHasDuplicate = checkDuplicateSchedules(SNSNumber, vacType);

        boolean userIsWithinAgeAndTime = checkUserIsWithinAgeAndTime(vac);

        if (userHasDuplicate && userIsWithinAgeAndTime)
        {
            VaccineScheduleStore.addScheduledVaccine(vac);
            boolean answer = Utils.confirm("Do you wish to recieve a notification with the vaccine information? (s/n)");
            if (answer) {
                sendNotification(vac);
            }
        }
    }

    private void sendNotification(VaccineSchedule vac) {
        File smsFile = new File("sms.txt");
        Center center;
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String date = formatter.format(vac.getDate());
        try {
            center = getVacCenter(vac.getCenter());
        } catch (Exception ignore) {
            center = getHealthCenter(vac.getCenter());
        }
        if (center == null) {
            center = getHealthCenter(vac.getCenter());
        }
        String smsString = "Your vaccine is scheduled for " + date + " at " + vac.getTime() + " in the " + center.getName() + " center.";
        try {
            if (smsFile.createNewFile()) {
                int a = 0;
            } else {
                int a = 1;
            }
            FileWriter myWriter = new FileWriter("sms.txt", true);
            myWriter.write("SMS message to: " + vac.getUser().getPhone() + "\n");
            myWriter.write(smsString);
            myWriter.write("\n--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    /**
     * Choose center for vaccination int.
     *
     * @return the int
     */
    public int chooseCenterForVaccination() {
        int id;
        int centerType = 0;

        do{
            System.out.println("Choose the type of center for the vaccination... \n");
            System.out.println("1 - Mass Vaccination Centers");
            System.out.println("2 - Health Care Centers \n");
            centerType = Utils.readIntegerFromConsole("Center Type- ");
        }while(centerType!=1 && centerType !=2);



        System.out.println("What Center do you want to schedule the vaccine for? \n");

        if(centerType==1){
            ChooseCenterController.showVacCenters();
        }else{
            ChooseCenterController.showHealthCenters();
        }

        int op = Utils.readIntegerFromConsole("Option - ");

        if(centerType == 1){
            id = ChooseCenterController.getVacCenterID(op);
        }else{
            id = ChooseCenterController.getHealthCenterID(op);
        }
        return id;
    }


    /**
     * Choose vac type string.
     *
     * @param centerId the center id
     * @return the string
     */
    public String chooseVacType(int centerId) {
        if (isHealthCenter(centerId)){
            System.out.println("Choose the type of vaccine you want to schedule: \n");
            VacType.showVacTypes();
            int op = Utils.readIntegerFromConsole("");
            return VacType.getType(op);
        } else {
            VaccinationCenter vac = getVacCenter(centerId);
            return vac.getVACVaccinetype();
        }
    }

    /**
     * Is health center boolean.
     *
     * @param id the id
     * @return the boolean
     */
    public boolean isHealthCenter(int id){
        for (int i = 1; i <= id; i++) {
            if(CenterStore.getHealthID(i) == id){
                return true;
            }
        }
        return false;
    }

    /**
     * Get vac center vaccination center.
     *
     * @param id the id
     * @return the vaccination center
     */
    /**
     * Get vac center vaccination center.
     *
     * @param id the id
     * @return the vaccination center
     */
    public VaccinationCenter getVacCenter(int id){
        try {
            for (int i = 1; i <= id; i++) {
                if(CenterStore.getVacID(i) == id){
                    return CenterStore.getVacCenter(id);
                }
            }
        } catch (Exception ignore) {

        }
        return null;
    }

    /**
     * Gets health center.
     *
     * @param id the id
     * @return the vac center
     */
    public HealthCareCenter getHealthCenter(int id){
        try {
            for (int i = 1; i <= id; i++) {
                if(CenterStore.getHealthID(i) == id){
                    return CenterStore.getHealthCenter(id);
                }
            }
        } catch (Exception ignore) {

        }
        System.out.println("Health Center not found");
        return null;
    }

    private boolean checkUserIsWithinAgeAndTime(VaccineSchedule vac){

        boolean isWithinAge = checkUserIsWithinAge(vac);

        boolean isWithinTime = checkUserIsWithinTime(vac);

        if(isWithinAge && isWithinTime){
            return true;
        }
        return false;
    }

    /**
     * Gets sns.
     *
     * @return the sns
     */
    public int getSNS() {
        return (NHSUserStore.getSns(authFacade.getCurrentUserSession().getUserId().getEmail() ));
    };
    private boolean checkUserIsWithinAge(VaccineSchedule vac){


        List<AdministrationProcess> adminProcesses = VaccineAdmStore.getProcesses();

        int ageI = -1;
        int ageF = -1;

        for(AdministrationProcess adminProcess : adminProcesses){
            if(adminProcess.getvactype().equals(vac.getVacType())){
                ageI = adminProcess.getAgeI();
                ageF = adminProcess.getAgeF();
            }
        }

        if(ageI == -1 || ageF == -1){
            return false;
        }

        Date date = vac.getDate();

        int ageInDays = (int)( (date.getTime() - vac.getUser().getBirthDate().getTime()) / (1000 * 60 * 60 * 24) );

        int age = ageInDays / 365;

        if(age >= ageI && age <= ageF){
            return true;
        }
        System.out.println("The user is not within the age range for this vaccine");
        return false;
    }

    /**
     * Check duplicate schedules boolean.
     *
     * @param SnsNumber the sns number
     * @param Vactype   the vactype
     * @return the boolean
     */
//US 01 Acceptance Criteria
    public boolean checkDuplicateSchedules(int SnsNumber, String Vactype)
    {
        return VaccineScheduleStore.checkDuplicateSchedules(SnsNumber, Vactype);
    }

    /**
     * Validatedate boolean.
     *
     * @param data the data
     * @return the boolean
     */
    public static boolean validatedate(Date data)
    {
        Date hoje=new Date();
        if (data.after(hoje)) {}
        else {
            System.out.println("Invalid Date, try again");
            return false;
        }
        return true;
    }

    /**
     * Check unique sns number boolean.
     *
     * @param snsNumber the sns number
     * @return the boolean
     */
    public boolean checkUniqueSNSNumber(int snsNumber) {
        return (NHSUserStore.checkUniqueSNSNumber(snsNumber));
    }

    /**
     * Verifyhours boolean.
     *
     * @param time the time
     * @return the boolean
     */
    public static boolean verifyhours(String time){
        int i;
        int length=time.length();
        String valores = "";
        for (i=0; i<length;i++)
        {
            if (time.charAt(i)==':'){
                if (Integer.parseInt(valores)>24)
                {
                    System.out.println("Invalid time, try again");
                    return false;
                }
            }
            valores=valores+time.charAt(i);
        }
        valores = "";
        for (i=3;i<length;i++)
        {
            valores=valores+time.charAt(i);
            if (i==length-1){
                if (Integer.parseInt(valores)>60)
                {
                    System.out.println("Invalid time, try again");
                    return false;
                }
            }
        }
        return true;
    }
    private boolean checkUserIsWithinTime(VaccineSchedule vac){

        for (int i = 0; i < VaccineScheduleStore.getScheduledVaccines().size(); i++) {
            if(VaccineScheduleStore.getScheduledVaccines().get(i).getUser().getSNSNumber() == vac.getUser().getSNSNumber()){
                if(VaccineScheduleStore.getScheduledVaccines().get(i).getVacType().equals(vac.getVacType())){
                    Date dateOfLastVaccine = VaccineScheduleStore.getScheduledVaccines().get(i).getDate();
                    Date dateOfNewVaccine = vac.getDate();

                    long timeOfNewVaccine = dateOfNewVaccine.getTime();

                    long timeOfLastVaccine = dateOfLastVaccine.getTime();

                    long diff = (long)(timeOfNewVaccine - timeOfLastVaccine) / (1000 * 60 * 60 * 24);

                    double minimumTimeBetweenVaccines = 0;

                    for (int j = 0; j < VaccineAdmStore.getProcesses().size(); j++) {
                        if(VaccineAdmStore.getProcesses().get(j).getvactype().equals(vac.getVacType())){
                            minimumTimeBetweenVaccines = VaccineAdmStore.getProcesses().get(j).getTimeInterval();
                        }
                    }

                    //double minimumTimeBetweenVaccines = VaccineAdmStore.getProcesses().get(VaccineAdmStore.getProcesses().indexOf(vac.getVacType())).getTimeInterval();

                    if(diff < minimumTimeBetweenVaccines){
                        return false;
                    }

                }
            }
        }
        return true;
    }

    /**
     * Showschedules.
     */
    public void showschedules() {
        VaccineScheduleStore.showschedules();
    }
}
