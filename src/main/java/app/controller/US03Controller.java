package app.controller;

import app.domain.dto.NHSUserDto;
import app.domain.model.Company;
import app.domain.model.NHSUser;
import app.domain.shared.Constants;
import app.ui.console.stores.NHSUserStore;
import org.apache.commons.lang3.RandomStringUtils;
import pt.isep.lei.esoft.auth.AuthFacade;
import app.domain.model.Serializable;

import java.util.*;

/**
 * The type Us 03 controller.
 *
 * @author Mário Baptista nº1211265
 */
public class US03Controller {

    private NHSUser nhsUser;

    private Company company;

    private NHSUserStore userStore;

    private AuthFacade facade;


    public US03Controller(App app){
        company=app.getCompany();
        userStore= company.getNHSUserStore();
        facade= company.getAuthFacade();
    }

    /**
     * Create nhs user nhs user.
     *
     * @param name              the name
     * @param address           the address
     * @param sex               the sex
     * @param phone             the phone
     * @param email             the email
     * @param birthDate         the birth date
     * @param SNSNumber         the sns number
     * @param citizenCardNumber the citizen card number
     * @return the nhs user
     */
    public NHSUser createNHSUser(String name, String address, String sex, int phone, String email, Date birthDate, int SNSNumber, int citizenCardNumber) {

        NHSUserDto userDto = new NHSUserDto(name, address, sex, phone, email, birthDate, SNSNumber, citizenCardNumber);

        nhsUser = userStore.Create(userDto);

        return nhsUser;
    }
    public NHSUser createNHSUser(String name, String address, String sex, int phone, String email, Date birthDate, int SNSNumber, int citizenCardNumber, String password) {

        NHSUserDto userDto = new NHSUserDto(name, address, sex, phone, email, birthDate, SNSNumber, citizenCardNumber, password);

        nhsUser = userStore.Create(userDto);

        return nhsUser;
    }


    /**
     * Save nhs user.
     */
    public void saveNHSUser(){
        company.getNHSUserStore().Save(nhsUser, facade);
    }




    /**
     * Check unique nhs user phone boolean.
     *
     * @param phone the phone
     * @return the boolean
     */
    public boolean checkUniqueNHSUserPhone(int phone){
        return userStore.checkUniquePhone(phone);
    }

    /**
     * Check unique nhs user email boolean.
     *
     * @param email the email
     * @return the boolean
     */
    public boolean checkUniqueNHSUserEmail(String email) {
        return userStore.checkUniqueEmail(email);
    }

    /**
     * Check unique nhs user sns number boolean.
     *
     * @param SNSNumber the sns number
     * @return the boolean
     */
    public boolean checkUniqueNHSUserSNSNumber(int SNSNumber) {
        return userStore.checkUniqueSNSNumber(SNSNumber);
    }

    /**
     * Check unique nhs user citizen card number boolean.
     *
     * @param citizenCardNumber the citizen card number
     * @return the boolean
     */
    public boolean checkUniqueNHSUserCitizenCardNumber(int citizenCardNumber) {
        return userStore.checkUniqueCitizenCardNumber(citizenCardNumber);
    }

    /**
     * Valid email boolean.
     *
     * @param email the email
     * @return the boolean
     */
    public boolean validEmail(String email){
        return email.contains("@") && email.contains(".");
    }

}
