package app.controller;

import app.domain.dto.NHSUserDto;
import app.domain.model.Company;
import app.domain.model.NHSUser;
import app.domain.shared.Constants;
import app.ui.console.stores.NHSUserStore;
import org.apache.commons.lang3.RandomStringUtils;
import pt.isep.lei.esoft.auth.AuthFacade;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * The type Us 10 controller.
 */
public class US14Controller {
        private NHSUser nhsUser;
        private Company company;
        private AuthFacade facade;
        private NHSUserStore store;

        /*
        Question: "In witch format will be given the date of birth (YYYY/MM/DD or DD/MM/YYYY)"

        Answer: In a previous clarification I said: "The dates registered in the system should follow the Portuguese format (dd/mm/yyyy)".
         */
        private SimpleDateFormat formatDate =new SimpleDateFormat("dd/MM/yyyy");

    /**
     * Instantiates a new Us 14 controller.
     *
     * @param app the app
     */
    public US14Controller(App app){
            company=app.getCompany();
            facade=company.getAuthFacade();
            store= company.getNHSUserStore();
        }

        private boolean checkExistsFilePath(String InitialFilePath){
            return (new File(InitialFilePath).exists());
        }

        private boolean checkFileExtension(String FilePath){
            return (((FilePath.split("\\.",0))[1]).equals("csv") );
        }

    /**
     * Gets users from file.
     *
     * @param FilePath the file path
     * @return the users from file
     * @throws FileNotFoundException the file not found exception
     * @throws ParseException        the parse exception
     */
    public List<NHSUserDto> getUsersFromFile(String FilePath) throws FileNotFoundException, ParseException {

            /*
            Question: "Regarding US014, I would like to clarify if the CSV file only contains information about SNS users of if the CSV file may also contain some information about employees from that vaccination center."

            Answer: The CSV file only contains information about SNS users.
             */
            List<NHSUserDto> usersList = new ArrayList<NHSUserDto>();
            File inpFile = new File(FilePath);
            Scanner inpInfo = new Scanner(inpFile);
            String readLine = "";
            String splitChar = ",";
            String[] userInfoList;

            /*
            Question: "On the last meeting you said that header contains names of the attributes that are listed in the file. My question is what is the delimiter for the header? Does it have points between words, commas or something else?"

            Answer: Each type of CSV file has only one delimiter character.
            Acceptance criteria for US14: The application must support importing two types of CSV files: a) one type must have a header, column separation is done using “;” character; b) the other type does not have a header, column separation is done using “,” character.

            Question: "Should our application detect if the CSV file to be loaded contains the header, or should we ask the user if is submitting a file with a header or not?"

            Answer: The application should automatically detect the CSV file type.
             */
            String bufferLine = inpInfo.nextLine();
            if ((bufferLine).contains(";")){
                splitChar = ";";
            } else {
                userInfoList = bufferLine.split(splitChar);
                if (ValidInfo(userInfoList)){
                    /*
                    Question: "is there any specific format that should be validated for the address, or we can assume it is just of string type?"

                    Answer: The address contained in the CSV file is a string and should not contain commas or semicolons.

                    Question: "What would be the sequence of parameters to be read on the CSV? For example: "Name | User Number".

                    Answer: Name, Sex, Birth Date, Address, Phone Number, E-mail, SNS User Number and Citizen Card Number.
                     */
                    usersList.add(new NHSUserDto(userInfoList[0],userInfoList[3],userInfoList[1],Integer.parseInt(userInfoList[4]),userInfoList[5],formatDate.parse(userInfoList[2]),Integer.parseInt(userInfoList[6]),Integer.parseInt(userInfoList[7])));
                }
            }
            while (inpInfo.hasNextLine()){
                readLine = inpInfo.nextLine();
                userInfoList = readLine.split(splitChar);
                /*
                Question: "Should we check if the users from the CSV file are already registered in the system? If so, which data should we use, the one already in the system or the one on the file?"

                Answer: This feature of the system will be used to register a batch users. If the user is already registered in the system, then the information available in the CSV file should not be used to register that user.

                Question: "Are there any SNS User attributes that can be omitted?"

                Answer: I already answered one question related to US3 that answers your question. The Sex attribute is optional (it can also take the NA value). All other fields are required

                Question: "What should the system do if the file to be loaded has information that is repeated? For example, if there are 5 lines that have the same information or that have the same attribute, like the phone number, should the whole file be discarded?"

                Answer: If the file does not have other errors, all records should be used to register users. The business rules will be used to decide if all file records will be used to register a user.
                For instance, if all users in the CSV file are already registered in system, the file should be processed normally but no user will be added to the system (because the users are already in the system).

                 */
                if (ValidInfo(userInfoList)){
                    /*
                    Question: "is there any specific format that should be validated for the address, or we can assume it is just of string type?"

                    Answer: The address contained in the CSV file is a string and should not contain commas or semicolons.

                    Question: "What would be the sequence of parameters to be read on the CSV? For example: "Name | User Number".

                    Answer: Name, Sex, Birth Date, Address, Phone Number, E-mail, SNS User Number and Citizen Card Number.
                     */
                    usersList.add(new NHSUserDto(userInfoList[0],userInfoList[3],userInfoList[1],Integer.parseInt(userInfoList[4]),userInfoList[5],formatDate.parse(userInfoList[2]),Integer.parseInt(userInfoList[6]),Integer.parseInt(userInfoList[7])));
                }
            }
            return usersList;
        }

    private boolean ValidInfo(String[] userInfo) {
        // Checks if parameters are Integers
        boolean val = isInt(userInfo[4]) && isInt(userInfo[6]) && isInt(userInfo[7]);
        // Checks if the strings aren't empty
        val &= !userInfo[0].trim().isEmpty() && !userInfo[1].trim().isEmpty() && !userInfo[3].trim().isEmpty() && !userInfo[5].trim().isEmpty();
        // Uses NHSUserStore verifications
        val &= storeVerifications(userInfo[5],Integer.parseInt(userInfo[4]),Integer.parseInt(userInfo[6]), Integer.parseInt(userInfo[7]) );
        //val &= store.checkUniqueEmail(userInfo[5]) && store.checkUniquePhone(Integer.parseInt(userInfo[4])) && store.checkUniqueSNSNumber(Integer.parseInt(userInfo[6])) && store.checkUniqueCitizenCardNumber(Integer.parseInt(userInfo[7]));
        return val;
    };

    private boolean storeVerifications(String email, int phone, int snsNumber, int citizenCardNumber){
        return (store.checkUniqueEmail(email) && store.checkUniquePhone(phone) && store.checkUniqueSNSNumber(snsNumber) && store.checkUniqueCitizenCardNumber(citizenCardNumber));
    }

    private boolean isInt(String value){
        try{
            int val = Integer.parseInt(value);
        } catch(Exception e){
            return false;
        }
        return true;
    }

    /**
     * Save users list.
     *
     * @param usersList the users list
     */
    public void saveUsersList(List<NHSUserDto> usersList) {
        String password;
        System.out.println("Saved Users: \n");
        for (NHSUserDto dto : usersList){
            if( storeVerifications(dto.getEmail(),dto.getPhone(),dto.getSNSNumber(),dto.getCitizenCardNumber()) && !facade.existsUser(dto.getEmail()) ) {
                password = generatePassword();
                store.Save(store.Create(dto), facade);
                facade.addUserWithRole(dto.getName(),dto.getEmail(),password, Constants.ROLE_NHS_USER);
                /*
                Question: "how should the admin receive the login data/passwords for all registered users?"

                Answer: During this sprint, login and password data should be presented in the console application.
                In US14 the application is used to register a batch of users. For each user, all the data required to register a user should be presented in the console.
                */
                System.out.println("Name: " + dto.getName() + "\n" +
                        "Phone: " + dto.getPhone() + "\n" +
                        "Email: " + dto.getEmail() + "\n" +
                        "Password: " + password + "\n");
            }
        }
    }

    private String generatePassword(){
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?";
        String pwd = RandomStringUtils.random( 15, characters );
        return pwd;
    }

    /**
     * Is file valid boolean.
     *
     * @param filePath the file path
     * @return the boolean
     */
    public boolean isFileValid(String filePath){
        return ( checkExistsFilePath(filePath) && checkFileExtension(filePath) );
    }

    /**
     * Register users from file.
     *
     * @param filePath the file path
     * @throws FileNotFoundException the file not found exception
     * @throws ParseException        the parse exception
     */
    public void registerUsersFromFile(String filePath) throws FileNotFoundException, ParseException {
        List<NHSUserDto> listOfUsers = getUsersFromFile(filePath);
        saveUsersList(listOfUsers);
    }
}

