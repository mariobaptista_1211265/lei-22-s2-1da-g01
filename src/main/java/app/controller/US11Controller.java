package app.controller;


import pt.isep.lei.esoft.auth.mappers.dto.UserDTO;
import pt.isep.lei.esoft.auth.mappers.dto.UserRoleDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Us 11 controller.
 *
 * @author Manuel Pimentel <1210670@isep.ipp.pt>
 */
public class US11Controller {

    private App app;

    /**
     * Instantiates a new Us 11 controller.
     *
     * @param app the app
     */
    public US11Controller(App app){
        this.app=app;
    }

    /**
     * Get list of roles that exist list.
     *
     * @return the list
     */
    public List<String> getListOfRolesThatExist(){
        List<String> newL = new ArrayList<>();
        List<UserRoleDTO> l = app.getCompany().getAuthFacade().getUserRoles();
        for (UserRoleDTO dto : l) {
            newL.add(dto.getDescription());
        }
        return newL;
//        return EmployeeStore.getListOfRolesThatExist();
    }

    /**
     * Get list of users with role list.
     *
     * @param role the role
     * @return the list
     */
    public List<UserDTO> getListOfUsersWithRole(String role){
        return app.getCompany().getAuthFacade().getUsersWithRole(role);
    }
}
