package app.controller;

import app.domain.model.Company;
import app.domain.shared.Constants;
import app.ui.console.stores.CoordinatorStore;
import app.ui.console.stores.VaccinationLogStore;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class US16Controller {
    private Company company;
    private CoordinatorStore coordinatorStore;
    private int centerID;
    private Properties props;
    private VaccinationLogStore logStore;

    SimpleDateFormat dateFormatter =new SimpleDateFormat("dd-MM-yyyy");

    public US16Controller(App app){
        company=app.getCompany();
        props = app.getProperties();
        coordinatorStore= company.getCoordinatorStore();
        logStore = company.getLogStore();
        try {
            String email = app.getCurrentUserSession().getUserId().getEmail();
            centerID = coordinatorStore.getCenterIDByEmail(email);
        } catch (Exception e){
            centerID = -1;
        }
    }

    public boolean checkCenterExists() {
        return !(centerID==-1);
    }

    public List<String> getDatesWithInfo(){
        //System.out.println(centerID);
        return  logStore.getDatesWithRecordsByCenter(centerID);
    }


    public void doLogoutGUI() {
        new AuthController().doLogout();
    }

    public int[] getListOfDifferences(double timeInterval, Date dateSelected) {
        return logStore.getDifferencesInDay(timeInterval,dateSelected,centerID);
    }

    public int[] getMaxContiguousSum(int[] differences) {
        int selectionOfAlgorithmFromConfig = getAlgorithmFromProps();
        int[] subListWithGreatestContagiousSum;
        if (selectionOfAlgorithmFromConfig == 0){
            subListWithGreatestContagiousSum = Sum.maxBruteForce(differences);
        } else if (selectionOfAlgorithmFromConfig == 1){
            subListWithGreatestContagiousSum = Sum.max(differences);
        } else {
            System.out.println("Error in the selection of algorithm from config file.\nProceeding with Brute force.");
            subListWithGreatestContagiousSum = Sum.maxBruteForce(differences);
        }
        return  subListWithGreatestContagiousSum;
    }

    public int getStartOfSublist(int[] sublist, int[] list) {
        return Sum.indexOfSubList(sublist, list);
    }

    public int getAlgorithmFromProps(){
        try {
            return Integer.parseInt(props.getProperty(Constants.PARAMS_CENTERPERFORMANCE_ALGORITHM));
        } catch (Exception e){
            System.out.println("Error in reading config file.");
        }
        return -1;
    }

    public void changeAlgorithmProps(int algorithmID) throws IOException {
        props.setProperty(Constants.PARAMS_CENTERPERFORMANCE_ALGORITHM, String.valueOf(algorithmID));
        props.store(new FileOutputStream("src/main/resources/" + Constants.PARAMS_FILENAME), null);
    }

    public void setProperty(String param, int value) {
        props.setProperty(param,String.valueOf(value));
    }

    public boolean validDate(List<String> possibleDays, String inpDate) {
        try{
            new SimpleDateFormat("dd-MM-yyyy").parse(inpDate);
        } catch (Exception e){
            return false;
        }
        if (!possibleDays.contains(inpDate)){
            return false;
        }
        return true;

    }
}
