package app.controller;

import app.domain.model.Company;
import app.domain.model.VacType;
import app.domain.model.AdministrationProcess;
import app.ui.console.stores.VaccineAdmStore;

/**
 * @author Pedro Sá nº1211269
 *
 * The type Create vac adm controller.
 */
public class CreateVacAdmController {

    private AdministrationProcess vaccineAdmStore;

    private Company company;

    private VacType vactypes;

    private VaccineAdmStore store;

    public CreateVacAdmController(App app){
        company=app.getCompany();
        vactypes= company.getVactypes();
        store= company.getVaccineAdmStore();
    }

    /**
     * Create process administration process.
     *
     * @param brand         the brand
     * @param vactype       the vactype
     * @param ageI          the age i
     * @param ageF          the age f
     * @param dosage        the dosage
     * @param numberOfDoses the number of doses
     * @param timeInterval  the time interval
     * @return the administration process
     */
    public AdministrationProcess createProcess(String brand, String vactype,int ageI,int ageF,double dosage,double numberOfDoses,int timeInterval)
    {
        vaccineAdmStore = store.createProcess(brand,vactype,ageI, ageF, dosage, numberOfDoses, timeInterval);
        return vaccineAdmStore;
    }
}
