package app.controller;

import app.domain.model.*;
import app.domain.model.Company;
import app.domain.model.VacType;
import app.domain.model.WaitingRoom;
import app.domain.shared.Constants;
import app.ui.console.stores.*;
import pt.isep.lei.esoft.auth.AuthFacade;
import pt.isep.lei.esoft.auth.UserSession;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Properties;
import java.util.Timer;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class App {


    // TODO This cannot be static due to singleton nature of App. This has to be of the object. Refactor later

    private Company company;
    private AuthFacade authFacade;
    private VacType vactypes;
    private Timer StatisticTimer;
    private Properties props;

    public App()
    {
        props = System.getProperties();
        try(FileReader reader = new FileReader("config.properties")){
            props.load(reader);
        } catch (IOException e) {
            e.printStackTrace();
        }
        company = new Company(props.getProperty(Constants.PARAMS_COMPANY_DESIGNATION));
        this.authFacade = company.getAuthFacade();
        vactypes = company.getVactypes();

        StatisticTimer = new Timer();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY,Integer.parseInt(props.getProperty("Timer.hour")));
        calendar.set(Calendar.MINUTE,Integer.parseInt(props.getProperty("Timer.minute")));
        StatisticTimer.scheduleAtFixedRate(new NumberOfUsersVaccinated(),calendar.getTime(),24*60*60*1000);


    }

    public Company getCompany()
    {
        return company;
    }

    public Properties getProperties(){ return props; }

    public UserSession getCurrentUserSession()
    {
        return this.authFacade.getCurrentUserSession();
    }

    public boolean doLogin(String email, String pwd)
    {
        return this.authFacade.doLogin(email,pwd).isLoggedIn();
    }

    public void doLogout()
    {
        this.authFacade.doLogout();
    }

    public void readPropertiesFromFile()
    {
        props = new Properties();

        // Add default properties and values
        props.setProperty(Constants.PARAMS_COMPANY_DESIGNATION, "DGS/SNS");


        // Read configured values
        try
        {
            InputStream in = new FileInputStream(Constants.PARAMS_FILENAME);
            props.load(in);
            in.close();
        }
        catch(IOException ex)
        {
            System.out.println("Error in reading the properties file");
        }
    }


    private void bootstrap()
    {

        readPropertiesFromFile();

        this.authFacade.addUserRole(Constants.ROLE_ADMIN,Constants.ROLE_ADMIN);
        this.authFacade.addUserRole(Constants.ROLE_RECEPTIONIST,Constants.ROLE_RECEPTIONIST);
        this.authFacade.addUserRole(Constants.ROLE_NURSE,Constants.ROLE_NURSE);
        this.authFacade.addUserRole(Constants.ROLE_COORDINATOR,Constants.ROLE_COORDINATOR);
        this.authFacade.addUserRole(Constants.ROLE_NHS_USER,Constants.ROLE_NHS_USER);

        // Users

        this.authFacade.addUserWithRole("Main Administrator", "admin@lei.sem2.pt", "123456", Constants.ROLE_ADMIN);
        this.authFacade.addUserWithRole("Receptionist #1", "receptionist1@lei.sem2.pt", "123456", Constants.ROLE_RECEPTIONIST);
        this.authFacade.addUserWithRole("User 1", "nhsuser@lei.sem2.pt", "123456", Constants.ROLE_NHS_USER);
        this.authFacade.addUserWithRole("Nurse 1", "nurse1@lei.sem2.pt", "123456", Constants.ROLE_NURSE);
        this.authFacade.addUserWithRole("Roberto","roberto@teste.pt","1234",Constants.ROLE_COORDINATOR);

       ImportData();

        //VaccinationLogStore.saveRecord(new VacAdministration(new VaccinationDTO(123, "123", 2, "12", new Date(), new Date(), new Date(), new Date(), 1) ));


/*
        company.getCoordinatorStore().saveWithId("roberto@teste.pt", 1, "Roberto");

        EmployeeDto dto = new EmployeeDto("Receptionist #1", "Rua da recepcionista 123", 966455233, "receptionis1@lei.sem2.pt", 123456543, "1223-231", "Porto", Constants.ROLE_RECEPTIONIST, "123456");

        Employee emp = EmployeeStore.Create(dto);
        EmployeeStore.Save(emp, authFacade);

        // Removed this part of the bootstrap because it is not necessary anymore

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        try {
            date = df.parse("02-12-2000");
        } catch (ParseException ignore) {
        }
        US03Controller ctrl = new US03Controller(App.getInstance());
        ctrl.createNHSUser("Inês Maria Alves Ramalho", "Rua da Alegria", "Female", 987654321, "nhsuser@lei.sem2.pt", date, 987654321, 123456789, "123456");
        ctrl.saveNHSUser();

        try {
            date = df.parse("21-07-2001");
        } catch (ParseException ignore) {
        }
        ctrl.createNHSUser("Rui Moreira da Silva", "Rua da Alegria", "Male", 987654323, "nhsuser2@lei.sem2.pt", date, 987654322, 124456789, "132456");
        ctrl.saveNHSUser();

        try {
            date = df.parse("11-10-2003");
        } catch (ParseException ignore) {
        }
        ctrl.createNHSUser("Pedro Maria Ferreira", "Rua da Alegria", "Male", 987654322, "nhsuser3@lei.sem2.pt", date, 987654323, 122456789, "123456");
        ctrl.saveNHSUser();

        // Vaccine types

        vactypes.setVacType("Covid19");
        vactypes.setVacType("Hepatite A");
        vactypes.setVacType("Varicela");


        // Centers


        CenterDto testVacDto = new CenterDto(1,"Centro de vacinacao em massa do Porto", "Rua do Porto", 123456789, "centrodoporto@gmail.com", "6","23","5",3,75);
        VaccinationCenter testVac = new VaccinationCenter(testVacDto);
        testVac.setVACVaccinetype("Covid19");
        CenterStore.SaveVac(testVac);

        CenterDto testVacDto2 = new CenterDto(2,"Centro de vacinacao em massa de Espinho", "Rua 19", 987123682, "centrodeespinho@gmail.com", "10","20","3",2,75);
        VaccinationCenter testVac2 = new VaccinationCenter(testVacDto2);
        testVac2.setVACVaccinetype("Hepatite A");
        CenterStore.SaveVac(testVac2);

        CenterDto testVacDto3 = new CenterDto(3,"Centro de saude da Maia", "Rua da Maia", 987654321, "centrodamaia@gmail.com", "5","19","3",3,35);
        HealthCareCenter testVac3 = new HealthCareCenter(testVacDto3,"Organizacao","Company");
        CenterStore.SaveHealth(testVac3);

        CenterDto testVacDto4 = new CenterDto(4,"Centro de saude de Lisboa", "Rua de Lisboa", 954264871, "centrodelisboa@gmail.com", "5","23","3",5,50);
        HealthCareCenter testVac4 = new HealthCareCenter(testVacDto4,"Company","Organizacao");
        CenterStore.SaveHealth(testVac4);


        // Vaccines

        Vaccine v1 =  VaccineStore.CreateVaccine("Janssen");
        VaccineStore.SaveVaccine(v1);
        Vaccine v2 =VaccineStore.CreateVaccine("Spikevax");
        VaccineStore.SaveVaccine(v2);
        Vaccine v3 =VaccineStore.CreateVaccine("Comirnaty");
        VaccineStore.SaveVaccine(v3);

        // Vaccine administration processes

        VaccineAdmStore.saveProcess("Janssen","Covid19", 8, 75,10,2,5);
        VaccineAdmStore.saveProcess("Spikevax","Covid19", 12, 90,12,3,60);
        VaccineAdmStore.saveProcess("Comirnaty","Hepatite A", 12, 90,12,1,0);
        VaccineAdmStore.saveProcess("Comirnaty","Varicela", 2, 25,8,1,0);

        // Vaccine schedules

        SimpleDateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm");

        date = null;
        try {
            date = df2.parse("31-05-2022 11:00");
        } catch (ParseException ignore) {
        }

        SimpleDateFormat hours = new SimpleDateFormat("HH");
        SimpleDateFormat minutes = new SimpleDateFormat("mm");

        String hour = hours.format(date);
        String minute = minutes.format(date);


        String time = hour + ":" + minute;

        VaccineSchedule vs = new VaccineSchedule(987654321, 1, date, time, "Covid19");
        VaccineSchedule vs2 = new VaccineSchedule(987654322, 1, date, time, "Hepatite A");
        VaccineSchedule vs3 = new VaccineSchedule(987654323, 3, date, time, "Varicela");

        VaccineScheduleStore.addScheduledVaccine(vs);
        VaccineScheduleStore.addScheduledVaccine(vs2);
        VaccineScheduleStore.addScheduledVaccine(vs3);

        UserArrival ua = new UserArrival(1, 987654321);
        WaitingRoom.addUserArrival(ua);

        UserArrival ua2 = new UserArrival(1, 987654322);
        WaitingRoom.addUserArrival(ua2);

        UserArrival ua3 = new UserArrival(3, 987654323);
        WaitingRoom.addUserArrival(ua3);
*/


    }

    public void ImportData(){
        NHSUserStore.ImportData(authFacade);
        CenterStore.ImportDataVacCenter();
        CenterStore.ImportDataHealthCenters();
        EmployeeStore.ImportData(authFacade);
        VaccineAdmStore.ImportData();
        VaccineScheduleStore.ImportData();
        VaccineStore.ImportData();
        VaccinationLogStore.ImportData();
        WaitingRoom.ImportData();
        VacType.ImportData();
        CoordinatorStore.ImportData(authFacade);
        CoordinatorStore.ImportData(authFacade);
        VaccinationLogStore.ImportData();

    }

    // Extracted from https://www.javaworld.com/article/2073352/core-java/core-java-simply-singleton.html?page=2
    private static App singleton = null;
    public static App getInstance()
    {
        if(singleton == null)
        {
            synchronized(App.class)
            {
                singleton = new App();
                singleton.bootstrap();

            }
        }
        return singleton;
    }
}
