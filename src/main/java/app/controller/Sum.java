package app.controller;

import java.util.Arrays;

public class Sum {
    public static int[] max(int[] seq){
        int maxSoFar = 0;
        int maxEndingHere = 0;
        int startMaxSoFar = 0;
        int endMaxSoFar = 0;
        int startMaxEndingHere = 0;
        for (int i = 0; i < seq.length; i++) {
            int elem = seq[i];
            int endMaxEndingHere = i + 1;
            if (maxEndingHere + elem < 0) {
                maxEndingHere = 0;
                startMaxEndingHere = i + 1;
            } else {
                maxEndingHere += elem;
            }
            if (maxSoFar < maxEndingHere) {
                maxSoFar = maxEndingHere;
                startMaxSoFar = startMaxEndingHere;
                endMaxSoFar = endMaxEndingHere;
            }
        }
        return Arrays.copyOfRange(seq, startMaxSoFar, endMaxSoFar);
    }

    public static int[] maxBruteForce(int[] listOfDifferences){
        int greatestSum = 0;
        int startingPosition = 0;
        int finalPosition = 0;
        int actualSum = 0;
        for (int i = 0; i<listOfDifferences.length; i++){
            for (int k = i; k< listOfDifferences.length; k++){
                actualSum += listOfDifferences[k];
                if (actualSum > greatestSum){
                    greatestSum = actualSum;
                    startingPosition = i;
                    finalPosition = k + 1;
                }
            }
            actualSum = 0;
        }
        return Arrays.copyOfRange(listOfDifferences, startingPosition, finalPosition);
    }

    public static int indexOfSubList(int[] source, int[] target) {
        int counter = 1;
        for (int positionOfTarget = 0; positionOfTarget < target.length - source.length; positionOfTarget++){
            if (source[0] == target[positionOfTarget]){
                for (int positionOfSource = 1 ; positionOfSource < source.length; positionOfSource ++){
                    if (source[positionOfSource] == target[positionOfTarget + positionOfSource]){
                        counter ++;
                    }
                }
                if (counter == source.length){
                    return positionOfTarget;
                }
            }
        }
        return -1;
    }

}
