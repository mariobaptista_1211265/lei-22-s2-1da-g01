package app.controller;

import app.domain.dto.CenterDto;
import app.domain.model.Company;
import app.domain.model.HealthCareCenter;
import app.domain.model.VacType;
import app.domain.model.VaccinationCenter;
import app.ui.console.stores.CenterStore;

/**
 * The US09 controller.
 *
 * @author João Fortuna <1211261@isep.ipp.pt>
 */
public class US09Controller {


    private VaccinationCenter vacCenter;

    private HealthCareCenter healthCenter;

    private Company company;

    private VacType vactypes;

    private CenterStore store;

    public US09Controller(App app){
        company=app.getCompany();
        store= company.getCenterStore();
        vactypes= company.getVactypes();

    }


    /**
     * Create vac center vaccination center.
     *
     * @param name           the name
     * @param address        the address
     * @param phoneNumber    the phone number
     * @param emailAddress   the email address
     * @param openingHours   the opening hours
     * @param closingHours   the closing hours
     * @param slotDuration   the slot duration
     * @param slots          the slots
     * @param maximumVacSlot the maximum vac slot
     * @return the vaccination center
     */
    public VaccinationCenter createVacCenter(int id,String name, String address, int phoneNumber, String emailAddress, String openingHours, String closingHours, String slotDuration,int slots, int maximumVacSlot){

        CenterDto centerDto = new CenterDto(id,name,address,phoneNumber,emailAddress,openingHours,closingHours,slotDuration,slots,maximumVacSlot);

        vacCenter =  store.CreateVac(centerDto);

        return vacCenter;

    }

    /**
     * Create health center health care center.
     *
     * @param name           the name
     * @param address        the address
     * @param phoneNumber    the phone number
     * @param emailAddress   the email address
     * @param openingHours   the opening hours
     * @param closingHours   the closing hours
     * @param slotDuration   the slot duration
     * @param slots          the slots
     * @param maximumVacSlot the maximum vac slot
     * @param ARS            the ars
     * @param AGES           the ages
     * @return the health care center
     */
    public HealthCareCenter createHealthCenter(int id,String name, String address, int phoneNumber, String emailAddress, String openingHours, String closingHours, String slotDuration,int slots, int maximumVacSlot, String ARS, String AGES){

        CenterDto centerDto = new CenterDto(id,name,address,phoneNumber,emailAddress,openingHours,closingHours,slotDuration,slots,maximumVacSlot);

        healthCenter =  store.CreateHealth(centerDto,ARS,AGES);

        return healthCenter;

    }

    /**
     * Shows the existent vaccine types owned by the company.
     */
    public void showVacTypes(){
        vactypes.showVacTypes();
    }

    /**
     * Gets the vaccine type wanted by the admin.
     *
     * @param op the position of the wanted type in the list.
     * @return the vaccine type (String).
     */
    public String getVAVVacType(int op){ return vactypes.getType(op);}

    /**
     * Saves the vaccination center in the center store.
     *
     * @param vcenter the vaccination center.
     */
    public void saveVacCenter(VaccinationCenter vcenter){
        store.SaveVac(vcenter);
    }

    /**
     * Saves the  health care center in the center store.
     *
     * @param hcenter the health care center.
     */
    public void saveHealthCenter(HealthCareCenter hcenter){
        store.SaveHealth(hcenter);
    }

    /**
     * Checks if the phone number of the new vaccination center being created already exists in other center.
     *
     * @param phone the phone number
     * @return false if the number already exists.
     */
    public boolean checkUniqueVacPhone(int phone){
        return store.checkUniqueVacPhone(phone);
    }

    /**
     * Checks if the email of the new vaccination center being created already exists in other center.
     *
     * @param email the email
     * @return false if the email already exists.
     */
    public boolean checkUniqueVacEmail(String email){
        return store.checkUniqueVacEmail(email);
    }

    /**
     * Checks if the phone number of the new health care center being created already exists in other center.
     *
     * @param phone the phone number
     * @return false if the number already exists.
     */
    public boolean checkUniqueHealthPhone(int phone){
        return store.checkUniqueHealthPhone(phone);
    }

    /**
     * Checks if the email of the new health care center being created already exists in other center.
     *
     * @param email the email
     * @return false if the email already exists.
     */
    public boolean checkUniqueHealthEmail(String email){
        return store.checkUniqueHealthEmail(email);
    }

    public boolean checkUniqueCenterID(int id){ return store.checkUniqueCenterID(id);}
}
