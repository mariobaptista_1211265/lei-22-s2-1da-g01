package app.controller;


import app.domain.dto.VaccinationDTO;
import app.domain.model.Company;
import app.domain.model.VacAdministration;
import app.domain.model.WaitingRoom;
import app.domain.shared.Constants;
import app.ui.console.stores.NHSUserStore;
import app.ui.console.stores.VaccinationLogStore;
import app.ui.console.stores.VaccineScheduleStore;
import app.ui.console.stores.VaccineStore;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Timer;

public class US08Controller {

    private Company company;

    private WaitingRoom waitingRoom;

    private VaccinationDTO vaccinationDTO;

    private VacAdministration vacConfirmation;

    private VaccinationLogStore logStore;

    private VaccineStore vaccines;

    private NHSUserStore nhsUsers;

    private VaccineScheduleStore vaccineScheduleStore;

    public US08Controller(App app){
        company=app.getCompany();
        waitingRoom=company.getWaitingRoom();
        logStore=company.getLogStore();
        vaccines =company.getVaccineStore();
        nhsUsers=company.getNHSUserStore();
        vaccineScheduleStore=company.getScheduleStore();
    }


    public boolean checkIfUserExistsInWaitingRoom(int centerid, int number){
        return waitingRoom.checkIfUserExistsInWaitingRoom(centerid, number);
    }

    public VaccinationDTO createVacAdministrationDTO(int snsNumber, String vaccineName, int vaccineDose, String lotNumber, Date scheduledDateTime, Date arrivalDateTime, Date confirmationDateTime, Date leavingDateTime, int centerID){
        return new VaccinationDTO(snsNumber,vaccineName,vaccineDose,lotNumber,scheduledDateTime,arrivalDateTime,confirmationDateTime,leavingDateTime,centerID);
    }


    public void saveVacAdministration(VaccinationDTO vaccinationDTO){
        vacConfirmation = logStore.createRecord(vaccinationDTO);

        logStore.saveRecord(vacConfirmation);
    }


    public void showVaccines(){
        vaccines.showVaccines();
    }

    public String getVaccineName(int option){
        return vaccines.getVaccineName(option);
    }



    public Date getScheduledDateTime(int snsNumber){
        return vaccineScheduleStore.getScheduledDateTime(snsNumber);
    }

    public Date getArrivalDateTime(int centerid, int snsNumber){
        return waitingRoom.getDateOfArrival(centerid, snsNumber);
    }

    public Date getAdministrationDateTime(){
        return new Date();
    }

    public Date getLeavingDateTime(Date administrationDateTime){

       // SimpleDateFormat minutes = new SimpleDateFormat("mm");

      //  int minutesToAdd = Integer.parseInt(minutes.format(administrationDateTime)) + (Constants.DEFAULT_RECOVERY_PERIOD_IN_MILLISECONDS / 60000 + 2);

        Date leavingDateTime = new Date(administrationDateTime.getTime() + 1800000);

        return leavingDateTime;

    }


    public void sendLeaveMessage(int snsNumber){

        Timer t = new java.util.Timer();

        t.schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {


                        File smsFile = new File("sms.txt");

                        String smsString = "Your recovery period has ended. You can now leave the Center. Thank You!";

                        try {
                            if (smsFile.createNewFile()) {
                                int a = 0;
                            } else {
                                int a = 1;
                            }
                            FileWriter myWriter = new FileWriter("sms.txt", true);
                            myWriter.write("SMS message to: " + nhsUsers.getPhoneNumberBySNS(snsNumber) + "\n");
                            myWriter.write(smsString);
                            myWriter.write("\n--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
                            myWriter.close();
                        } catch (IOException e) {
                            System.out.println("An error occurred.");
                            e.printStackTrace();
                        }

                        t.cancel();
                    }
                },
                Constants.RECOVERY_PERIOD_FOR_MESSAGE_TEST
        );

    }





}
