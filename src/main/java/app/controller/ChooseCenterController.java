package app.controller;

import app.ui.console.stores.CenterStore;

public class ChooseCenterController {

    public ChooseCenterController(){}


    public static void showHealthCenters(){CenterStore.showHealthCenters();}

    public static void showVacCenters(){CenterStore.showMassVaccinationCenters();}

    public static int getVacCenterID(int option){
        return CenterStore.getVacID(option);
    }

    public static int getHealthCenterID(int option){
        return CenterStore.getHealthID(option);
    }

}
