package app.controller;

import app.domain.model.Company;
import app.domain.model.VacType;
import app.domain.model.Vaccine;
import app.ui.console.stores.VaccineStore;

/**
 * @author Pedro Sá nº1211269
 *
 * The type Create vaccine controller.
 */
public class CreateVaccineController {

    private Vaccine vac;

    private VaccineStore vaccineStore;

    private Company company ;

    private VacType vactypes;

    private VaccineStore store;

    public CreateVaccineController(App app){
        company=app.getCompany();
        vactypes= company.getVactypes();
        store = company.getVaccineStore();
    }

    /**
     * Create vaccine vaccine.
     *
     * @param brand the brand
     * @return the vaccine
     */
    public Vaccine createVaccine(String brand)
    {
        vac = store.CreateVaccine(brand);
        return vac;
    }

    /**
     * Save vaccine.
     *
     * @param brand the brand
     */
    public void saveVaccine(Vaccine brand){
        store.SaveVaccine(brand);
    }

    /**
     * Check dups boolean.
     *
     * @param brand the brand
     * @return the boolean
     */
    public boolean CheckDups (String brand){
        return store.CheckDups(brand);
    }

    /**
     * Check dups 2 boolean.
     *
     * @param brand the brand
     * @return the boolean
     */
    public boolean CheckDups2 (String brand){
        return store.CheckDups2(brand);
    }
}
