package app.controller;

import app.domain.dto.WaitingRoomDTO;
import app.domain.model.UserArrival;
import app.domain.model.WaitingRoom;
import app.ui.console.stores.CenterStore;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Us 05 controller.
 *
 * @author Manuel Pimentel <1210670@isep.ipp.pt>
 */
public class US05Controller {

    private App app;

    /**
     * Instantiates a new Us 05 controller.
     *
     * @param app the app
     */
    public US05Controller(App app){
        this.app=app;
    }

    /**
     * Show vac centers.
     */
    public void showVacCenters(){
        CenterStore.showMassVaccinationCenters();
    }

    /**
     * Show health centers.
     */
    public void showHealthCenters(){
        CenterStore.showHealthCenters();
    }

    /**
     * Get health id int.
     *
     * @param option the option
     * @return the int
     */
    public int getHealthID(int option){
        return CenterStore.getHealthID(option);
    }

    /**
     * Get vac id int.
     *
     * @param option the option
     * @return the int
     */
    public int getVacID(int option){
        return CenterStore.getVacID(option);
    }

    /**
     * Get vac center name string.
     *
     * @param idCenter the id center
     * @return the string
     */
    public String getVacCenterName(int idCenter){
        return CenterStore.getVacCenter(idCenter).getName();
    }

    /**
     * Get health center name string.
     *
     * @param idCenter the id center
     * @return the string
     */
    public String getHealthCenterName(int idCenter){
        return CenterStore.getHealthCenter(idCenter).getName();
    }

    /**
     * Show waiting room from center list.
     *
     * @param idCenter the id center
     * @return the list
     */
    public List<WaitingRoomDTO> showWaitingRoomFromCenter(int idCenter){
        List<UserArrival> waitingList =  WaitingRoom.getWaitingList();

        List<WaitingRoomDTO> results = new ArrayList<>();
        for(UserArrival user : waitingList){
            if(user.getCenterID() == idCenter){
                WaitingRoomDTO w = new WaitingRoomDTO(App.getInstance().getCompany().getNHSUserStore().findByNumber(user.getUserNumber()).getName(),user.getDateOfArrival());
                results.add(w);
            }
        }
        return results;
    }
}
