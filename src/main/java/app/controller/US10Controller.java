package app.controller;


import app.domain.dto.EmployeeDto;
import app.domain.model.Company;
import app.domain.model.Employee;
import app.ui.console.stores.EmployeeStore;
import org.apache.commons.lang3.RandomStringUtils;
import pt.isep.lei.esoft.auth.AuthFacade;
import pt.isep.lei.esoft.auth.mappers.dto.UserRoleDTO;

import java.util.List;

/**
 * The type Us 10 controller.
 */
public class US10Controller {
    private Employee employee;
    private Company company;
    private AuthFacade facade;
    private EmployeeStore store;
    private static App app;

    public US10Controller(App app){
        this.app = app;
        company=app.getCompany();
        facade=company.getAuthFacade();
        store= company.getEmployeeStore();
    }


    /**
     * Create employee employee.
     *
     * @param name              the name
     * @param address           the address
     * @param phone             the phone
     * @param email             the email
     * @param citizenCardNumber the citizen card number
     * @param postCode          the post code
     * @param city              the city
     * @param role              the role
     * @return the employee
     */
    public Employee createEmployee(String name, String address, int phone, String email, int citizenCardNumber, String postCode, String city, String role) {

        String password = generatePassword();

        EmployeeDto userDto = new EmployeeDto(name, address, phone, email, citizenCardNumber, postCode, city, role, password);

        employee = EmployeeStore.Create(userDto);

        return employee;
    }


    /**
     * Save employee.
     *
     * @param SaveEmployee the save employee
     */
    public void saveEmployee(Employee SaveEmployee){
        EmployeeStore.Save(SaveEmployee, facade);
        //facade.addUserWithRole(SaveEmployee.getName(), SaveEmployee.getEmail(),SaveEmployee.getPassword(), SaveEmployee.getRole());
    }

    /**
     * Check phone boolean.
     *
     * @param phone the phone
     * @return the boolean
     */
    public boolean checkPhone(int phone){
        return store.existsPhone(phone);
    }

    /**
     * Check email boolean.
     *
     * @param email the email
     * @return the boolean
     */
    public boolean checkEmail(String email) {
        return (store.existsEmail(email)) ;
    }


    /**
     * Check citizen card number boolean.
     *
     * @param citizenCardNumber the citizen card number
     * @return the boolean
     */
    public boolean checkCitizenCardNumber(int citizenCardNumber) {
        return store.existsCitizenCardNumber(citizenCardNumber);
    }

    /**
     * Get list of roles list.
     *
     * @return the list
     */
    public List<UserRoleDTO> getListOfRoles(){ return store.getListOfRoles();}

    /**
     * Role exists boolean.
     *
     * @param id the id
     * @return the boolean
     */
    public boolean roleExists(String id){
        return store.roleExists(id);
    }

    /**
     * Get facade auth facade.
     *
     * @return the auth facade
     */
    public AuthFacade getFacade(){
        return facade;
    }

    private String generatePassword(){
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?";
        String pwd = RandomStringUtils.random( 15, characters );
        System.out.println("User Password : " + pwd);
        return pwd;
    }

}
