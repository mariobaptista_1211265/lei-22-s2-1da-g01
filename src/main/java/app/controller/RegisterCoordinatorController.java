package app.controller;

import app.domain.model.Company;
import app.domain.model.NHSUser;
import app.ui.console.stores.CenterStore;
import app.ui.console.stores.CoordinatorStore;
import pt.isep.lei.esoft.auth.AuthFacade;

public class RegisterCoordinatorController {
    private NHSUser nhsUser;
    private Company company;
    private AuthFacade facade;
    private CoordinatorStore coordinatorStore;

    public RegisterCoordinatorController(App app){
        company=app.getCompany();
        facade=company.getAuthFacade();
        coordinatorStore= company.getCoordinatorStore();
    }

    public String[] getVacCenterNames(){
        return CenterStore.getVacCenterNames();
    }

    public void registerCoordinator(String email, String vacCenterName,String name){
        coordinatorStore.saveWithCenterName(email, vacCenterName, name);
    }


}
