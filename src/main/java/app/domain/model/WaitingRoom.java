package app.domain.model;

import app.domain.shared.Constants;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * The Waiting room.
 *
 * @author João Fortuna <1211261@isep.ipp.pt>
 */
public class WaitingRoom implements java.io.Serializable {

    /**
     * Instantiates a new Waiting room.
     */
    public WaitingRoom(){

    }

    private static List<UserArrival> WaitingList = new ArrayList<>();

    /**
     * Add user arrival.
     *
     * @param arrival the user arrival
     */
    public static void addUserArrival(UserArrival arrival){
        WaitingList.add(arrival);
        Serializable.ExportDataToBinaryFile(Collections.singletonList(WaitingList), Constants.WAITING_ROOM_FILE_NAME);

    }

    public static void ImportData(){
        String FileName = Constants.WAITING_ROOM_FILE_NAME;
        File file = new File(FileName);

        try
        {
            file.createNewFile();
            List<List<Object>> listaAuxiliar = new ArrayList<>();
            FileInputStream fis = new FileInputStream(file);
            if (fis.available() > 0) {
                ObjectInputStream ois = new ObjectInputStream(fis);

                listaAuxiliar = (List<List<Object>>) ois.readObject();
                ois.close();
                fis.close();

                List<Object> a = listaAuxiliar.get(0);

                WaitingList = (List<UserArrival>)(Object) a;
            }
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
        catch (ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
        }
    }


 /*   public static UserArrival getUserArrival(int centerID, int number){
        for (UserArrival user : WaitingList) {
            if (user.getCenterID() == centerID && user.getUserNumber() == number ) {
                return user;
            }
        }
        return null;
    }

 */

    /**
     * Check duplicated entry boolean.
     *
     * @param centerID the center id
     * @param number   the nhs number
     * @return the boolean
     */
    public static boolean checkDuplicatedEntry(int centerID, int number){
        for (UserArrival user : WaitingList) {
            if (user.getUserNumber() == number && user.getCenterID() == centerID && checkIfSameDay(user.getDateOfArrival())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check if same day boolean.
     *
     * @param dateOfArrival the date of arrival
     * @return the boolean
     */
    public static boolean checkIfSameDay(Date dateOfArrival){

        SimpleDateFormat day = new SimpleDateFormat("dd/MM/yyyy");
        Date now = new Date();


        if(day.format(now).equals(day.format(dateOfArrival))){
            return true;
        }

        return false;
    }

    /**
     * Get waiting list.
     *
     * @return the waiting list
     */
    public static List<UserArrival> getWaitingList(){
        return WaitingList;
    }


    public static boolean checkIfUserExistsInWaitingRoom(int centerID, int number){
        for (UserArrival user : WaitingList) {
            if (user.getUserNumber() == number && user.getCenterID() == centerID) {
                return true;
            }
        }
        return false;
    }


    public static Date getDateOfArrival(int centerID, int number){
        for (UserArrival user : WaitingList) {
            if (user.getUserNumber() == number && user.getCenterID() == centerID) {
                return user.getDateOfArrival();
            }
        }
        return null;
    }

}
