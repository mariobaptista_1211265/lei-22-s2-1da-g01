package app.domain.model;

import app.domain.dto.CoordinatorDto;

public class Coordinator implements java.io.Serializable{
    private int vacCenterID;
    private String email;

    private String name;

    private String password;

    public Coordinator(CoordinatorDto dto){
        vacCenterID = dto.getVacCenterID();
        email = dto.getEmail();
        name = dto.getName();
        password = dto.getPassword();
    }

    public String getEmail() {
        return email;
    }

    public int getCenterID(){
        return vacCenterID;
    }

    public String getName(){ return  name;}

    public String getPassword() {
        return password;
    }
}
