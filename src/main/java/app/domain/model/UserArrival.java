package app.domain.model;

import java.util.Date;

/**
 * The User arrival.
 *
 * @author João Fortuna <1211261@isep.ipp.pt>
 */
public class UserArrival implements java.io.Serializable {

    private int centerID = 0;
    private int userNumber = 0;
    private Date dateOfArrival = null;


    /**
     * Instantiates a new User arrival.
     *
     * @param centerid   the center id
     * @param usernumber the user nhs number
     */
    public UserArrival(int centerid,int usernumber){
        this.centerID = centerid;
        this.userNumber = usernumber;
        this.dateOfArrival = new Date();
    }

    /**
     * Gets center id.
     *
     * @return the center id
     */
    public int getCenterID() {
        return centerID;
    }

    /**
     * Sets center id.
     *
     * @param centerID the center id
     */
    public void setCenterID(int centerID) {
        this.centerID = centerID;
    }

    /**
     * Gets user nhs number.
     *
     * @return the user nhs number
     */
    public int getUserNumber() {
        return userNumber;
    }

    /**
     * Sets user nhs number.
     *
     * @param userNumber the user nhs number
     */
    public void setUserNumber(int userNumber) {
        this.userNumber = userNumber;
    }

    /**
     * Gets date of arrival.
     *
     * @return the date of arrival
     */
    public Date getDateOfArrival() {
        return dateOfArrival;
    }

    /**
     * Sets date of arrival.
     *
     * @param dateOfArrival the date of arrival
     */
    public void setDateOfArrival(Date dateOfArrival) {
        this.dateOfArrival = dateOfArrival;
    }

    @Override
    public String toString() {
        return "UserArrival{" +
                ", userNumber=" + userNumber +
                ", dateOfArrival=" + dateOfArrival +
                '}';
    }
}
