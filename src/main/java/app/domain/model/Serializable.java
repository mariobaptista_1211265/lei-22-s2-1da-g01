package app.domain.model;
import app.ui.console.stores.EmployeeStore;

import java.io.File;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Serializable {


    // serialização: gravando o objetos no arquivo binário "nomeArq"
    public static void ExportDataToBinaryFile(List<Object> lista, String fileName) {
        try
        {
            FileOutputStream fos = new FileOutputStream(fileName);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(lista);
            oos.close();
            fos.close();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
/*        File arq = new File(fileName);
        try {
            arq.delete();
            arq.createNewFile();

            ObjectOutputStream objOutput = new ObjectOutputStream(new FileOutputStream(arq));
            objOutput.writeObject(lista);
            objOutput.close();

        } catch(IOException erro) {
            System.out.printf("Error: %s", erro);
        }*/
    }

    // desserialização: recuperando os objetos gravados no arquivo binário "nomeArq"
    public static List<Object> ImportDataFromBinaryFile(String fileName, List<Object> listToFill) {

        return null;
        /*File boas = new File(fileName);

        try
        {
            List<Object> listaAuxiliar = new ArrayList<>();
            List<Object> apagar = new ArrayList<>();
            FileInputStream fis = new FileInputStream(boas);
            ObjectInputStream ois = new ObjectInputStream(fis);

            listaAuxiliar = (List<Object>) ois.readObject();
            ois.close();
            fis.close();

            System.out.println(Arrays.toString(listaAuxiliar.toArray()));

            apagar.addAll(listaAuxiliar);
            return apagar;
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
            return null;
        }
        catch (ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
            return null;
        }
/*        List<Object> lista = new ArrayList<>();
        try {
            File arq = new File(fileName);
            if (arq.exists()) {
                ObjectInputStream objInput = new ObjectInputStream(new FileInputStream(arq));
                lista = (List<Object>)objInput.readObject();
                objInput.close();
            }
        } catch(IOException | ClassNotFoundException erro1) {
            System.out.printf("Error: %s", erro1.getMessage());
        }
        try{
            System.out.println(Arrays.toString(lista.toArray()));
            listToFill = List.copyOf(lista);
        } catch (Exception e){
            System.out.println(e);
        }*/
    }

}