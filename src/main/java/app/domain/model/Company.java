package app.domain.model;

import app.ui.console.stores.CenterStore;
import app.ui.console.stores.EmployeeStore;
import app.ui.console.stores.NHSUserStore;
import app.ui.console.stores.*;
import org.apache.commons.lang3.StringUtils;
import pt.isep.lei.esoft.auth.AuthFacade;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Company {

    private String designation;
    private AuthFacade authFacade;
    private VacType vactypes = new VacType();
    private CenterStore centerStore = new CenterStore();
    private NHSUserStore nhsUserStore = new NHSUserStore();
    private EmployeeStore employeeStore = new EmployeeStore();
    private VaccineStore vaccineStore = new VaccineStore();
    private VaccineAdmStore admStore = new VaccineAdmStore();
    private CoordinatorStore corStore = new CoordinatorStore();

    private VaccineScheduleStore scheduleStore = new VaccineScheduleStore();
    private WaitingRoom waitingroom = new WaitingRoom();

    private VaccinationLogStore logStore = new VaccinationLogStore();



    public Company(String designation)
    {
        if (StringUtils.isBlank(designation))
            throw new IllegalArgumentException("Designation cannot be blank.");

        this.designation = designation;
        this.authFacade = new AuthFacade();
    }

    public String getDesignation() {
        return designation;
    }

    public AuthFacade getAuthFacade() {
        return authFacade;
    }

    public CenterStore getCenterStore(){ return centerStore;}

    public VacType getVactypes(){return vactypes;}

    public NHSUserStore getNHSUserStore(){ return nhsUserStore;}

    public EmployeeStore getEmployeeStore(){
        return employeeStore;
    }

    public VaccineStore getVaccineStore() { return vaccineStore; }

    public VaccineAdmStore getVaccineAdmStore() { return admStore; }

    public WaitingRoom getWaitingRoom(){ return waitingroom; }

    public VaccineScheduleStore getScheduleStore() {
        return scheduleStore;
    }

    public VaccinationLogStore getLogStore() {
        return logStore;
    }

    public CoordinatorStore getCoordinatorStore(){ return corStore; }

}
