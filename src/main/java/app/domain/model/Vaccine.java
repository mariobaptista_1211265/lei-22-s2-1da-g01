package app.domain.model;

/**
 * @author Pedro Sá nº1211269
 *
 * The type Vaccine.
 */
public class Vaccine implements java.io.Serializable {

    private String brand;

    /**
     * Instantiates a new Vaccine.
     *
     * @param brand the brand
     */
    public Vaccine(String brand){
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "Vaccine{" +
                "brand='" + brand + '\'' +
                '}';
    }

    /**
     * Gets brand.
     *
     * @return the brand
     */
    public  String getBrand() {
        return this.brand;
    }

    /**
     * Sets brand.
     *
     * @param brand the brand
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }
}
