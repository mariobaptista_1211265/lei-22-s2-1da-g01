package app.domain.model;

import app.domain.shared.Constants;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Vaccine type class.
 *
 * @author João Fortuna <1211261@isep.ipp.pt>
 */
public class VacType implements java.io.Serializable {
    /**
     * @param VacTypes an arraylist with all existent vaccine types
     */
    private static List<String> VacTypes = new ArrayList<>();
    private String type;

    /**
     * Instantiates a new Vac type.
     */
    public VacType(){

    }


    /**
     * adds a new type to the vaccine types list
     *
     * @param type the type
     */
    public void setVacType(String type){
        this.type = type;
        VacTypes.add(this.type);
        Serializable.ExportDataToBinaryFile(Collections.singletonList(VacTypes), Constants.VACCINATION_TYPES_FILE_NAME);

    }

    public static void ImportData(){
        String FileName = Constants.VACCINATION_TYPES_FILE_NAME;
        File file = new File(FileName);

        try
        {
            file.createNewFile();
            List<List<Object>> listaAuxiliar = new ArrayList<>();
            FileInputStream fis = new FileInputStream(file);
            if (fis.available() > 0) {
                ObjectInputStream ois = new ObjectInputStream(fis);

                listaAuxiliar = (List<List<Object>>) ois.readObject();
                ois.close();
                fis.close();

                List<Object> a = listaAuxiliar.get(0);

                VacTypes = (List<String>)(Object) a;
            }
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
        catch (ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
        }
    }

    /**
     * Shows all existent vaccine types in the ArrayList.
     */
    public static void showVacTypes(){
        int i = 1;

        System.out.println("");
        System.out.println("Existent Vaccine Types: ");
        for (String vtype : VacTypes) {
            System.out.println(i + " - " +vtype);
            i++;
        }

    }

    /**
     * Get type string.
     *
     * @param op the position in the list of the type
     * @return the type (String)
     */
    public static String getType(int op){
        return VacTypes.get(op-1);
    }
    public String getType2(){
        return this.type;
    }
    public static boolean Checkvac(String vactype){
        int i=0;
        for (String vtype : VacTypes) {
            if (vtype.equals(vactype)) {
                return false;
            }
            i++;
        }
        System.out.println("This Vaccine Type doesn't exists, please try again.");
        return true;
    }
}
