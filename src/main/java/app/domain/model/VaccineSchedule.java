package app.domain.model;


import app.ui.console.stores.NHSUserStore;

import java.util.Date;

/**
 *
 * @author Mário Baptista <1211265@isep.ipp.pt>
 * @author Pedro Sá <1211269@isep.ipp.pt>
 */

/**
 * The type Vaccine schedule.
 */
public class VaccineSchedule implements java.io.Serializable {

    private Date date;
    private String time;
    private int centerId;
    private String vacType;
    private int SNSNumber;
    private NHSUser user;

    @Override
    public String toString() {
        return "VaccineSchedule{" +
                "date=" + date +
                ", time='" + time + '\'' +
                ", centerId=" + centerId +
                ", vacType='" + vacType + '\'' +
                ", SNSNumber=" + SNSNumber +
                ", user=" + user.getName() +
                '}';
    }

    /**
     * Instantiates a new Vaccine schedule.
     *
     * @param SNSNumber the sns number
     * @param centerId  the center id
     * @param date      the date
     * @param time      the time
     * @param vacType   the vac type
     */
    public VaccineSchedule(int SNSNumber, int centerId, Date date, String time, String vacType) {
        this.date = date;
        this.time = time;
        this.centerId = centerId;
        this.vacType = vacType;
        this.SNSNumber = SNSNumber;
        getUserFromSNSNumber();
    }

    private void getUserFromSNSNumber(){
        NHSUserStore.getNHSUsers().forEach(nhsUser -> {
            if(nhsUser.getSNSNumber() == SNSNumber){
                user = nhsUser;
            }
        });
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Sets date.
     *
     * @param date the date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Gets time.
     *
     * @return the time
     */
    public String getTime() {
        return time;
    }

    /**
     * Sets time.
     *
     * @param time the time
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * Gets center.
     *
     * @return the center
     */
    public int getCenter() {
        return centerId;
    }

    /**
     * Sets center.
     *
     * @param centerId the center id
     */
    public void setCenter(int centerId) {
        this.centerId = centerId;
    }

    /**
     * Gets vac type.
     *
     * @return the vac type
     */
    public String getVacType() {
        return vacType;
    }

    /**
     * Sets vac type.
     *
     * @param vacType the vac type
     */
    public void setVacType(String vacType) {
        this.vacType = vacType;
    }

    /**
     * Gets sns number.
     *
     * @return the sns number
     */
    public int getSNSNumber() {
        return SNSNumber;
    }

    /**
     * Sets sns number.
     *
     * @param SNSNumber the sns number
     */
    public void setSNSNumber(int SNSNumber) {
        this.SNSNumber = SNSNumber;
    }

    /**
     * Gets user.
     *
     * @return the user
     */
    public NHSUser getUser() {
        return user;
    }


}
