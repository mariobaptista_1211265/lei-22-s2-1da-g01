package app.domain.model;

import app.domain.dto.CenterDto;

/**
 * Vaccination Center Class.
 *
 * @author João Fortuna <1211261@isep.ipp.pt>
 */
public class VaccinationCenter extends Center implements java.io.Serializable{

    private String vaccinetype;

    /**
     * Instantiates a new Vaccination center.
     *
     * @param vacCenter the vaccination center
     */
    public VaccinationCenter(CenterDto vacCenter){
        super(vacCenter.getId(),vacCenter.getName(), vacCenter.getAddress(), vacCenter.getPhoneNumber(), vacCenter.getEmailAddress(), vacCenter.getOpeningHours(), vacCenter.getClosingHours(), vacCenter.getSlotDuration(), vacCenter.getSlots(), vacCenter.getMaximumVacSlot());
    }

    /**
     * Gets the vaccine type used in that center.
     *
     * @return the vaccine type used in that center.
     */
    public String getVACVaccinetype() {
        return vaccinetype;
    }

    /**
     * Sets the vaccine type used in that center.
     *
     * @param vaccinetype the vaccine type used in that center
     */
    public void setVACVaccinetype(String vaccinetype) {
        this.vaccinetype = vaccinetype;
    }

    @Override
    public String toString() {
        return "Vaccination Center{" + '\'' +
                "name='" + getName() + '\'' +
                ", address='" + getAddress() + '\'' +
                ", phoneNumber='" + getPhoneNumber() + '\'' +
                ", emailAddress='" + getEmailAddress() + '\'' +
                ", openingHours='" + getOpeningHours() + '\'' +
                ", closingHours='" + getClosingHours() + '\'' +
                ", slots='" + getSlots() + '\'' +
                ", slotDuration='" + getSlotDuration() + '\'' +
                ", maximumVacSlot='" + getMaximumVacSlot() + '\'' +
                ", has vac type='" + vaccinetype + '\'' +
                '}';
    }
}
