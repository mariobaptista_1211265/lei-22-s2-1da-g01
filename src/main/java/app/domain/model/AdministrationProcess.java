package app.domain.model;

/**
 * @author Pedro Sá nº1211269
 *
 * The type Administration process.
 */
public class AdministrationProcess implements java.io.Serializable {

    private int ageI;
    private int ageF;
    private double dosage;
    private double numberOfDoses;
    private int timeInterval;
    private String vactype;
    private String brand;

    /**
     * Instantiates a new Administration process.
     *
     * @param brand         the brand
     * @param vactype       the vactype
     * @param ageI          the age i
     * @param ageF          the age f
     * @param dosage        the dosage
     * @param numberOfDoses the number of doses
     * @param timeInterval  the time interval
     */
    public AdministrationProcess(String brand,String vactype ,int ageI, int ageF, double dosage, double numberOfDoses, int timeInterval){
        this.brand=brand;
        this.vactype=vactype;
        this.ageI=ageI;
        this.ageF=ageF;
        this.dosage=dosage;
        this.numberOfDoses=numberOfDoses;
        this.timeInterval=timeInterval;
    }

    /**
     * Get brand string.
     *
     * @return the string
     */
    public String getBrand(){
        return this.brand;
    }

    /**
     * Set brand.
     *
     * @param brand the brand
     */
    public void setBrand(String brand){
        this.brand= brand;
    }

    /**
     * Getvactype string.
     *
     * @return the string
     */
    public String getvactype(){
        return this.vactype;
    }

    /**
     * Setvactype.
     *
     * @param vactype the vactype
     */
    public void setvactype(String vactype){
        this.vactype= vactype;
    }

    /**
     * Gets age i.
     *
     * @return the age i
     */
    public int getAgeI() {
        return this.ageI;
    }

    /**
     * Sets age i.
     *
     * @param ageI the age i
     */
    public void setAgeI(int ageI) {
        this.ageI = ageI;
    }

    /**
     * Gets age f.
     *
     * @return the age f
     */
    public int getAgeF() {
        return this.ageF;
    }

    /**
     * Sets age f.
     *
     * @param ageF the age f
     */
    public void setAgeF(int ageF) {
        this.ageF = ageF;
    }

    /**
     * Gets dosage.
     *
     * @return the dosage
     */
    public double getDosage() {
        return this.dosage;
    }

    /**
     * Sets dosage.
     *
     * @param dosage the dosage
     */
    public void setDosage(double dosage) {
        this.dosage = dosage;
    }

    /**
     * Gets number of doses.
     *
     * @return the number of doses
     */
    public double getNumberOfDoses() {
        return this.numberOfDoses;
    }

    /**
     * Sets number of doses.
     *
     * @param numberOfDoses the number of doses
     */
    public void setNumberOfDoses(double numberOfDoses) {
        this.numberOfDoses = numberOfDoses;
    }

    /**
     * Gets time interval.
     *
     * @return the time interval
     */
    public double getTimeInterval() {
        return this.timeInterval;
    }

    /**
     * Sets time interval.
     *
     * @param timeInterval the time interval
     */
    public void setTimeInterval(int timeInterval) {
        this.timeInterval = timeInterval;
    }

    @Override
    public String toString() {
        return "AdministrationProcess{" +
                "ageI=" + ageI +
                ", ageF=" + ageF +
                ", dosage=" + dosage +
                ", numberOfDoses=" + numberOfDoses +
                ", timeInterval=" + timeInterval +
                ", vactype='" + vactype + '\'' +
                ", brand='" + brand + '\'' +
                '}';
    }
}
