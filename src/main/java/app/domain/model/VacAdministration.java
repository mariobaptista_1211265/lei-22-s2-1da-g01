package app.domain.model;

import app.domain.dto.VaccinationDTO;

import java.util.Date;

public class VacAdministration implements java.io.Serializable {

    private int snsNumber;
    private String vaccineName;
    private int vaccineDose;
    private String lotNumber;
    private Date scheduledDateTime;
    private Date arrivalDateTime;
    private Date administrationDateTime;
    private Date leavingDateTime;
    private int centerID;

    public VacAdministration() {
    }

    public VacAdministration(VaccinationDTO vaccinationDTO) {
        this.snsNumber = vaccinationDTO.getSnsNumber();
        this.vaccineName = vaccinationDTO.getVaccineName();
        this.vaccineDose = vaccinationDTO.getVaccineDose();
        this.lotNumber = vaccinationDTO.getLotNumber();
        this.scheduledDateTime = vaccinationDTO.getScheduledDateTime();
        this.arrivalDateTime = vaccinationDTO.getArrivalDateTime();
        this.administrationDateTime = vaccinationDTO.getConfirmationDateTime();
        this.leavingDateTime = vaccinationDTO.getLeavingDateTime();
        this.centerID = vaccinationDTO.getCenterID();
    }

    public int getCenterID() {
        return centerID;
    }

    public void setCenterID(int centerID) {
        this.centerID = centerID;
    }

    public String getVaccineName() {
        return vaccineName;
    }

    public void setVaccineName(String vaccineName) {
        this.vaccineName = vaccineName;
    }

    public int getVaccineDose() {
        return vaccineDose;
    }

    public void setVaccineDose(int vaccineDose) {
        this.vaccineDose = vaccineDose;
    }

    public String getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }

    public Date getScheduledDateTime() {
        return scheduledDateTime;
    }

    public void setScheduledDateTime(Date scheduledDateTime) {
        this.scheduledDateTime = scheduledDateTime;
    }

    public Date getArrivalDateTime() {
        return arrivalDateTime;
    }

    public void setArrivalDateTime(Date arrivalDateTime) {
        this.arrivalDateTime = arrivalDateTime;
    }

    public int getSnsNumber() {
        return snsNumber;
    }

    public void setSnsNumber(int snsNumber) {
        this.snsNumber = snsNumber;
    }

    public Date getAdministrationDateTime() {
        return administrationDateTime;
    }

    public void setAdministrationDateTime(Date administrationDateTime) {
        this.administrationDateTime = administrationDateTime;
    }

    public Date getLeavingDateTime() {
        return leavingDateTime;
    }

    public void setLeavingDateTime(Date leavingDateTime) {
        this.leavingDateTime = leavingDateTime;
    }

    @Override
    public String toString() {
        return "VacConfirmation{" +
                "snsNumber=" + snsNumber + '\'' +
                "centerID=" + centerID + '\'' +
                "vaccineName='" + vaccineName + '\'' +
                ", vaccineDose='" + vaccineDose + '\'' +
                ", lotNumber='" + lotNumber + '\'' +
                ", ScheduledDateTime=" + scheduledDateTime + '\'' +
                ", ArrivalDateTime=" + arrivalDateTime + '\'' +
                ", ConfirmationDateTime=" + administrationDateTime + '\'' +
                ", LeavingDateTime=" + leavingDateTime +
                '}';
    }
}
