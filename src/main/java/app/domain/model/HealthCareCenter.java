package app.domain.model;

import app.domain.dto.CenterDto;

import java.util.ArrayList;
import java.util.List;

/**
 *  Health Care Center Class.
 *
 * @author João Fortuna <1211261@isep.ipp.pt>
 */
public class HealthCareCenter extends Center implements java.io.Serializable {

    private String ARS;
    private String AGES;
    /**
     * @param VacTypes an ArrayList with the vaccine types used in the healthcare center
     */
    private List<String> VacTypes = new ArrayList<>();


    /**
     * Instantiates a new Health care center.
     *
     * @param vacCenter the center DTO
     * @param ARS       the ars
     * @param AGES      the ages
     */
    public HealthCareCenter(CenterDto vacCenter, String ARS, String AGES){
        super(vacCenter.getId(), vacCenter.getName(), vacCenter.getAddress(), vacCenter.getPhoneNumber(), vacCenter.getEmailAddress(), vacCenter.getOpeningHours(), vacCenter.getClosingHours(), vacCenter.getSlotDuration(), vacCenter.getSlots(), vacCenter.getMaximumVacSlot());
        this.ARS = ARS;
        this.AGES = AGES;
    }

    /**
     * Adds a vaccine type to the array list.
     *
     * @param type the type
     */
    public void addHealthType(String type){
        VacTypes.add(type);
    }

    /**
     * shows all vaccine types used in that health care center.
     */
    public void getHealthTypes(){
        int i = 1;
        System.out.println("Vaccine Types the Center has :");
        for (String vtype : VacTypes) {
            System.out.println(i + "- " + vtype);
            i++;
        }
    }

    /**
     * Gets ars.
     *
     * @return the ars
     */
    public String getARS() {
        return ARS;
    }

    /**
     * Sets ars.
     *
     * @param ARS the ars
     */
    public void setARS(String ARS) {
        this.ARS = ARS;
    }

    /**
     * Gets ages.
     *
     * @return the ages
     */
    public String getAGES() {
        return AGES;
    }

    /**
     * Sets ages.
     *
     * @param AGES the ages
     */
    public void setAGES(String AGES) {
        this.AGES = AGES;
    }

    /**
     * used to replace a vaccine type with a new one
     * verifies if the vaccine type already exists in the center types list, and if it doesn't, it adds the new type to it.
     * if it already exists in the list, it deletes the one he was replacing and doesn't add any.
     *
     * @param op     the position in the list of the type the admin wants to replace
     * @param newtype the new type
     */
    public void setHealthVacType(int op,String newtype){

        boolean verify = false;

        for (String vtype : VacTypes) {
            if(vtype == newtype){
                verify = true;
            }
        }

        if(!verify){
            for (String vtype : VacTypes) {
                if(vtype == VacTypes.get(op-1)){
                    VacTypes.set(op-1,newtype);
                }
            }
        }else VacTypes.remove(op-1);

    }

    @Override
    public String toString() {
        return "Health Care Center{" + '\'' +
                "name='" + getName() + '\'' +
                ", address='" + getAddress()  + '\'' +
                ", phoneNumber='" + getPhoneNumber() + '\'' +
                ", emailAddress='" + getEmailAddress() + '\'' +
                ", openingHours='" + getOpeningHours()  + '\'' +
                ", closingHours='" + getClosingHours() + '\'' +
                ", slots='" + getSlots() + '\'' +
                ", slotDuration='" + getSlotDuration() + '\'' +
                ", maximumVacSlot='" + getMaximumVacSlot() + '\'' +
                ", ARS='" + ARS + '\'' +
                ", AGES='" + AGES + '\'' + "}";
    }

}
