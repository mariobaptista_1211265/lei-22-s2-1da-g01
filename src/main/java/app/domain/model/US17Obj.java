package app.domain.model;

public class US17Obj {
    private String SNS;
    private String Name;
    private String Vaccine;
    private String Dose;
    private String Lot;
    private String Schedule;
    private String Arrival;
    private String Administration;
    private String Leave;

    public US17Obj(String SNS, String name, String vaccine, String dose, String lot, String schedule, String arrival, String administration, String leave) {
        this.SNS = SNS;
        Name = name;
        Vaccine = vaccine;
        Dose = dose;
        Lot = lot;
        Schedule = schedule;
        Arrival = arrival;
        Administration = administration;
        Leave = leave;
    }

    public String getSNS() {
        return SNS;
    }

    public String getName() {
        return Name;
    }

    public String getVaccine() {
        return Vaccine;
    }

    public String getDose() {
        return Dose;
    }

    public String getLot() {
        return Lot;
    }

    public String getSchedule() {
        return Schedule;
    }

    public String getArrival() {
        return Arrival;
    }

    public String getAdministration() {
        return Administration;
    }

    public String getLeave() {
        return Leave;
    }
}
