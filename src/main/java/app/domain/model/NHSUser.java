package app.domain.model;

import app.domain.dto.NHSUserDto;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The type Nhs user.
 *
 * @author Mário Baptista nº1211265
 */
public class NHSUser implements java.io.Serializable {
    private String name;
    private String address;
    private String sex;
    private int phone;
    private String email;
    private Date birthDate;
    private int SNSNumber;
    private int citizenCardNumber;
    private String password = null;


    /**
     * Instantiates a new Nhs user.
     *
     * @param dto the dto
     */
    public NHSUser(NHSUserDto dto) {
        this.name = dto.getName();
        this.address = dto.getAddress();
        this.sex = dto.getSex();
        this.phone = dto.getPhone();
        this.email = dto.getEmail();
        this.birthDate = dto.getBirthDate();
        this.SNSNumber = dto.getSNSNumber();
        this.citizenCardNumber = dto.getCitizenCardNumber();
        this.password = dto.getPassword();
    }


    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets address.
     *
     * @param address the address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Gets phone.
     *
     * @return the phone
     */
    public int getPhone() {
        return phone;
    }

    /**
     * Sets phone.
     *
     * @param phone the phone
     */
    public void setPhone(int phone) {
        this.phone = phone;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets sex.
     *
     * @return the sex
     */
    public String getSex() {
        return sex;
    }

    /**
     * Sets sex.
     *
     * @param sex the sex
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * Gets birth date.
     *
     * @return the birth date
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * Sets birth date.
     *
     * @param birthDate the birth date
     */
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * Gets sns number.
     *
     * @return the sns number
     */
    public int getSNSNumber() {
        return SNSNumber;
    }

    /**
     * Sets sns number.
     *
     * @param SNSNumber the sns number
     */
    public void setSNSNumber(int SNSNumber) {
        this.SNSNumber = SNSNumber;
    }

    /**
     * Gets citizen card number.
     *
     * @return the citizen card number
     */
    public int getCitizenCardNumber() {
        return citizenCardNumber;
    }

    /**
     * Sets citizen card number.
     *
     * @param citizenCardNumber the citizen card number
     */
    public void setCitizenCardNumber(int citizenCardNumber) {
        this.citizenCardNumber = citizenCardNumber;
    }

    public String getPassword(){
        return password;
    }

    @Override
    public String toString() {
        SimpleDateFormat dt1 = new SimpleDateFormat("dd-MM-yyyy");
        return
                "\n name: '" + name + '\'' +
                "\n address: '" + address + '\'' +
                "\n sex: '" + sex + '\'' +
                "\n phone: " + phone +
                "\n email: '" + email + '\'' +
                "\n birthDate: " + dt1.format(birthDate) +
                "\n SNSNumber: " + SNSNumber +
                "\n citizenCardNumber: " + citizenCardNumber +
                "\n password: " + password;
    }
}
