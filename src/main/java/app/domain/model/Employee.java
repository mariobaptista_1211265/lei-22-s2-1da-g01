package app.domain.model;

import app.controller.App;
import app.domain.dto.EmployeeDto;

/**
 * The type Employee.
 */
public class Employee implements java.io.Serializable {
    private String name;
    private String address;
    private int phone;
    private String email;
    private int citizenCardNumber;
    private String postCode;
    private String city;
    private String role;
    private String password;


    /**
     * Instantiates a new Employee.
     *
     * @param dto the dto
     */
    public Employee(EmployeeDto dto) {
        this.name = dto.getName();
        this.address = dto.getAddress();
        this.phone = dto.getPhone();
        this.email = dto.getEmail();
        this.citizenCardNumber = dto.getCitizenCardNumber();
        this.city = dto.getCity();
        this.postCode = dto.getPostCode();
        this.role = dto.getRole();
        this.password = dto.getPassword();
    }


    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets address.
     *
     * @param address the address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Gets phone.
     *
     * @return the phone
     */
    public int getPhone() {
        return phone;
    }

    /**
     * Sets phone.
     *
     * @param phone the phone
     */
    public void setPhone(int phone) {
        this.phone = phone;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets citizen card number.
     *
     * @return the citizen card number
     */
    public int getCitizenCardNumber() {
        return citizenCardNumber;
    }

    /**
     * Sets citizen card number.
     *
     * @param citizenCardNumber the citizen card number
     */
    public void setCitizenCardNumber(int citizenCardNumber) {
        this.citizenCardNumber = citizenCardNumber;
    }

    /**
     * Gets post code.
     *
     * @return the post code
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * Sets post code.
     *
     * @param postCode the post code
     */
    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    /**
     * Gets city.
     *
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets city.
     *
     * @param city the city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Gets role.
     *
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * Sets role.
     *
     * @param role the role
     */
    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {

        //String StringRole = App.getInstance().getCompany().getAuthFacade().getRole(role).get().getDescription();

        return
                "\n name: " + name +
                "\n address: " + address +
                "\n phone: " + phone +
                "\n email: " + email +
                "\n citizenCardNumber: " + citizenCardNumber  +
                "\n postCode: " + postCode +
                "\n city: " + city;
                //"\n Role: " + StringRole;
    }
}
