package app.domain.model;

import app.ui.console.stores.CenterStore;
import app.ui.console.stores.VaccinationLogStore;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

public class NumberOfUsersVaccinated extends TimerTask{

    @Override
    public void run() {

        String filename = "VacinationsPerDay.csv";

        File vacfile = new File(filename);
        addHeader(filename, vacfile);



        SimpleDateFormat dateFormater = new SimpleDateFormat("dd-MM-yyyy");

        VaccinationLogStore vacLogstore = new VaccinationLogStore();
        CenterStore centerStore = new CenterStore();

        List<VacAdministration> userVaccinatedToday;
        List<String> vaccinationCentersNames;
        List<String> healthCentersNames;
        List<String> allCentersNames = new ArrayList<>();
        List<Integer> centersIds = new ArrayList<>();
        List<Integer> numberUsersPerCenter = new ArrayList<>();
        Date today = new Date();


        userVaccinatedToday = vacLogstore.getVacLogsOfDay(today);
        vaccinationCentersNames = List.of(centerStore.getVacCenterNames());
        healthCentersNames = List.of(centerStore.getHealthCenterNames());

        for(String name : vaccinationCentersNames){
            allCentersNames.add(name);
        }

        for(String name : healthCentersNames){
            allCentersNames.add(name);
        }

        for(String centerName : vaccinationCentersNames){
            centersIds.add(centerStore.getVacCenterIDbyName(centerName));
        }

        for(String centerName : healthCentersNames){
            centersIds.add(centerStore.getHealthCenterIDbyName(centerName));
        }

        for(Integer id : centersIds){
            int count = 0;
            for(VacAdministration user : userVaccinatedToday){
                if ((user.getCenterID() == id)){
                    count++;
                }
            }
            numberUsersPerCenter.add(count);
        }

        appendContent(filename, dateFormater, allCentersNames, numberUsersPerCenter, today);

    }

    private void appendContent(String filename, SimpleDateFormat dateFormater, List<String> allCentersNames, List<Integer> numberUsersPerCenter, Date today) {
        try {
            FileWriter fileWriter = new FileWriter(filename, true);
            PrintWriter pw = new PrintWriter(fileWriter);
            StringBuffer csvData = new StringBuffer("");
            for(int i = 0; i < numberUsersPerCenter.size(); i++){
                csvData.append(dateFormater.format(today));
                csvData.append(';');
                csvData.append(allCentersNames.get(i));
                csvData.append(';');
                csvData.append(numberUsersPerCenter.get(i));
                csvData.append('\n');
            }
            csvData.append('\n');
            pw.write(csvData.toString());
            pw.close();
            System.out.println("Data from today(" + dateFormater.format(today) + ") registered");
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void addHeader(String filename, File vacfile) {
        try {
            if(vacfile.createNewFile()){
                FileWriter fileWriter = new FileWriter(filename, true);
                PrintWriter pw = new PrintWriter(fileWriter);
                StringBuffer csvHeader = new StringBuffer("");
                csvHeader.append("Date;Name of the Center;Number of Users Vacinated\n");
                pw.write(csvHeader.toString());
                pw.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
