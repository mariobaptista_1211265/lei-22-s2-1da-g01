package app.domain.model;

import java.util.Date;

public class US15Obj {

    private String Date;
    private String User;

    public US15Obj(String Date, String User) {
        this.Date = Date;
        this.User = User;

    }

    public String getDate() {
        return Date;
    }

    public String getUser() {
        return User;
    }
}
