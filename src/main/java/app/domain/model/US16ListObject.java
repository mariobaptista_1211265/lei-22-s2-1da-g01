package app.domain.model;

public class US16ListObject {
    private String timeStart;
    private String timeEnd;
    private String difference;

    public US16ListObject(String startDate, String endDate,String differenceOfPeople){
        timeStart = startDate;
        timeEnd = endDate;
        difference = differenceOfPeople;
    }

    public String getDifference() {
        return difference;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

}
