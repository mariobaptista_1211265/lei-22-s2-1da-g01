package app.domain.dto;

import java.util.Date;

/**
 * The type Waiting room dto.
 *
 * @author Manuel Pimentel <1210670@isep.ipp.pt>
 */
public class WaitingRoomDTO {
    private String Name;
    private Date DateOfArrival;

    /**
     * Instantiates a new Waiting room dto.
     *
     * @param name          the name
     * @param DateOfArrival the date of arrival
     */
    public WaitingRoomDTO(String name, Date DateOfArrival) {
        this.Name = name;
        this.DateOfArrival = DateOfArrival;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return Name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.Name = Name;
    }

    /**
     * Gets date of arrival.
     *
     * @return the date of arrival
     */
    public Date getDateOfArrival() {
        return DateOfArrival;
    }

    /**
     * Sets date of arrival.
     *
     * @param DateOfArrival the date of arrival
     */
    public void setDateOfArrival(Date DateOfArrival) {
        this.DateOfArrival = DateOfArrival;
    }
}
