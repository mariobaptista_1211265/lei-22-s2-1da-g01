package app.domain.dto;

import java.util.Date;

/**
 * The type Nhs user dto.
 *
 * @author Mário Baptista nº1211265
 */
public class NHSUserDto {

    private String name;
    private String address;
    private String sex;
    private int phone;
    private String email;
    private Date birthDate;
    private int SNSNumber;
    private int citizenCardNumber;
    private String password;

    private String SEX_BY_DEFAULT = "Sem sexo especificado";


    /**
     * Instantiates a new Nhs user dto.
     *
     * @param name              the name
     * @param address           the address
     * @param sex               the sex
     * @param phone             the phone
     * @param email             the email
     * @param birthDate         the birth date
     * @param SNSNumber         the sns number
     * @param citizenCardNumber the citizen card number
     */
    public NHSUserDto(String name, String address, String sex, int phone, String email, Date birthDate, int SNSNumber, int citizenCardNumber) {
        this.name = name;
        this.address = address;
        this.sex = sex;
        this.phone = phone;
        this.email = email;
        this.birthDate = birthDate;
        this.SNSNumber = SNSNumber;
        this.citizenCardNumber = citizenCardNumber;
        this.password = "123456";
    }
    public NHSUserDto(String name, String address, String sex, int phone, String email, Date birthDate, int SNSNumber, int citizenCardNumber, String password) {
        this.name = name;
        this.address = address;
        this.sex = sex;
        this.phone = phone;
        this.email = email;
        this.birthDate = birthDate;
        this.SNSNumber = SNSNumber;
        this.citizenCardNumber = citizenCardNumber;
        this.password = password;
    }

    /**
     * Instantiates a new Nhs user dto.
     *
     * @param name              the name
     * @param address           the address
     * @param phone             the phone
     * @param email             the email
     * @param birthDate         the birth date
     * @param SNSNumber         the sns number
     * @param citizenCardNumber the citizen card number
     */
    public NHSUserDto(String name, String address, int phone, String email, Date birthDate, int SNSNumber, int citizenCardNumber) {
        this.name = name;
        this.address = address;
        this.sex = SEX_BY_DEFAULT;
        this.phone = phone;
        this.email = email;
        this.birthDate = birthDate;
        this.SNSNumber = SNSNumber;
        this.citizenCardNumber = citizenCardNumber;
    }


    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets address.
     *
     * @param address the address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Gets phone.
     *
     * @return the phone
     */
    public int getPhone() {
        return phone;
    }

    /**
     * Sets phone.
     *
     * @param phone the phone
     */
    public void setPhone(int phone) {
        this.phone = phone;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets sex.
     *
     * @return the sex
     */
    public String getSex() {
        return sex;
    }

    /**
     * Sets sex.
     *
     * @param sex the sex
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * Gets birth date.
     *
     * @return the birth date
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * Sets birth date.
     *
     * @param birthDate the birth date
     */
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * Gets sns number.
     *
     * @return the sns number
     */
    public int getSNSNumber() {
        return SNSNumber;
    }

    /**
     * Sets sns number.
     *
     * @param SNSNumber the sns number
     */
    public void setSNSNumber(int SNSNumber) {
        this.SNSNumber = SNSNumber;
    }

    /**
     * Gets citizen card number.
     *
     * @return the citizen card number
     */
    public int getCitizenCardNumber() {
        return citizenCardNumber;
    }

    /**
     * Sets citizen card number.
     *
     * @param citizenCardNumber the citizen card number
     */
    public void setCitizenCardNumber(int citizenCardNumber) {
        this.citizenCardNumber = citizenCardNumber;
    }

    public String getPassword() {
        return password;
    }
}
