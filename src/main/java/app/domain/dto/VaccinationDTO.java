package app.domain.dto;

import java.util.Date;

public class VaccinationDTO {
    private int snsNumber;
    private String vaccineName;
    private int vaccineDose;
    private String lotNumber;
    private Date ScheduledDateTime;
    private Date ArrivalDateTime;
    private Date ConfirmationDateTime;
    private Date LeavingDateTime;
    private int CenterID;


    public VaccinationDTO(int snsNumber, String vaccineName, int vaccineDose, String lotNumber, Date scheduledDateTime, Date arrivalDateTime, Date confirmationDateTime, Date leavingDateTime, int centerID) {
        this.snsNumber = snsNumber;
        this.vaccineName = vaccineName;
        this.vaccineDose = vaccineDose;
        this.lotNumber = lotNumber;
        this.ScheduledDateTime = scheduledDateTime;
        this.ArrivalDateTime = arrivalDateTime;
        this.ConfirmationDateTime = confirmationDateTime;
        this.LeavingDateTime = leavingDateTime;
        this.CenterID = centerID;

    }

    public int getSnsNumber() {
        return snsNumber;
    }

    public String getVaccineName() {
        return vaccineName;
    }

    public int getVaccineDose() {
        return vaccineDose;
    }

    public String getLotNumber() {
        return lotNumber;
    }

    public Date getScheduledDateTime() {
        return ScheduledDateTime;
    }

    public Date getArrivalDateTime() {
        return ArrivalDateTime;
    }

    public Date getConfirmationDateTime() {
        return ConfirmationDateTime;
    }

    public Date getLeavingDateTime() {
        return LeavingDateTime;
    }

    public int getCenterID() {
        return CenterID;
    }
}