package app.domain.dto;

/**
 * The type Center dto.
 *
 * @author João Fortuna <1211261@isep.ipp.pt>
 */
public class CenterDto {

    private int id;
    private String name;
    private String address;
    private int phoneNumber;
    private String emailAddress;
    private String openingHours;
    private String closingHours;
    private String slotDuration;
    private int slots;
    private int maximumVacSlot;


    /**
     * Instantiates a new Center dto.
     *
     * @param name           the name
     * @param address        the address
     * @param phoneNumber    the phone number
     * @param emailAddress   the email address
     * @param openingHours   the opening hours
     * @param closingHours   the closing hours
     * @param slotDuration   the slot duration
     * @param slots          the slots
     * @param maximumVacSlot the maximum vac slot
     */
    public CenterDto(int id,String name, String address, int phoneNumber, String emailAddress, String openingHours, String closingHours, String slotDuration,int slots, int maximumVacSlot){
        this.id = id;
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.openingHours = openingHours;
        this.closingHours = closingHours;
        this.slotDuration = slotDuration;
        this.slots = slots;
        this.maximumVacSlot = maximumVacSlot;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets address.
     *
     * @param address the address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Gets phone number.
     *
     * @return the phone number
     */
    public int getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Sets phone number.
     *
     * @param phoneNumber the phone number
     */
    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Gets email address.
     *
     * @return the email address
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets email address.
     *
     * @param emailAddress the email address
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * Gets opening hours.
     *
     * @return the opening hours
     */
    public String getOpeningHours() {
        return openingHours;
    }

    /**
     * Sets opening hours.
     *
     * @param openingHours the opening hours
     */
    public void setOpeningHours(String openingHours) {
        this.openingHours = openingHours;
    }

    /**
     * Gets closing hours.
     *
     * @return the closing hours
     */
    public String getClosingHours() {
        return closingHours;
    }

    /**
     * Sets closing hours.
     *
     * @param closingHours the closing hours
     */
    public void setClosingHours(String closingHours) {
        this.closingHours = closingHours;
    }

    /**
     * Gets slot duration.
     *
     * @return the slot duration
     */
    public String getSlotDuration() {
        return slotDuration;
    }

    /**
     * Sets slot duration.
     *
     * @param slotDuration the slot duration
     */
    public void setSlotDuration(String slotDuration) {
        this.slotDuration = slotDuration;
    }

    /**
     * Gets slots.
     *
     * @return the slots
     */
    public int getSlots() {
        return slots;
    }

    /**
     * Sets slots.
     *
     * @param slots the slots
     */
    public void setSlots(int slots) {
        this.slots = slots;
    }

    /**
     * Gets maximum vac slot.
     *
     * @return the maximum vaccines per slot
     */
    public int getMaximumVacSlot() {
        return maximumVacSlot;
    }

    /**
     * Sets maximum vac slot.
     *
     * @param maximumVacSlot the maximum vaccines per slot
     */
    public void setMaximumVacSlot(int maximumVacSlot) {
        this.maximumVacSlot = maximumVacSlot;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
