package app.domain.dto;

public class EmployeeDto {
    private String name;
    private String address;
    private int phone;
    private String email;
    private int citizenCardNumber;
    private String postCode;
    private String city;
    private String role;

    private String password;

    public String getPassword() {
        return password;
    }

    public EmployeeDto(String name, String address, int phone, String email, int citizenCardNumber, String postCode, String city, String role, String password) {
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.citizenCardNumber = citizenCardNumber;
        this.postCode = postCode;
        this.city = city;
        this.role = role;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getCitizenCardNumber() {
        return citizenCardNumber;
    }

    public void setCitizenCardNumber(int citizenCardNumber) {
        this.citizenCardNumber = citizenCardNumber;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
