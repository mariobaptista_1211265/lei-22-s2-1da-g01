package app.domain.dto;

public class CoordinatorDto {
    private int vacCenterID;
    private String email;

    private String name;

    private String password;

    public CoordinatorDto(int centerID, String email, String name, String password){
        vacCenterID = centerID;
        this.email = email;
        this.name = name;
        this.password = password;
    }

    public int getVacCenterID() {
        return vacCenterID;
    }

    public String getEmail() {
        return email;
    }

    public void setVacCenterID(int vacCenterID) {
        this.vacCenterID = vacCenterID;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
