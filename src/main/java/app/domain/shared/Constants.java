package app.domain.shared;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Constants {
    public static final String ROLE_ADMIN = "ADMINISTRATOR";
    public static final String ROLE_RECEPTIONIST = "RECEPTIONIST";
    public static final String ROLE_NURSE = "NURSE";
    public static final String ROLE_COORDINATOR = "COORDINATOR";
    public static final String ROLE_NHS_USER = "NHS_USER";

    public static final int RECOVERY_PERIOD_FOR_MESSAGE_TEST = 60000;

    public static final int DEFAULT_RECOVERY_PERIOD_IN_MILISECONDS = 1800000;
    public static final String SERIALIZATION_FOLDER_NAME = "data/";
    public static final String NHSUSERS_FILE_NAME = SERIALIZATION_FOLDER_NAME+"nhsUsers.ser";
    public static final String VACCINATION_CENTER_FILE_NAME = SERIALIZATION_FOLDER_NAME+"vacCenters.ser";
    public static final String HEALTH_CARE_CENTER_FILE_NAME = SERIALIZATION_FOLDER_NAME+"healthCenters.ser";
    public static final String EMPLOYEE_FILE_NAME = SERIALIZATION_FOLDER_NAME+"employees.ser";
    public static final String VACCINE_ADM_FILE_NAME = SERIALIZATION_FOLDER_NAME+"vaccineAdm.ser";
    public static final String VACCINE_SCHEDULE_FILE_NAME = SERIALIZATION_FOLDER_NAME+"vaccineSchedule.ser";
    public static final String VACCINES_FILE_NAME = SERIALIZATION_FOLDER_NAME+"vaccines.ser";
    public static final String VACCINATION_LOG_FILE_NAME = SERIALIZATION_FOLDER_NAME+"vaccinationLog.ser";
    public static final String WAITING_ROOM_FILE_NAME = SERIALIZATION_FOLDER_NAME+"waitingRoom.ser";
    public static final String VACCINATION_TYPES_FILE_NAME = SERIALIZATION_FOLDER_NAME+"vaccinationTypes.ser";
    public static final String COORDINATORS_FILE_NAME = SERIALIZATION_FOLDER_NAME+"coordinators.ser";

    public static final int NUMBER_OF_DIGITS_PHONE_NUMBER = 9;

    public static final int NUMBER_OF_DIGITS_NHS_NUMBER = 9;
    public static final String PARAMS_FILENAME = "config.properties";
    public static final String PARAMS_COMPANY_DESIGNATION = "Company.Designation";
    public static final String PARAMS_CENTERPERFORMANCE_ALGORITHM = "Center.Performance.Algorithm";
}
